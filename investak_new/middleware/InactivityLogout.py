# -*- coding: utf-8 -*-
class InactivityLogout(object):
	def process_request( self, request ):
		from datetime import timedelta
		import datetime, time
		from django.conf import settings
		COOKIE_AGE = getattr(settings, 'SESSION_COOKIE_AGE', 10)
		#print datetime.now()
		#print request.session.get_expiry_date()
		date1 = datetime.datetime.today().replace(tzinfo=None)
		expiry_date = request.session.get_expiry_date().replace(tzinfo=None)
		date2 = expiry_date - date1
		
		date3 = timedelta(seconds = COOKIE_AGE)
		
		#print expiry_date
		#print datetime.now() - expiry_date
		#print timedelta(seconds = COOKIE_AGE)
		if date2 < timedelta(seconds = COOKIE_AGE):
			new_date = date1 + date3 
			#print time.mktime(new_date.timetuple()) , "hellllllllllo"
			request.session.set_expiry(int(time.mktime(new_date.timetuple())))
		return None # pass through