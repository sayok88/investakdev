"""
WSGI config for investak_new project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""

import os, sys

sys.path.append('C:/Apache2.2/htdocs/investakdev')
sys.path.append('C:/Apache2.2/htdocs/investakdev/investak_new')
os.environ['DJANGO_SETTINGS_MODULE'] = 'investak_new.settings'

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
