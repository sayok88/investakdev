import os, sys
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "investak_new.settings")
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from django.core.wsgi import get_wsgi_application
import requests,base64
os.environ['DJANGO_SETTINGS_MODULE'] = 'investak_new.settings'
application = get_wsgi_application()

from django.template.loader import get_template
from django.template import Context
from django.conf import settings
import json
from investak.models import *
from investak.amolib import *
def decode(key, string):
	decoded_chars = []
	string = base64.urlsafe_b64decode(str(string))
	for i in xrange(len(string)):
		key_c = key[i % len(key)]
		encoded_c = chr(abs(ord(string[i]) - ord(key_c) % 256))
		decoded_chars.append(encoded_c)
	decoded_string = "".join(decoded_chars)
	return decoded_string


stock_price = 0 #price
live_unit_price = 0.00
total_stock_price = 0.00
live_share_unit = 1
nest_orderno=''
tradebook_qty=0
qty=0
tradebook_price=0.0
nest_orderno=''
order_status=6
virtual_status='rejected'
strike_price=0
rejectionreason=''
actual_investment=0.0
api_url = settings.ANGEL_BROKING_SVC_URL
api_order_book_url = str(api_url) + '/orderbook'
api_trade_book_url = str(api_url) + '/tradebook'
api_login=str(api_url)+'/login'

orders=Order.objects.filter(live_order_status__in=['amo','pending'])
amo_user=[]
for o in orders:
	if o.user_id not in amo_user:
		amo_user.append(o.user_id)

for u in amo_user:
	uid=LiveUserAngelBrokingApi.objects.filter(user_id=u).first()
	amoto=AmoTemp.objects.filter(account_id=uid.angel_id)
	if amoto:
		amoto=amoto.first()
		pw=decode(uid.angel_id+str(uid.email_id)+str(uid.mobile_number)+str(uid.dob)+str(uid.pan_Number),amoto.token)
		r = requests.post(settings.ANGEL_BROKING_SVC_URL+'/login', "{'angelID':'"+uid.angel_id+"','password':'"+pw+"','clientIP':'"+str('127.0.0.1	')+"','osType':'"+str(settings.OSTYPE)+"','osVersion':'"+str(settings.OSVERSION)+"','deviceID':'"+str(settings.DEVICEID)+"'}", verify=False)
		api_login = r.json()
		try:
			if "logonStatusValue" in api_login["Login"] and api_login["Login"]["logonStatusValue"] == "10000":
				session=api_login['Login']['sessionID']
				get_order_book=''
				#get_order_book = requests.post(api_order_book_url,"{'angelID':'"+str(uid.angel_id)+"','groupID':'"+str(api_login['Login']['groupID'])+"','userCode':'"+str(api_login['Login']['userCode'])+"','sessionID':'"+str(session)+"'}", verify=False)
				with open('order_book_response.json') as data_file:
					get_order_book=json.load(data_file)
				order_book_response_data = get_order_book
				with open('tradebook_response.json') as data_file:
					get_trade_book=json.load(data_file)
				#get_trade_book = requests.post(api_trade_book_url,"{'angelID':'"+str(uid.angel_id)+"','groupID':'"+str(api_login['Login']['groupID'])+"','userCode':'"+str(api_login['Login']['userCode'])+"','sessionID':'"+str(session)+"'}", verify=False)
				trade_book_response_data = get_trade_book
				trade_book_response = trade_book_response_data["data"]
				user_orders=Order.objects.filter(live_order_status__in=['amo','pending'],user_id=u)
				live_orders=[]
				for o in user_orders:
					templive=LiveOrder.objects.filter(virtual_order_id=o.id)
					for t in templive:
						live_orders.append(t)
				live_api=[]
				for lo in live_orders:
					liveapitemp=ApiPlaceOrderAngelBroking.objects.filter(live_order_id=lo.id)
					for l in liveapitemp:
						live_api.append(l)

				for live_api_o in live_api:
					print live_api_o.exchange_timestamp
					for order_book_response in order_book_response_data["data"]:
						if order_book_response["exchangeTimestamp"]==live_api_o.exchange_timestamp:
							print "found"
							if "status" in order_book_response:
									order_status = order_book_response["status"]
							if "rejectionrReason" in order_book_response:
									rejectionreason = order_book_response["rejectionrReason"]
							if "nestOrderNumber" in order_book_response:
								nest_orderno = order_book_response["nestOrderNumber"]
							for trade_book_res in trade_book_response:
								if order_book_response["nestOrderNumber"]==trade_book_res["nestOrderNumber"]:

									if "price" in trade_book_res:
										#stock_price = float(trade_book_res["price"])/float(trade_book_res["decimalLocator"])
										tradebook_price = float(trade_book_res["price"])/float(trade_book_res["decimalLocator"])
									if "strikePrice" in order_book_response:
										strike_price = float(order_book_response["strikePrice"])

									# ---------------------------------------------#
									if "totalQuantity" in trade_book_res:
										#qty = trade_book_res["totalQuantity"]
										tradebook_qty = trade_book_res["totalQuantity"]
									qty = tradebook_qty

									if float(order_book_response["price"]) > 0 and qty > 0:
										live_unit_price = float(stock_price)#/float(order_detail.no_of_share)
									else:
										live_unit_price = 0.00
									break
					if int(order_status)!=18:
						if (int(order_status) == 20 or int(order_status) == 7) :

							# ---------------------------------------------#
							# --------- (total stock price)/(no. of stock share) ---------- #
							#live_unit_price = float(stock_price)/float(order_detail.no_of_share)
							#live_unit_price = float(stock_price)
							total_stock_price = float(stock_price)
							# ---------------------------------------------#
							if int(qty) > 0:
								total_stock_price = float(float(stock_price) * float(qty))
							# ---------------------------------------------#

							# ---------------------------------------------#
							actual_investment = float(actual_investment) + float(total_stock_price)
							virtual_status = 'complete'
							# count_success = count_success + 1
						else:
							if int(order_status) == 6 or int(order_status) == 9 or int(order_status) == 10 or int(order_status) == 11 or int(order_status) == 12 or int(order_status) == 15 or int(order_status) == 17 or int(order_status) == 19 or int(order_status) == 21 or int(order_status) == 22:
								# count_rejected = count_rejected + 1
								virtual_status = 'rejected'
							else:
								# count_pending = count_pending + 1
								virtual_status = 'pending'
						if rejectionreason=='' and virtual_status=='rejected':
							rejectionreason='Order could not be placed at the exchange.'
						live_api_o.tradebook_qty=int(tradebook_qty)
						live_api_o.tradebook_price=int(tradebook_price)
						live_api_o.nest_orderno=nest_orderno
						live_api_o.status=int(order_status)
						live_api_o.virtual_status=virtual_status
						live_api_o.strike_price=strike_price
						live_api_o.rejectionreason=rejectionreason
						live_api_o.save()
						count_success = 0
						count_rejected = 0
						count_pending = 0
						count_amo = 0
						count_stock = 0

							# --------------------------------#
						count_stock = count_stock + 1
							# --------------------------------#
							# -------------------------------------------------#
						if live_api_o.virtual_status == 'complete':
							count_success = count_success + 1

						if live_api_o.virtual_status == 'pending' or live_api_o.virtual_status == 'postpond':
							count_pending = count_pending + 1

						if live_api_o.virtual_status == 'rejected':
							count_rejected = count_rejected + 1

						if live_api_o.virtual_status == 'amo':
							count_amo = count_amo + 1

						total_count = int(count_success) + int(count_rejected)
						if int(count_stock) == int(count_success):
							live_order_status = 'complete'
						elif int(count_stock) == int(count_rejected):
							live_order_status = 'rejected'
						elif int(count_stock) == int(total_count):
							live_order_status = 'complete'
						elif int(count_amo) > 0:
							live_order_status = 'amo'
						else:
							live_order_status = 'pending'
						#print live_order_status
						LiveOrder.objects.filter(pk=live_api_o.live_order_id).update(status=live_order_status)
						#												  #,minimum_investment=actual_investment)
						livo1=LiveOrder.objects.filter(pk=live_api_o.live_order_id).first()
						od1=Order.objects.filter(pk=livo1.virtual_order_id).first()
						LiveOrder.objects.filter(pk=od1.live_order_hold_id).update(status=live_order_status)
						#													   #,minimum_investment=actual_investment)

						od1.live_order_status=live_order_status
						od1.save()
					tradebook_qty=0
					tradebook_price=0
					nest_orderno=''
					order_status=6
					virtual_status='rejected'
					strike_price=0
					rejectionreason=''



		except Exception as e:
			pass
