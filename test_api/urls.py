from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.angel_broking_api_login, name='index'),
]