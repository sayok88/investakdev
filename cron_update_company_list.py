import os, sys
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "investak_new.settings")
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from django.core.wsgi import get_wsgi_application
import requests,base64
os.environ['DJANGO_SETTINGS_MODULE'] = 'investak_new.settings'
application = get_wsgi_application()

from django.template.loader import get_template
from django.template import Context
from django.conf import settings
import json
from investak.models import *
from investak.amolib import *
companies=Company.objects.all()
r=requests.get("http://investak.cmlinks.com/Market.svc/Company-Master?responsetype=json")
company_raw=r.json()
new_companies=[]
symbol=''
isin=''
bsecode=''
company_master=company_raw["response"]["data"]["CompanyList"]["Company"]
try:
	for company in company_master:
		c1=companies.filter(co_code=int(company["co_code"]))
		if c1:
			c1=c1.first()
			c1.co_name=company["companyname"]
			if company["nsesymbol"] is not None:
				c1.symbol=company["nsesymbol"]
			else:
				c1.symbol="NA"
			if company["isin"] is not None:
				c1.isin=company["isin"]
			else:
				c1.isin="NA"
			c1.bselisted=company["bselisted"]
			c1.nselisted=company["nselisted"]
			c1.bsetrading=company["bsetrading"]
			c1.nsetrading=company["nsetrading"]
			c1.company_short_name=company["companyshortname"]
			if company["bsecode"] is not None:
				c1.BSEcode=company["bsecode"]
			else:
				c1.BSEcode='NA'
			c1.save()
		else:
			if company["nsesymbol"] is not None:
				symbol=company["nsesymbol"]
			else:
				symbol="NA"
			if company["isin"] is not None:
				isin=company["isin"]
			else:
				isin="NA"
			if company["bsecode"] is not None:
				bsecode=company["bsecode"]
			else:
				bsecode='NA'
			c1.save()
			new_companies.append(Company(BSEcode=bsecode,co_code=int(company["co_code"]),co_name=company["companyname"],symbol=symbol,
				isin=isin,bselisted=company["bselisted"],nselisted=company["nselisted"],bsetrading=company["bsetrading"],
				nsetrading=company["nsetrading"],company_short_name=company["companyshortname"]))
	Company.objects.bulk_create(new_companies)
except Exception as e:
	print e