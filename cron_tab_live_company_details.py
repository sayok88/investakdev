# import sys
import psycopg2
# import os
import requests
# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "investak_new.settings")
# from django.db import connection

print "Hello"

db_con = psycopg2.connect(database="investak_new", user="postgres", password="postgres", host="localhost", port="5432")
stock_cursor =  db_con.cursor()

rBSE = requests.get('http://investak.cmlinks.com/Market.svc/CompanyLiveDetails/BSE?responsetype=json')
rNSE = requests.get('http://investak.cmlinks.com/Market.svc/CompanyLiveDetails/NSE?responsetype=json')

stockDetailsBSE = rBSE.json()
stockDetailsNSE = rNSE.json()

print("Updating BSE stocks details:")
for itemBSE in stockDetailsBSE['response']['data']['Companylist']['company']:
	coCode = itemBSE['co_code']
	lname = itemBSE['lname'] if itemBSE['lname'] != None else ""
	dayopen = itemBSE['dayopen'] if itemBSE['dayopen'] != None else 0.00
	dayhigh = itemBSE['dayhigh'] if itemBSE['dayhigh'] != None else 0.00
	daylow = itemBSE['daylow'] if itemBSE['daylow'] != None else 0.00
	ltp = itemBSE['ltp'] if itemBSE['ltp'] != None else 0.00
	dayvolume = itemBSE['dayvolume'] if itemBSE['dayvolume'] != None else 0.00
	tradedate = itemBSE['tradedate'] if itemBSE['tradedate'] != None else ""
	prevclose = itemBSE['prevclose'] if itemBSE['prevclose'] != None else 0.00
	pricediff = itemBSE['pricediff'] if itemBSE['pricediff'] != None else 0.00
	perchange = itemBSE['perchange'] if itemBSE['perchange'] != None else 0.00
	series = itemBSE['series'] if itemBSE['series'] != None else 0.00
	# print(coCode + ' : ' + lname + ' : ' + ltp)
	#stock_cursor.execute("INSERT INTO api_stock_update (co_code, exchange, day_open, day_high, day_low, ltp) VALUES (" + coCode + ", 'BSE', " + str(dayopen) + ", " + str(dayhigh) + ", " + str(daylow) + ", " + str(ltp) + ") ON CONFLICT (co_code, exchange) DO UPDATE SET day_open = EXCLUDED.day_open, day_high = EXCLUDED.day_high, day_low = EXCLUDED.day_low, ltp = EXCLUDED.ltp;")
	stock_cursor.execute("SELECT COUNT(1) AS Tot_Num FROM investak_clusterdetails WHERE co_code = %s AND exchange = 'BSE'", [coCode])
	rec = stock_cursor.fetchone();
	# print("rec : " + str(rec[0]))
	if rec[0] > 0:
		stock_cursor.execute("UPDATE investak_clusterdetails SET co_name = '" + str(lname) + "', day_open = " + str(dayopen) + ", day_high = " + str(dayhigh) + ", day_low = " + str(daylow) + ", ltp = " + str(ltp) + ", day_volume = " + str(dayvolume) + ", trade_date = '" + str(tradedate) + "', prev_close = " + str(prevclose) + ", price_diff = " + str(pricediff) + ", per_change = " + str(perchange) + ", series = '" + str(series) + "' WHERE co_code = %s AND exchange = 'BSE'", [coCode])
		print("Updated for BSE, Co_Code: " + coCode + ", Name : " + lname)
	# break;
db_con.commit()

print("\n\n")
print("Updating NSE stocks details:")
for itemNSE in stockDetailsNSE['response']['data']['Companylist']['company']:
	coCodeNSE = itemNSE['co_code']
	lnameNSE = itemNSE['lname'] if itemNSE['lname'] != None else ""
	dayopenNSE = itemNSE['dayopen'] if itemNSE['dayopen'] != None else 0.00
	dayhighNSE = itemNSE['dayhigh'] if itemNSE['dayhigh'] != None else 0.00
	daylowNSE = itemNSE['daylow'] if itemNSE['daylow'] != None else 0.00
	ltpNSE = itemNSE['ltp'] if itemNSE['ltp'] != None else 0.00
	dayvolumeNSE = itemNSE['dayvolume'] if itemNSE['dayvolume'] != None else 0.00
	tradedateNSE = itemNSE['tradedate'] if itemNSE['tradedate'] != None else ""
	prevcloseNSE = itemNSE['prevclose'] if itemNSE['prevclose'] != None else 0.00
	pricediffNSE = itemNSE['pricediff'] if itemNSE['pricediff'] != None else 0.00
	perchangeNSE = itemNSE['perchange'] if itemNSE['perchange'] != None else 0.00
	seriesNSE = itemNSE['series'] if itemNSE['series'] != None else 0.00
	# print(coCodeNSE + ' : ' + lnameNSE + ' : ' + ltpNSE)
	#stock_cursor.execute("INSERT INTO api_stock_update (co_code, exchange, day_open, day_high, day_low, ltp) VALUES (" + coCodeNSE + ", 'NSE', " + str(dayopenNSE) + ", " + str(dayhighNSE) + ", " + str(daylowNSE) + ", " + str(ltpNSE) + ") ON CONFLICT (co_code, exchange) DO UPDATE SET day_open = EXCLUDED.day_open, day_high = EXCLUDED.day_high, day_low = EXCLUDED.day_low, ltp = EXCLUDED.ltp;")
	stock_cursor.execute("SELECT COUNT(1) AS Tot_Num FROM investak_clusterdetails WHERE co_code = %s AND exchange = 'NSE'", [coCodeNSE])
	rec = stock_cursor.fetchone();
	# print("rec : " + str(rec[0]))
	if rec[0] > 0:
		stock_cursor.execute("UPDATE investak_clusterdetails SET co_name = '" + str(lnameNSE) + "', day_open = " + str(dayopenNSE) + ", day_high = " + str(dayhighNSE) + ", day_low = " + str(daylowNSE) + ", ltp = " + str(ltpNSE) + ", day_volume = " + str(dayvolumeNSE) + ", trade_date = '" + str(tradedateNSE) + "', prev_close = " + str(prevcloseNSE) + ", price_diff = " + str(pricediffNSE) + ", per_change = " + str(perchangeNSE) + ", series = '" + str(seriesNSE) + "' WHERE co_code = %s AND exchange = 'NSE'", [coCodeNSE])
		print("Updated for NSE, Co_Code: " + coCodeNSE + ", Name : " + lnameNSE)
	# break;
db_con.commit()
print "Done"

