import sys
import psycopg2
import os
import requests
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "investak_new.settings")
from django.db import connection

print "Hello"
db_con = psycopg2.connect(database="investak_dev", user="investak", password="investak2016", host="localhost", port="5432")
cursor = db_con.cursor()

cursor.execute("SELECT DISTINCT(co_code),exchange FROM investak_clusterdetails ORDER BY co_code DESC")
records = cursor.fetchall()

i=0
for record in records:
    stock_cursor =  db_con.cursor()
    print str(record[0])+'/'+str(record[1]);
    #print 'http://investak.cmlinks.com/Market.svc/CompanyDetails/'+str(record[0])+'/'+record[1]+'?responsetype=json&callback='
    r = requests.get('http://investak.cmlinks.com/Market.svc/CompanyDetails/'+str(record[0])+'/'+str(record[1])+'?responsetype=json&callback=')
    stock_details = r.json()
    #print stock_details
    if 'data' in stock_details['response']:
        day_open = stock_details['response']['data']['Companylist']['company']['dayopen'] if stock_details['response']['data']['Companylist']['company']['dayopen'] != None else 0.00
        day_high = stock_details['response']['data']['Companylist']['company']['dayhigh'] if stock_details['response']['data']['Companylist']['company']['dayhigh'] != None else 0.00
        day_low = stock_details['response']['data']['Companylist']['company']['daylow'] if stock_details['response']['data']['Companylist']['company']['daylow'] != None else 0.00
        ltp = stock_details['response']['data']['Companylist']['company']['ltp'] if stock_details['response']['data']['Companylist']['company']['ltp'] != None else 0.00
        mcap = stock_details['response']['data']['Companylist']['company']['mcap'] if stock_details['response']['data']['Companylist']['company']['mcap'] != None else 0.00
        pe = stock_details['response']['data']['Companylist']['company']['pe'] if stock_details['response']['data']['Companylist']['company']['pe'] != None else 0.00
        eps = stock_details['response']['data']['Companylist']['company']['eps'] if stock_details['response']['data']['Companylist']['company']['eps'] != None else 0.00
        div_yield = stock_details['response']['data']['Companylist']['company']['divyield'] if stock_details['response']['data']['Companylist']['company']['divyield'] != None else 0.00
        sector_name = stock_details['response']['data']['Companylist']['company']['sectorname'] if stock_details['response']['data']['Companylist']['company']['sectorname'] != None else ''
        r1_mret = stock_details['response']['data']['Companylist']['company']['r1mret'] if stock_details['response']['data']['Companylist']['company']['r1mret'] != None else 0.00
        r6_mret = stock_details['response']['data']['Companylist']['company']['r6mret'] if stock_details['response']['data']['Companylist']['company']['r6mret'] != None else 0.00
        r1_yearret = stock_details['response']['data']['Companylist']['company']['r1yearret'] if stock_details['response']['data']['Companylist']['company']['r1yearret'] != None else 0.00
        
        # check if data exists
        stock_cursor.execute("select COUNT(*) as tot_num from api_stock_update where co_code = %s", [record[0]])
        rec = stock_cursor.fetchone();
        if rec[0] > 0:
            stock_cursor.execute("UPDATE api_stock_update SET \"day_open\"="+str(day_open)+",\"day_high\"="+str(day_high)+",\"day_low\"="+str(day_low)+",\"ltp\"="+str(ltp)+",\"mcap\"="+str(mcap)+",\"pe\"="+str(pe)+",\"eps\"="+str(eps)+",\"div_yield\"="+str(div_yield)+",\"sector_name\"='"+str(sector_name)+"',\"r1_mret\"="+str(r1_mret)+",\"r6_mret\"="+str(r6_mret)+",\"r1_yearret\"="+str(r1_yearret)+" WHERE \"co_code\" = %s", [record[0]])
        else:
            stock_cursor.execute("INSERT INTO api_stock_update (\"exchange\",\"day_open\",\"day_high\",\"day_low\",\"ltp\",\"mcap\",\"pe\",\"eps\",\"div_yield\",\"sector_name\",\"r1_mret\",\"r6_mret\",\"r1_yearret\",\"co_code\") values ('"+str(record[1])+"',"+str(day_open)+","+str(day_high)+","+str(day_low)+","+str(ltp)+","+str(mcap)+","+str(pe)+","+str(eps)+","+str(div_yield)+",'"+str(sector_name)+"',"+str(r1_mret)+","+str(r6_mret)+","+str(r1_yearret)+","+str(record[0])+")")
        db_con.commit()
    if i % 25 == 0: 
        print i
        # update cluster_details
        cursor.execute("UPDATE investak_clusterdetails SET \"day_open\" = api_stock_update.\"day_open\", \"day_high\" = api_stock_update.\"day_high\", \"day_low\" = api_stock_update.\"day_low\", \"ltp\" = api_stock_update.\"ltp\", \"mcap\" = api_stock_update.\"mcap\", \"pe\" = api_stock_update.\"pe\", \"eps\" = api_stock_update.\"eps\", \"div_yield\" = api_stock_update.\"div_yield\", \"sector_name\" = api_stock_update.\"sector_name\", \"r1_mret\" = api_stock_update.\"r1_mret\", \"r6_mret\" = api_stock_update.\"r6_mret\", \"r1_yearret\" = api_stock_update.\"r1_yearret\" from api_stock_update where investak_clusterdetails.\"co_code\" = api_stock_update.\"co_code\"");
        #break;
    i=i+1
print "Done"

