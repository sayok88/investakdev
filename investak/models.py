import os, sys
from django.db import models
import datetime
from django.utils import timezone


class Category(models.Model):
    cat_name = models.CharField(max_length=200)
    p_id = models.IntegerField(default=0)
    status = models.IntegerField(default=1)
    date_created = models.CharField(max_length=100)
    is_deleted = models.SmallIntegerField(default=0)


    def __unicode__(self):
        return self.id

class Company(models.Model):
    BSEcode = models.CharField(max_length=200)
    co_code = models.IntegerField(default=0)
    co_name = models.CharField(max_length=200)
    symbol = models.CharField(max_length=200)
    isin = models.CharField(max_length=200, default='0')
    bselisted = models.CharField(max_length=1, default='')
    nselisted = models.CharField(max_length=1, default='')
    bsetrading = models.CharField(max_length=1, default='')
    nsetrading = models.CharField(max_length=1, default='')
    company_short_name = models.CharField(max_length=200, default='')

    def __unicode__(self):
        return self.co_code


class Cluster(models.Model):
    cluster_name = models.CharField(max_length=200)
    cluster_category = models.IntegerField(default=0)
    cluster_sub_category = models.IntegerField(default=0)
    public_cluster = models.IntegerField(default=0)
    cluster_status = models.SmallIntegerField(default=0)
    cluster_desc = models.TextField(default='')
    cluster_image = models.CharField(max_length=200, default='')
    posted_by = models.CharField(max_length=5, default='')
    created_date = models.CharField(max_length=50, default='')
    created_by = models.IntegerField(default=0) #max_length=10, 
    total_stock = models.IntegerField(default=0) #max_length=10, 
    total_stock_price = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    short_desc = models.TextField(default='')
    additional_desc = models.TextField(default='')
    research_desc = models.TextField(default='')
    weight_desc = models.TextField(default='')
    rebal_freq = models.CharField(default='', max_length=200)
    recently_viewed = models.IntegerField(default=0)
    owner_id = models.IntegerField(default=0)
    is_deleted = models.SmallIntegerField(default=0)
    is_published = models.SmallIntegerField(default=1)

    def __unicode__(self):
        return self.id

class ClusterDetails(models.Model):
    cluster_id = models.ForeignKey(Cluster, on_delete=models.CASCADE, related_name='cluster_id')
    co_code = models.IntegerField(default=0)
    co_name = models.CharField(max_length=200)
    symbol = models.CharField(max_length=200, default='')
    day_open = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    day_high = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    day_low = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    bid_price = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    ask_price = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    ltp = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    day_volume = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    trade_date = models.CharField(max_length=100, null=True)
    prev_close = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    price_diff = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    per_change = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    beta = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    hi_52_wk = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    lo_52_wk = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    avg_vol_3m = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    mcap = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    pe = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    eps = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    div_yield = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    sector_name = models.CharField(max_length=100, null=True)
    r1_mret = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    r3_mret = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    r6_mret = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    r1_yearret = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    r5_yearret = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    weightage = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    exchange = models.CharField(max_length=5, default='')
    stock_information = models.TextField(default='')

    def __unicode__(self):
        return self.cluster_id

class ClusterLog(models.Model):
    cluster_id = models.IntegerField(default=0)
    cluster_name = models.CharField(max_length=200)
    cluster_category = models.IntegerField(default=0)
    cluster_sub_category = models.IntegerField(default=0)
    public_cluster = models.IntegerField(default=0)
    cluster_status = models.SmallIntegerField(default=0)
    cluster_desc = models.TextField(default='')
    cluster_image = models.CharField(max_length=200, default='')
    posted_by = models.CharField(max_length=5, default='')
    created_date = models.CharField(max_length=50, default='')
    created_by = models.IntegerField(default=0) #max_length=10, 
    total_stock = models.IntegerField(default=0) #max_length=10, 
    total_stock_price = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    short_desc = models.TextField(default='')
    additional_desc = models.TextField(default='')
    research_desc = models.TextField(default='')
    weight_desc = models.TextField(default='')
    rebal_freq = models.CharField(default='', max_length=200)
    recently_viewed = models.IntegerField(default=0)
    owner_id = models.IntegerField(default=0)
    updated_date = models.DateTimeField(default=datetime.datetime.now)

    def __unicode__(self):
        return self.id

class ClusterDetailsLog(models.Model):
    log_id = models.IntegerField(default=0)
    cluster_id = models.IntegerField(default=0)
    co_code = models.IntegerField(default=0)
    co_name = models.CharField(max_length=200)
    symbol = models.CharField(max_length=200, default='')
    day_open = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    day_high = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    day_low = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    bid_price = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    ask_price = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    ltp = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    day_volume = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    trade_date = models.CharField(max_length=100, null=True)
    prev_close = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    price_diff = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    per_change = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    beta = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    hi_52_wk = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    lo_52_wk = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    avg_vol_3m = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    mcap = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    pe = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    eps = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    div_yield = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    sector_name = models.CharField(max_length=100, null=True)
    r1_mret = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    r3_mret = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    r6_mret = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    r1_yearret = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    r5_yearret = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    weightage = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    exchange = models.CharField(max_length=5, default='')
    stock_information = models.TextField(default='')

    def __unicode__(self):
        return self.log_id


class User(models.Model):
    full_name = models.CharField(max_length=200)
    email_id = models.CharField(max_length=200)
    password = models.CharField(max_length=200)
    dob = models.CharField(default='', max_length=200)
    user_type = models.CharField(max_length=1, default='G')
    reg_type = models.CharField(max_length=1, default='I')
    status = models.IntegerField(default=1) #max_length=1, 
    created_date = models.DateTimeField(default=datetime.datetime.now)
    profile_img = models.CharField(default='', max_length=200)
    social_img = models.CharField(default='', max_length=500)

    def __unicode__(self):
        return self.email_id


class WatchList(models.Model):
    cluster_id = models.ForeignKey(Cluster, related_name='cluster_watch_list')
    user_id = models.ForeignKey(User, related_name='user_watch_list')
    watch_date = models.CharField(max_length=100)

    def __unicode__(self):
        return self.cluster_id


class LikeCluster(models.Model):
    cluster_id = models.ForeignKey(Cluster, related_name='cluster_like_list')
    user_id = models.ForeignKey(User, related_name='user_like_list')

    def __unicode__(self):
        return self.cluster_id


class CommentOnCluster(models.Model):
    cluster_id = models.ForeignKey(Cluster, related_name='cluster_comment_list')
    user_id = models.ForeignKey(User, related_name='user_comment_list')
    comment_description = models.TextField(default='')
    comment_date = models.CharField(max_length=100)
    status = models.IntegerField(default=0)

    def __unicode__(self):
        return self.cluster_id


class CompanyDirectors(models.Model):
    co_code = models.CharField(max_length=50, default='')
    board_member_name = models.CharField(max_length=200)
    designation = models.CharField(max_length=200)
    cd_year = models.CharField(max_length=50, default='')

    def __unicode__(self):
        return self.id


class CompanyPerformance(models.Model):
    co_code = models.CharField(max_length=200, default=0)
    company_background = models.TextField(default='')

    def __unicode__(self):
        return self.id


class CompanyContactInfo(models.Model):
    co_code = models.CharField(max_length=50, default='')
    reg_add_1 = models.TextField(default='')
    reg_add_2 = models.TextField(default='')
    district = models.CharField(max_length=100, default='')
    state = models.CharField(max_length=100, default='')
    pin_code = models.CharField(max_length=100, default='')
    tel_1 = models.CharField(max_length=100, default='')
    tel_2 = models.CharField(max_length=100, default='')
    fax = models.CharField(max_length=100, default='')
    email = models.CharField(max_length=200, default='')
    website = models.CharField(max_length=200, default='')

    def __unicode__(self):
        return self.id


class AllClusters(models.Model):
    cluster = models.ForeignKey(Cluster, on_delete=models.CASCADE, related_name='all_cluster_details')
    index_value_m1 = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    index_value_m3 = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    index_value_m6 = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    index_value_y1 = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    index_value_y5 = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    div_yield_M1 = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    div_yield_M3 = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    div_yield_M6 = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    div_yield_Y1 = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    div_yield_Y5 = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    r1_mret = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    r3_mret = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    r6_mret = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    r1_yearret = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    r5_yearret = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    market_value = models.DecimalField(max_digits=100, decimal_places=2, default=0.00)
    indexvalue = models.DecimalField(max_digits=100, decimal_places=15, default=0.00)
    previndexvalue = models.DecimalField(max_digits=100, decimal_places=15, default=0.00)
    def __unicode__(self):
        return self.cluster



class FaqTopic(models.Model):
    topic_name = models.CharField(max_length=200)
    status = models.IntegerField(default=1)
    created_date = models.DateTimeField(default=datetime.datetime.now)

    def __unicode__(self):
        return self.id


class Faq(models.Model):
    faq_topic = models.ForeignKey(FaqTopic, on_delete=models.CASCADE, related_name='faq_topic')
    faq_qsn = models.CharField(max_length=200)
    faq_ans = models.TextField(default='')
    faq_status = models.SmallIntegerField(default=1)
    created_date = models.CharField(max_length=200, default='')

    def __unicode__(self):
        return self.id


class Testimonial(models.Model):
    short_desc = models.CharField(default='',max_length=200)
    long_desc = models.TextField(default='')
    status = models.SmallIntegerField(default=1)
    created_date = models.DateTimeField(default=datetime.datetime.now)

    def __unicode__(self):
        return self.id


class Partner(models.Model):
    image = models.CharField(default='',max_length=200)
    link = models.CharField(default='',max_length=200)
    status = models.SmallIntegerField(default=1)
    created_date = models.DateTimeField(default=datetime.datetime.now)

    def __unicode__(self):
        return self.id


class Team(models.Model):
    profile_name = models.CharField(default='',max_length=200)
    designation = models.CharField(default='',max_length=200)
    long_desc = models.TextField(default='')
    profile_image = models.CharField(default='',max_length=200)
    status = models.SmallIntegerField(default=1)
    created_date = models.DateTimeField(default=datetime.datetime.now)

    def __unicode__(self):
        return self.id


class Customize(models.Model):
    heading = models.CharField(default='',max_length=200)
    description = models.TextField(default='')
    image = models.CharField(default='',max_length=200)
    status = models.SmallIntegerField(default=1)
    created_date = models.DateTimeField(default=datetime.datetime.now)

    def __unicode__(self):
        return self.id


class Sitesetting(models.Model):
    description = models.TextField(default='')
    phone = models.CharField(default='',max_length=200)
    email = models.CharField(default='',max_length=200)
    address = models.TextField(default='')
    about_us = models.TextField(default='')
    about_footer = models.TextField(default='')
    status = models.SmallIntegerField(default=1)
    created_date = models.DateTimeField(default=datetime.datetime.now)
    banner_main_heading = models.CharField(default='',max_length=200)
    banner_sub_heading = models.CharField(default='',max_length=200)
    banner_text = models.TextField(default='')
    aboutus_customer = models.IntegerField(default=0) #max_length=10, 
    aboutus_aum = models.CharField(default='',max_length=200)
    aboutus_page1_heading = models.CharField(default='',max_length=200)
    aboutus_page2_heading = models.CharField(default='',max_length=200)
    aboutus_page3_heading = models.CharField(default='',max_length=200)
    aboutus_page4_heading = models.CharField(default='',max_length=200)
    aboutus_page5_heading = models.CharField(default='',max_length=200)
    footer_copyright = models.TextField(default='')
    footer_sub_heading = models.TextField(default='')
    terms_conditions = models.TextField(default='')
    privacy_policy = models.TextField(default='')

    def __unicode__(self):
        return self.id


class Analyze(models.Model):
    heading = models.CharField(default='',max_length=200)
    description = models.TextField(default='')
    image = models.CharField(default='',max_length=200)
    status = models.SmallIntegerField(default=1)
    created_date = models.DateTimeField(default=datetime.datetime.now)

    def __unicode__(self):
        return self.id


class Optimize(models.Model):
    heading = models.CharField(default='',max_length=200)
    description = models.TextField(default='')
    image = models.CharField(default='',max_length=200)
    status = models.SmallIntegerField(default=1)
    created_date = models.DateTimeField(default=datetime.datetime.now)

    def __unicode__(self):
        return self.id


class RecentlyViewedCluster(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='recently_viewed')
    cluster = models.ForeignKey(Cluster, on_delete=models.CASCADE, related_name='recently_viewed_cluster')
    viewed_date = models.DateTimeField(default=datetime.datetime.now)


class UserNotification(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_notification')
    notification_text = models.TextField(default='')
    notification_date = models.DateTimeField(default=datetime.datetime.now)
    notification_type = models.CharField(max_length=1, default='A')
    comment_id = models.IntegerField(default=0)
    status = models.SmallIntegerField(default=0)

    def __unicode__(self):
        return self.id


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_order')
    cluster = models.ForeignKey(Cluster, on_delete=models.CASCADE, related_name='cluster_order')
    cluster_name = models.CharField(max_length=250, default='')
    order_date = models.DateTimeField(default=datetime.datetime.now)
    no_of_stock = models.IntegerField(default=0)
    minimum_investment = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    transaction_cost = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    rebalance_required = models.CharField(max_length=20, default='NO')
    rebalance_id = models.IntegerField(default=0)
    rebalanced_date = models.DateTimeField(blank=True, null=True)
    order_buy_id = models.IntegerField(default=0)
    order_hold_id = models.IntegerField(default=0)
    sold_out = models.CharField(max_length=10, default='NO')
    # added for LIVE order#
    # order_type = 'virtual' or 'live'#
    # live_order_status = 'complete' or 'repair' or 'archive'
    order_type = models.CharField(max_length=20, default='virtual')
    live_order_status = models.CharField(max_length=10, default='')
    live_order_id = models.IntegerField(default=0)
    live_order_buy_id = models.IntegerField(default=0)
    live_order_sell_id = models.IntegerField(default=0)
    live_order_hold_id = models.IntegerField(default=0)
    live_order_repair_id = models.IntegerField(default=0)
    live_order_archive_id = models.IntegerField(default=0)
    broker_name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.id


class OrderDetails(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='order_details')
    co_code = models.CharField(max_length=200, default='')
    co_name = models.CharField(max_length=200, default='')
    no_of_share = models.CharField(max_length=10, default='')
    price = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    current_unit_price = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    value = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    old_weigh = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    new_weigh = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    exchange = models.CharField(max_length=5, default='')
    symbol = models.CharField(max_length=200, default='')

    def __unicode__(self):
        return self.id

class OrderBuy(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_buy')
    cluster = models.ForeignKey(Cluster, on_delete=models.CASCADE, related_name='cluster_buy')
    cluster_name = models.CharField(max_length=250, default='')
    order = models.ForeignKey(Order, related_name='buy_order')
    order_date = models.DateTimeField(default=datetime.datetime.now)
    order_track_id = models.IntegerField(default=0)
    no_of_stock = models.IntegerField(default=0)
    minimum_investment = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)

    def __unicode__(self):
        return self.id

class OrderBuyDetails(models.Model):
    buy = models.ForeignKey(OrderBuy, on_delete=models.CASCADE, db_index=True)
    order = models.ForeignKey(Order, default=0.00)
    co_code = models.CharField(max_length=200, default='')
    co_name = models.CharField(max_length=200, default='')
    unit_share = models.IntegerField(default=0)
    no_of_share = models.CharField(max_length=10, default='')
    price = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    current_unit_price = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    value = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    old_weigh = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    new_weigh = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    exchange = models.CharField(max_length=5, default='')
    symbol = models.CharField(max_length=200)
    order_track_id = models.IntegerField(default=0)

    def __unicode__(self):
        return self.id

class OrderSell(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_sell')
    cluster = models.ForeignKey(Cluster, on_delete=models.CASCADE, related_name='cluster_sell')
    order = models.ForeignKey(Order, related_name='sell_of_order')
    sell_date = models.DateTimeField(default=datetime.datetime.now)
    no_of_stock = models.IntegerField(default=0)
    minimum_investment = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    order_track_id = models.IntegerField(default=0)
    order_buy_id = models.IntegerField(default=0)

    def __unicode__(self):
        return self.id

class OrderSellDetails(models.Model):
    sell = models.ForeignKey(OrderSell, on_delete=models.CASCADE, db_index=True)
    order = models.ForeignKey(Order, default=0.00)
    co_code = models.CharField(max_length=200, default='')
    co_name = models.CharField(max_length=200, default='')
    unit_share = models.IntegerField(default=0)
    bought_unit_price = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    unit_price = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    no_of_share = models.IntegerField(default=0)
    sell_value = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    exchange = models.CharField(max_length=5, default='')
    symbol = models.CharField(max_length=200, default='')
    order_track_id = models.IntegerField(default=0)

    def __unicode__(self):
        return self.id

class OrderHold(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_hold')
    cluster = models.ForeignKey(Cluster, on_delete=models.CASCADE, related_name='cluster_hold')
    order = models.ForeignKey(Order, related_name='hold_order')
    hold_date = models.DateTimeField(default=datetime.datetime.now)
    no_of_stock = models.IntegerField(default=0)
    minimum_investment = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    order_track_id = models.IntegerField(default=0)

    def __unicode__(self):
        return self.id

class OrderHoldDetails(models.Model):
    hold = models.ForeignKey(OrderHold, on_delete=models.CASCADE, related_name='hold_details')
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='order_hold_details')
    co_code = models.CharField(max_length=200, default='')
    co_name = models.CharField(max_length=200, default='')
    unit_share = models.IntegerField(default=0)
    unit_price = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    current_price = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    no_of_share = models.IntegerField(default=0)
    bought_value = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    exchange = models.CharField(max_length=5, default='')
    symbol = models.CharField(max_length=200, default='')
    hold_order_date = models.DateTimeField(blank=True, null=True)
    order_track_id = models.IntegerField(default=0)

    def __unicode__(self):
        return self.id

class OrderHoldDisplay(models.Model):
    hold = models.ForeignKey(OrderHold, on_delete=models.CASCADE, related_name='hold_display')
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='order_hold_display')
    co_code = models.CharField(max_length=200, default='')
    co_name = models.CharField(max_length=200, default='')
    unit_share = models.IntegerField(default=0)
    unit_price = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    current_price = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    no_of_share = models.IntegerField(default=0)
    bought_value = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    exchange = models.CharField(max_length=5, default='')
    symbol = models.CharField(max_length=200, default='')
    hold_order_date = models.DateTimeField(blank=True, null=True)
    order_track_id = models.IntegerField(default=0)

    def __unicode__(self):
        return self.id

class OrderTrack(models.Model):
    order = models.ForeignKey(Order, related_name='track_order')
    order_date = models.DateTimeField(default=datetime.datetime.now)
    no_of_lots = models.IntegerField(default=0)
    order_type = models.CharField(max_length=50, default='')
    order_buy_id = models.IntegerField(default=0)
    order_sell_id = models.IntegerField(default=0)
    order_buy_new_id = models.IntegerField(default=0)
    order_hold_id = models.IntegerField(default=0)
    order_hold_new_id = models.IntegerField(default=0)
    rebalance_id = models.IntegerField(default=0)
    minimum_investment = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    # -----for live data-------#
    live_order_id = models.IntegerField(default=0)
    live_order_buy_id = models.IntegerField(default=0)
    live_order_old_id = models.IntegerField(default=0)

    def __unicode__(self):
        return self.id

class Banner(models.Model):
    banner_image = models.CharField(default='',max_length=200)
    status = models.SmallIntegerField(default=1)
    created_date = models.DateTimeField(default=datetime.datetime.now)

    def __unicode__(self):
        return self.id

class OrderAccountGraph(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_graph')
    account_value = models.FloatField(blank=True, null=True)
    return_value = models.FloatField(blank=True, null=True)
    order_date = models.DateField(default=datetime.date.today)
    order_type = models.CharField(max_length=20, default='virtual')

    def __unicode__(self):
        return self.id


class OrderMail(models.Model):
    order_id = models.IntegerField(default=0)
    order_date = models.DateTimeField(blank=True, null=True)
    segment = models.CharField(default='',max_length=200)
    instrument = models.CharField(default='EQ',max_length=200)
    symbol = models.CharField(default='',max_length=200)
    option_type = models.CharField(default='',max_length=200)
    stricke_price =  models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    expriy_date = models.DateField(blank=True, null=True)
    price = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    qty = models.FloatField(default=0.00, blank=True, null=True)
    disclosed_qty = models.FloatField(default=0.00, blank=True, null=True)
    order_type = models.CharField(default='',max_length=20)
    order_order = models.CharField(default='LIMIT',max_length=100)
    triger_price = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    cli_pro = models.CharField(default='CLI',max_length=20)
    product_type = models.CharField(default='CNC',max_length=200)
    validity = models.CharField(default='DAY',max_length=100)
    client_code = models.IntegerField(default=0)
    remark = models.CharField(default='',max_length=100)
    validity_date = models.DateField(blank=True, null=True)
    participant_code = models.CharField(default='',max_length=100)
    validity_time = models.DateTimeField(blank=True, null=True)
    market_participant = models.CharField(default='',max_length=200)
    if_sent = models.CharField(default='',max_length=10)


    def __unicode__(self):
        return self.id

class Rebalance(models.Model):
    cluster_id = models.IntegerField(default=0)
    cluster_name = models.CharField(max_length=250, default='')
    order = models.ForeignKey(Order, related_name='rebalance_order')
    rebalanced_date = models.DateTimeField(blank=True, null=True)
    order_buy_id = models.IntegerField(default=0)
    order_buy_new_id = models.IntegerField(default=0)
    order_track_id = models.IntegerField(default=0)
    no_of_lots = models.IntegerField(default=0)
    rebalance_investment = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)

    def __unicode__(self):
        return self.id

class RebalanceDetails(models.Model):
    rebalance = models.ForeignKey(Rebalance, on_delete=models.CASCADE, db_index=True)
    order = models.ForeignKey(Order, default=0.00)
    co_code = models.CharField(max_length=200, default='')
    co_name = models.CharField(max_length=200, default='')
    exchange = models.CharField(max_length=5, default='')
    symbol = models.CharField(max_length=200)
    rebalance_type = models.CharField(max_length=50, default='')
    no_of_share = models.CharField(max_length=10, default='')
    unit_share = models.IntegerField(default=0)
    unit_price = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    rebalance_amount = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    rebalanced_date = models.DateTimeField(blank=True)

    def __unicode__(self):
        return self.id

class Subscribe(models.Model):
    email = models.CharField(max_length=200)
    created_date = models.DateTimeField(default=datetime.datetime.now)
    
    def __unicode__(self):
        return self.id

class Newsletter(models.Model):
    templatename = models.CharField(max_length=200)
    subject_name = models.CharField(max_length=200)
    text_desc = models.TextField(default='')
    #text_desc = RichTextUploadingField('contents')
    created_date = models.DateTimeField(default=datetime.datetime.now)
    status = models.SmallIntegerField(default=1)
    source = models.CharField(max_length=200, default='', blank=True, null=True)

    def __unicode__(self):
        return self.id


class NewsletterToUser(models.Model):
    news_letter = models.ForeignKey(Newsletter, on_delete=models.CASCADE, related_name='news_letter')
    user_email = models.CharField(max_length=200)
    created_date = models.DateTimeField(default=datetime.datetime.now)
    status = models.SmallIntegerField(default=1)

    def __unicode__(self):
        return self.id        

class Newsroom(models.Model):
    heading = models.CharField(default='',max_length=200)
    description = models.TextField(default='')
    image = models.CharField(default='',max_length=200)
    status = models.SmallIntegerField(default=1)
    created_date = models.DateTimeField(default=datetime.datetime.now)
    link = models.CharField(default='',max_length=200)
    
    def __unicode__(self):
        return self.id


class ClusterTemplateEmailLog(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_email_log')
    cluster = models.ForeignKey(Cluster, on_delete=models.CASCADE, related_name='cluster_email_details')
    send_status = models.SmallIntegerField(default=0)
    user_email_id = models.CharField(max_length=200, default='')

    def __unicode__(self):
        return self.id


class Country(models.Model):
    country_name = models.CharField(max_length=100, default='')
    status = models.SmallIntegerField(default=1)

    def __unicode__(self):
        return self.id


class Bankproof(models.Model):
    bank_proof_name = models.CharField(max_length=100, default='')
    status = models.SmallIntegerField(default=1)

    def __unicode__(self):
        return self.id



class GrossAnnualIncome(models.Model):
    income_name = models.CharField(max_length=100, default='')
    status = models.SmallIntegerField(default=1)

    def __unicode__(self):
        return self.id

    

class IdentityProof(models.Model):
    proof_name = models.CharField(max_length=100, default='')
    status = models.SmallIntegerField(default=1)

    def __unicode__(self):
        return self.id


class Education(models.Model):
    type_name = models.CharField(max_length=100, default='')
    status = models.SmallIntegerField(default='')

    def __unicode__(self):
        return self.id


class Nationality(models.Model):
    type_name = models.CharField(max_length=100, default='')
    status = models.SmallIntegerField(default=1)

    def __unicode__(self):
        return self.id


class ResidentialStatus(models.Model):
    type_name = models.CharField(max_length=100, default='')
    status = models.SmallIntegerField(default=1)

    def __unicode__(self):
        return self.id


class Occupation(models.Model):
    occupation_name = models.CharField(max_length=100, default='')
    status = models.SmallIntegerField(default=1)

    def __unicode__(self):
        return self.id


class AddressProof(models.Model):
    type_name = models.CharField(max_length=100, default='')
    status = models.SmallIntegerField(default=1)

    def __unicode__(self):
        return self.id


class AccountType(models.Model):
    type_name = models.CharField(max_length=100, default='')
    status = models.SmallIntegerField(default=1)

    def __unicode__(self):
        return self.id


class State(models.Model):
    state_name = models.CharField(max_length=100, default='')
    status = models.SmallIntegerField(default=1)

    def __unicode__(self):
        return self.id


class IncomeSource(models.Model):
    type_name = models.CharField(max_length=100, default='')
    status = models.SmallIntegerField(default=1)

    def __unicode__(self):
        return self.id


class UserTradingAccount(models.Model):
    full_name = models.CharField(max_length=100, default='')
    father_name = models.CharField(max_length=100, default='')
    mother_name = models.CharField(max_length=100, default='')
    date_of_birth = models.CharField(max_length=12, default='')
    gender = models.CharField(max_length=6, default='')
    marital_status = models.CharField(max_length=12, default='')
    occupation = models.ForeignKey(Occupation, related_name='occupation')
    cst_number = models.CharField(max_length=30, default='')
    pan_number = models.CharField(max_length=10, default='')
    adhar_number = models.CharField(max_length=100, default='')
    mobile_no = models.CharField(max_length=10, default='')
    email_id = models.CharField(max_length=100, default='')
    gross_annual_income = models.ForeignKey(GrossAnnualIncome, related_name='gross_annual_income')
    net_worth = models.CharField(max_length=100, default='')
    income_source = models.ForeignKey(IncomeSource, related_name='income_source', default=1)
    invest_exp = models.CharField(max_length=100, default='')
    identity_proof = models.ForeignKey(IdentityProof, related_name='identity_proof')
    residential_status = models.ForeignKey(ResidentialStatus, related_name='residential_status')
    education = models.ForeignKey(Education, related_name='education')
    nationality = models.ForeignKey(Nationality, related_name='nationality')
    address = models.TextField(default='')
    state = models.ForeignKey(State, related_name='state', default=1)
    city = models.CharField(max_length=100, default='')
    country = models.ForeignKey(Country, related_name='country')
    zipcode = models.CharField(max_length=10, default='')
    address_proof = models.ForeignKey(AddressProof, related_name='address_proof')
    c_address = models.TextField(default='')
    c_state = models.ForeignKey(State, related_name='c_state', blank=True, null=True)
    c_city = models.CharField(max_length=100, default='')
    c_country = models.ForeignKey(Country, related_name='c_country', blank=True, null=True)
    c_zipcode = models.CharField(max_length=10, default='')
    c_address_proof = models.ForeignKey(AddressProof, related_name='c_address_proof', blank=True, null=True)
    bank_name = models.CharField(max_length=50, default='')
    account_type = models.ForeignKey(AccountType, related_name='account_type')
    ifsc_code = models.CharField(max_length=20, default='')
    account_no = models.CharField(max_length=50, default='')
    branch_name = models.CharField(max_length=100, default='')
    bank_proof = models.ForeignKey(Bankproof, related_name='bank_proof')
    company_name = models.CharField(max_length=50, default='')
    company_address = models.TextField(default='')
    nominee_name = models.CharField(max_length=100, default='')
    nominee_relationship = models.CharField(max_length=100, default='')
    nominee_contact_no = models.CharField(max_length=50, default='')
    nominee_pan_number = models.CharField(max_length=10, default='')
    guardian_name = models.CharField(max_length=50, default='')
    guardian_address = models.TextField(default='')
    guardian_contact_no = models.CharField(max_length=50, default='')
    user = models.ForeignKey(User, related_name='user_trading_account', default=1)
    application_date = models.DateTimeField(default=datetime.datetime.now)
    application_status = models.SmallIntegerField(default=0)
    client_referral = models.SmallIntegerField(default=0)
    client_name = models.CharField(max_length=100, default='')
    client_id = models.CharField(max_length=100, default='')
    client_mobile_no = models.CharField(max_length=20, default='')
    executive_referral = models.SmallIntegerField(default=0)
    executive_name = models.CharField(max_length=100, default='')
    executive_id = models.CharField(max_length=100, default='')
    executive_mobile_no = models.CharField(max_length=100, default='')
    tso_linked = models.CharField(max_length=10, default='NO')

    def __unicode__(self):
        return self.id


class UserTradingDocument(models.Model):
    user = models.ForeignKey(User, related_name='user_trading_document')
    trading = models.ForeignKey(UserTradingAccount, related_name='trading_account', default=1)
    pan_upload = models.CharField(max_length=100, default='')
    adhar_upload = models.CharField(max_length=100, default='')
    cancelled_cheque_upload = models.CharField(max_length=100, default='')
    bank_statement_upload = models.CharField(max_length=100, default='')
    present_address_proof_upload = models.CharField(max_length=100, default='')
    correspondence_address_proof_upload = models.CharField(max_length=100, default='')
    ipv_record = models.TextField(default='')
    signature_upload = models.CharField(max_length=100, default='')

    def __unicode__(self):
        return self.id

class SeasonGreetings(models.Model):
    heading = models.CharField(max_length=100, default='')
    subject = models.CharField(max_length=100, default='')
    description = models.TextField(default='')
    created_date = models.DateTimeField(default=datetime.datetime.now)

    def __unicode__(self):
        return self.id

class GreetingsToUser(models.Model):
    season_greetings = models.ForeignKey(SeasonGreetings, on_delete=models.CASCADE, related_name='season_greetings')
    user_email = models.CharField(max_length=200)
    created_date = models.DateTimeField(default=datetime.datetime.now)

    def __unicode__(self):
        return self.id  


class LiveUser(models.Model):
    live_user_id = models.CharField(max_length=20, default='')
    initial_token = models.TextField(default='')
    img_src = models.TextField(default='')
    access_token = models.TextField(default='')
    live_session_id = models.CharField(max_length=50, default='')
    p_arr = models.TextField(default='')
    brk_name = models.TextField(default='')
    s_prdt_ali = models.TextField(default='')
    branch_id = models.CharField(max_length=100, default='')
    account_id = models.CharField(max_length=100, default='')
    account_name = models.CharField(max_length=100, default='')
    ex_arr = models.CharField(max_length=100, default='')
    or_arr = models.CharField(max_length=100, default='')
    email = models.CharField(max_length=100, default='')
    currency_details = models.TextField(default='')
    derivative_details = models.TextField(default='')
    commodity_details = models.TextField(default='')
    equity_details = models.TextField(default='')
    user = models.ForeignKey(User, related_name='live_user_account', null=True)

    def __unicode__(self):
        return self.id

class LiveUserAccountDetails(models.Model):
    live_user_id = models.CharField(max_length=20, default='')
    bank_address = models.TextField(default='')
    bank_name = models.CharField(default='', max_length=100)
    dob_account = models.CharField(default='', max_length=100)
    cell_addr = models.CharField(default='', max_length=100)
    account_status = models.CharField(default='', max_length=100)
    bank_account_no = models.CharField(default='', max_length=100)
    email_addr = models.CharField(default='', max_length=100)
    bank_branch_name = models.CharField(default='', max_length=100)
    exch_enabled = models.CharField(default='', max_length=200)
    account_type = models.CharField(default='', max_length=100)
    address = models.TextField(default='')
    pan_no = models.CharField(default='', max_length=10)
    account_name = models.CharField(default='', max_length=100)
    account_id = models.CharField(default='', max_length=100)
    user = models.ForeignKey(User, related_name='live_user_account_details', null=True)

    def __unicode__(self):
        return self.id

class LiveUserProfileDetails(models.Model):
    live_user_id = models.CharField(max_length=20, default='')
    dp_name = models.CharField(max_length=100, default='')
    nse_pcode = models.CharField(max_length=100, default='')
    addr = models.TextField(default='')
    bank_addr = models.TextField(default='')
    depo = models.CharField(max_length=100, default='')
    bb_name = models.CharField(max_length=100, default='')
    cell_adr = models.CharField(max_length=20, default='')
    bank_name = models.CharField(max_length=100, default='')
    offc_addr = models.TextField(default='')
    bse_pcode = models.CharField(default='', max_length=100)
    dp_id = models.CharField(default='', max_length=50)
    dp_num = models.CharField(default='', max_length=50)
    bank_accid = models.CharField(default='', max_length=50)
    email = models.CharField(default='', max_length=100)
    pan = models.CharField(default='', max_length=10)
    name = models.CharField(default='', max_length=100)
    user = models.ForeignKey(User, related_name='live_user_profile_details', null=True)

    def __unicode__(self):
        return self.id

class KycSendEmailToUser(models.Model):
    email_id = models.CharField(max_length=150, default='')
    subject = models.CharField(max_length=100, default='')
    message = models.TextField(default='')
    send_date = models.DateField(default=datetime.date.today)
    user = models.ForeignKey(User, related_name='kyc_send_email_to_user', null=True)

    def __unicode__(self):
        return self.id

class LiveOrder(models.Model):
    virtual_user_id = models.IntegerField(default=0)
    virtual_order_id = models.IntegerField(default=0)
    virtual_order_track_id = models.IntegerField(default=0)
    virtual_cluster_id = models.IntegerField(default=0)
    order_type = models.CharField(max_length=20, default='BUY')

    accountid = models.CharField(max_length=200, default='')
    uid = models.CharField(max_length=200, default='')
    status = models.CharField(max_length=200, default='')
    order_date = models.DateTimeField(default=datetime.datetime.now)
    minimum_investment = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    transaction_cost = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    no_of_lots = models.IntegerField(default=0)

    def __unicode__(self):
        return self.id

class ApiPlaceOrder(models.Model):
    virtual_user_id = models.IntegerField(default=0)
    virtual_cluster_id = models.IntegerField(default=0)
    live_order_id = models.IntegerField(default=0)
    virtual_status = models.CharField(max_length=200, default='')

    co_code = models.CharField(max_length=200, default='')
    symbol = models.CharField(max_length=200, default='')
    no_of_share = models.IntegerField(default=0)
    unit_price = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    sell_price = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)

    uid = models.CharField(max_length=200, default='')
    accountid = models.CharField(max_length=200, default='')
    status = models.CharField(max_length=200, default='')

    amo = models.CharField(max_length=200, default='')
    brokerclient = models.CharField(max_length=200, default='')
    datedays = models.IntegerField(default=0)
    discqty = models.IntegerField(default=0)
    exch = models.CharField(max_length=200, default='')
    mktpro = models.IntegerField(default=0)
    minqty = models.IntegerField(default=0)
    naiccode = models.CharField(max_length=200, default='')
    ordersource = models.CharField(max_length=200, default='')
    orderno = models.CharField(max_length=200, default='')
    order_date = models.DateTimeField(default=datetime.datetime.now)
    pcode = models.CharField(max_length=200, default='EQUITY')
    possquareflg = models.CharField(max_length=200, default='')
    price = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    prctyp = models.CharField(max_length=200, default='')
    qty = models.IntegerField(default=0)
    ret = models.CharField(max_length=200, default='')
    rejectionreason = models.CharField(max_length=200, default='')
    s_prdt_ali = models.CharField(max_length=200, default='')
    tokenno = models.CharField(max_length=200, default='')
    ttranstype = models.CharField(max_length=200, default='')
    trigprice = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    tsym = models.CharField(max_length=200, default='')
    usertag = models.CharField(max_length=200, default='WEB')
    symbolname = models.CharField(max_length=200, default='')

    def __unicode__(self):
        return self.id
        
# Added ClusterArticle #
class ClusterArticle(models.Model):
    cluster = models.ForeignKey(Cluster, on_delete=models.CASCADE, related_name='article_cluster')
    heading = models.CharField(default='',max_length=200)
    description = models.TextField(default='')
    image = models.CharField(default='',max_length=200)
    status = models.SmallIntegerField(default=1)
    created_date = models.DateTimeField(default=datetime.datetime.now)
    link = models.CharField(default='',max_length=200)
    
    def __unicode__(self):
        return self.id

class LiveUserAngelBrokingApi(models.Model): 
    angel_id = models.CharField(default='',max_length=20)
    session_id = models.TextField(default='')
    group_id = models.CharField(default='',max_length=10)
    user_code = models.CharField(default='',max_length=10)
    login_message = models.CharField(default='',max_length=255)
    login_allowed = models.CharField(default='',max_length=255)
    trading_allowed = models.CharField(default='',max_length=255)    
    exchange_allowed = models.CharField(default='',max_length=255)
    product_types_allowed = models.CharField(default='',max_length=255)      
    client_risk_preference = models.TextField(default='')
    allied_products_allowed = models.CharField(default='0',max_length=255)   
    product_display_name = models.TextField(default='')
    second_level_authentication = models.CharField(default='0',max_length=50)      
    user_id = models.IntegerField(default=0)
    login_status = models.IntegerField(default=1)
    last_login = models.DateTimeField(default=datetime.datetime.now)
    login_id=models.CharField(null=True,max_length=20)
    user_name=models.CharField(null=True,max_length=20)
    email_id=models.EmailField(null=True)
    rm_name=models.CharField(null=True,max_length=100)
    rm_mobile=models.CharField(null=True,max_length=20)
    rm_email_id=models.EmailField(null=True)
    is_trading_client=models.CharField(null=True,max_length=2)
    mobile_number=models.CharField(null=True,max_length=14)
    phone_number=models.CharField(null=True,max_length=14)
    dob=models.DateField(null=True)
    gender=models.CharField(null=True,max_length=8)
    pan_Number=models.CharField(null=True,max_length=10)
    def __unicode__(self):
        return self.id

class ApiPlaceOrderAngelBroking(models.Model):
    virtual_user_id = models.IntegerField(default=0)
    live_order_id = models.IntegerField(default=0)
    virtual_cluster_id = models.IntegerField(default=0)
    accountid = models.CharField(max_length=50, default='')
    status = models.CharField(max_length=100, default='')
    virtual_status = models.CharField(max_length=100, default='')
    symbol = models.CharField(max_length=50, default='')
    no_of_share = models.IntegerField(default=0)
    discqty = models.IntegerField(default=0)
    exch = models.CharField(max_length=10, default='')
    orderno = models.CharField(max_length=10, default='')
    order_date = models.DateTimeField(default=datetime.datetime.now)
    price = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    order_type = models.CharField(max_length=10, default='')
    qty = models.IntegerField(default=0)
    order_validity = models.CharField(max_length=10, default='')
    rejectionreason = models.TextField(default='')
    trade_action = models.CharField(max_length=10, default='')
    trigger_price = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    trading_symbol = models.CharField(max_length=50, default='')
    symbolname = models.CharField(max_length=50, default='')
    uid = models.CharField(max_length=200, default='')
    co_code = models.CharField(max_length=200, default='')
    unit_price = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    sell_price = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    series = models.CharField(max_length=20, default='')
    instrument = models.CharField(max_length=255, default='')
    gtd = models.IntegerField(default=0)
    os_type = models.CharField(max_length=50, default='')
    os_version = models.CharField(max_length=20, default='')
    device_id = models.CharField(max_length=50, default='')
    expiry_date = models.CharField(max_length=255, default='')
    strike_price = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    product_type= models.CharField(max_length=10, default='')
    option_type = models.IntegerField(default=1)
    nest_orderno = models.CharField(max_length=50, default=0)
    gateway_orderno = models.CharField(max_length=50, default=0)
    tradebook_qty = models.IntegerField(default=0)
    tradebook_price = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    amo_orderno = models.CharField(max_length=50, default=0)
    actual_share = models.IntegerField(default=0)
    exchange_timestamp = models.CharField(max_length=50, default='')

    def __unicode__(self):
        return self.id

    def get_fields(self):
        return [(field, field.value_to_string(self)) for field in ApiPlaceOrderAngelBroking._meta.fields]


class ApiClusterUpdate(models.Model):
    cluster_id = models.IntegerField(default=0)
    index_value_m1 = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    index_value_m3 = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    index_value_m6 = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    index_value_y1 = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    index_value_y5 = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    div_yield_M1 = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    div_yield_M3 = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    div_yield_M6 = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    div_yield_Y1 = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    div_yield_Y5 = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    r1_mret = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    r3_mret = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    r6_mret = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    r1_yearret = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    r5_yearret = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    market_value = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    indexvalue = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    previndexvalue = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)

    def __unicode__(self):
        return self.id

class ApiStockUpdate(models.Model):
    exchange = models.CharField(max_length=6, default='')
    day_open = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    day_high = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    day_low = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    ltp = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    mcap = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    pe = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    eps = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    div_yield = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    sector_name = models.CharField(max_length=100, default='')
    r1_mret = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    r6_mret = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    r1_yearret = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    co_code = models.IntegerField(default=0)

    def __unicode__(self):
        return self.id



class ApiTradeBook(models.Model):
    user=models.ForeignKey(LiveUserAngelBrokingApi)
    tradeNumber=models.IntegerField()
    instrument=models.CharField(null=True,max_length=10)
    tradingSymbol=models.CharField(null=False,max_length=15)
    series=models.CharField(null=False,max_length=4)
    expiryDate=models.CharField(null=True,max_length=20)
    strikePrice=models.FloatField(default=0)
    optionType=models.CharField(null=True,max_length=10)
    bracketOrderID=models.IntegerField(null=True)
    tradeTime=models.CharField(null=True,max_length=20)
    nestOrderNumber=models.BigIntegerField(null=True)
    gatewayOrderNumber=models.IntegerField(null=True)
    clientOrderNumber=models.IntegerField(null=True)
    transactionType=models.CharField(null=True,max_length=5)
    totalQuantity=models.IntegerField()
    price=models.FloatField()
    productType=models.CharField(null=True,max_length=10)
    exchange=models.IntegerField(null=True)
    symbol=models.IntegerField(null=True)
    decimalLocator=models.IntegerField(null=True)
    participantCode=models.CharField(null=True,max_length=15)
    preOpenFlag=models.CharField(null=True,max_length=15)
    orderType=models.CharField(null=True,max_length=15)
    lotSize=models.IntegerField(null=True)
    companyName=models.CharField(null=True,max_length=30)
    def __unicode__(self):
        return self.id

    def get_fields(self):
        return [(field, field.value_to_string(self)) for field in ApiTradeBook._meta.fields]

class TransactionInitiate(models.Model):
    order_id=models.IntegerField()
    order_buy_id=models.IntegerField()
    order_hold_id=models.IntegerField()
    cluster_id=models.IntegerField()
    profit=models.FloatField()
    paid=models.BooleanField()
    minimum_investment=models.FloatField(null=True)
    no_of_unit=models.IntegerField(null=True)
    transaction_cost=models.FloatField(null=True)
    live_order_id=models.FloatField(default=0.0)

    def __unicode__(self):
        return self.id
class VirtualTransactionDump(models.Model):
    vti=models.ForeignKey(TransactionInitiate)
    amount=models.FloatField()
    dump=models.CharField(max_length=2000,null=True)
    txnid=models.CharField(max_length=20,null=True)
class AB_SubmitOrderParameters(models.Model):
    user_id=models.IntegerField()
    client_id=models.CharField(max_length=20)
    client_order_id=models.CharField(max_length=20)
    request=models.CharField(max_length=750)
    trade_action=models.CharField(max_length=5,null=True)
    def __str__(self):  # __unicode__ on Python 2
        return self.cluster_id+" "+self.client_order_id

