import sys
import psycopg2
import os
import requests
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "investak_new.settings")
from django.shortcuts import render_to_response
from django.db import connection

print "Hello"
db_con = psycopg2.connect(database="investak_dev", user="investak", password="investak2016", host="localhost", port="5432")
cursor = db_con.cursor()

cursor.execute("SELECT DISTINCT(cluster_id) FROM investak_allclusters ORDER BY cluster_id")
records = cursor.fetchall()

i=0
for record in records:
    stock_cursor = db_con.cursor()
    market_value_cursor = db_con.cursor()
    r = requests.get('http://investak.cmlinks.com/Market.svc/AllClusters/'+str(record[0])+'?responsetype=json&callback=')

    stock_details = r.json()
    rr = requests.get('http://investak.cmlinks.com/market.svc/ClusterIndexBaseInceptionDate/'+str(record[0])+'?responsetype=json')
    market_price = rr.json()

    print record[0]
    if 'data' in stock_details['response']:
        index_value_m1 = stock_details['response']['data']['Companylist']['company']['indexvalue_m1']
        index_value_m3 = stock_details['response']['data']['Companylist']['company']['indexvalue_m3']
        index_value_m6 = stock_details['response']['data']['Companylist']['company']['indexvalue_m6']
        index_value_y1 = stock_details['response']['data']['Companylist']['company']['indexvalue_y1']
        index_value_y5 = stock_details['response']['data']['Companylist']['company']['indexvalue_y5']
        div_yield_M1 = stock_details['response']['data']['Companylist']['company']['divyield_m1']
        div_yield_M3 = stock_details['response']['data']['Companylist']['company']['divyield_m3']
        div_yield_M6 = stock_details['response']['data']['Companylist']['company']['divyield_m6']
        div_yield_Y1 = stock_details['response']['data']['Companylist']['company']['divyield_y1']
        div_yield_Y5 = stock_details['response']['data']['Companylist']['company']['divyield_y5']
        r1_mret = stock_details['response']['data']['Companylist']['company']['ret_m1']
        r3_mret = stock_details['response']['data']['Companylist']['company']['ret_m3']
        r6_mret = stock_details['response']['data']['Companylist']['company']['ret_m6']
        r1_yearret = stock_details['response']['data']['Companylist']['company']['ret_y1']
        r5_yearret = stock_details['response']['data']['Companylist']['company']['ret_y5']
        market_value = market_price['response']['data']['Companylist']['company']['marketvalue']
        indexvalue = market_price['response']['data']['Companylist']['company']['indexvalue']
        previndexvalue = market_price['response']['data']['Companylist']['company']['previndexvalue']
        
        # check if data exists
        stock_cursor.execute("select COUNT(*) as tot_num from api_cluster_update where cluster_id = %s", [record[0]])
        rec = stock_cursor.fetchone();
        if rec[0] > 0:
            stock_cursor.execute("UPDATE api_cluster_update SET \"index_value_m1\"="+index_value_m1+",\"index_value_m3\"="+index_value_m3+",\"index_value_m6\"="+index_value_m6+",\"index_value_y1\"="+index_value_y1+",\"index_value_y5\"="+index_value_y5+",\"div_yield_M1\"="+div_yield_M1+",\"div_yield_M3\"="+div_yield_M3+",\"div_yield_M6\"="+div_yield_M6+",\"div_yield_Y1\"='"+div_yield_Y1+"',\"div_yield_Y5\"="+div_yield_Y5+",\"r1_mret\"="+r1_mret+",\"r3_mret\"="+r3_mret+",\"r6_mret\"="+r6_mret+",\"r1_yearret\"="+r1_yearret+",\"r5_yearret\"="+r5_yearret+",\"market_value\"="+market_value+",\"indexvalue\"="+indexvalue+",\"previndexvalue\"="+previndexvalue+" WHERE \"cluster_id\" = %s", [record[0]])
        else:
            stock_cursor.execute("INSERT INTO api_cluster_update (\"cluster_id\",\"index_value_m1\",\"index_value_m3\",\"index_value_m6\",\"index_value_y1\",\"index_value_y5\",\"div_yield_M1\",\"div_yield_M3\",\"div_yield_M6\",\"div_yield_Y1\",\"div_yield_Y5\",\"r1_mret\",\"r3_mret\",\"r6_mret\",\"r1_yearret\",\"r5_yearret\",\"market_value\",\"indexvalue\",\"previndexvalue\") values ("+str(record[0])+","+str(index_value_m1)+","+str(index_value_m3)+","+str(index_value_m6)+","+str(index_value_y1)+","+str(index_value_y5)+","+str(div_yield_M1)+","+str(div_yield_M3)+","+str(div_yield_M6)+",'"+str(div_yield_Y1)+"',"+str(div_yield_Y5)+","+str(r1_mret)+","+str(r3_mret)+","+str(r6_mret)+","+str(r1_yearret)+","+str(r5_yearret)+","+str(market_value)+","+str(indexvalue)+","+str(previndexvalue)+")")
        db_con.commit()
    if i % 25 == 0: 
        print i
        # update cluster_details
        cursor.execute("UPDATE investak_allclusters SET \"index_value_m1\" = api_cluster_update.\"index_value_m1\", \"index_value_m3\" = api_cluster_update.\"index_value_m3\", \"index_value_m6\" = api_cluster_update.\"index_value_m6\", \"index_value_y1\" = api_cluster_update.\"index_value_y1\", \"index_value_y5\" = api_cluster_update.\"index_value_y5\", \"div_yield_M1\" = api_cluster_update.\"div_yield_M1\", \"div_yield_M3\" = api_cluster_update.\"div_yield_M3\", \"div_yield_M6\" = api_cluster_update.\"div_yield_M6\", \"div_yield_Y1\" = api_cluster_update.\"div_yield_Y1\", \"div_yield_Y5\" = api_cluster_update.\"div_yield_Y5\", \"r1_mret\" = api_cluster_update.\"r1_mret\", \"r3_mret\" = api_cluster_update.\"r3_mret\", \"r6_mret\" = api_cluster_update.\"r6_mret\", \"r1_yearret\" = api_cluster_update.\"r1_yearret\", \"r5_yearret\" = api_cluster_update.\"r5_yearret\", \"market_value\" = api_cluster_update.\"market_value\", \"indexvalue\" = api_cluster_update.\"indexvalue\", \"previndexvalue\" = api_cluster_update.\"previndexvalue\" from api_cluster_update where investak_allclusters.\"id\" = api_cluster_update.\"cluster_id\"");
        #break;
    i=i+1
print "Done"
