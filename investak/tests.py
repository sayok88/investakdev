def user_rebalance_cluster(request, order_buy_id):

    ####----------------------------####
    user_rebalance_cluster_from_email(request, order_buy_id)
    ####----------------------------####

    if 'id' not in request.session:
        return render_to_response('login.html', context_instance=RequestContext(request))
    else:
        user_id = int(request.session['id'])
        cluster_name = ''
        cluster_id = 0
        order_id = 0
        current_cluster_status = []
        existing_cluster_status = []
        buy_cluster_data = []
        sell_cluster_data = []
        hold_cluster = []
        email_cluster = []
        total_buy_amount = 0
        total_sell_amount = 0
        rebalance_min_investment = 0
        latest_order_id = 0


        if order_buy_id > 0:
            order_buy = OrderBuy.objects.filter(pk=order_buy_id).first()
            if order_buy:
                order_id = order_buy.order_id
                user_id = order_buy.user_id
                order_track_id = order_buy.order_track_id

        if order_id > 0:
            latest_order = Order.objects.filter(pk=order_id, order_buy_id=order_buy_id).first()
            if latest_order:
                latest_order_id = order_id
                order = Order.objects.filter(pk=order_id).first()
                order_hold_id = order.order_hold_id
                order_row = OrderHold.objects.filter(pk=order_hold_id,order_id=order_id).first()
                cluster_id = order_row.cluster_id
                lots_limit = order_row.no_of_stock
                cluster_name = order.cluster_name

        if latest_order_id == 0:
            return HttpResponseRedirect('/portfolio/')
        else:
            if cluster_id > 0:

                if cluster_name == '':
                    cluster_name = 'Cluster Name'
                    #-----------Find Cluster Name--------------------#
                    cluster_data = Cluster.objects.filter(pk=cluster_id).first()
                    if cluster_data:
                        cluster_name = cluster_data.cluster_name

                #-----------Find Sell Date-----------------------#
                sell_date = datetime.datetime.today()
                sell_print_date = sell_date.strftime("%d %b %Y, %I:%M %p")

                #########################################################
                pp = pprint.PrettyPrinter(indent=4)
                #----------------------------------------------------#
                current_cluster_status = get_current_cluster_rebalance_status(cluster_id)
                existing_cluster_status = get_cluster_order_rebalance_status(cluster_id, order_id)

                #print '------------current_cluster----------'
                #pp.pprint(current_cluster_status)
                #print '------------rebalance_cluster_order----------'
                #pp.pprint(existing_cluster_status)

                if (len(current_cluster_status)>0) and (len(existing_cluster_status)>0):
                    buy_cluster_data,sell_cluster_data,hold_cluster,email_cluster,total_buy_amount,total_sell_amount = rebalance_sell_buy_data(order_id,current_cluster_status, existing_cluster_status)
                    #----------------------------------------------------#
                    #print '------------buy_cluster_data----------'
                    #pp.pprint(buy_cluster_data)
                    #print '------------sell_cluster_data----------'
                    #pp.pprint(sell_cluster_data)
                    #print len(buy_cluster_data),'----------',len(sell_cluster_data)
                    #----------------------------------------------------#
                    if (int(cluster_id) > 0) and ((len(buy_cluster_data)>0) or (len(sell_cluster_data)>0)):

                        total_buy_amount = float(total_buy_amount) * float(lots_limit)
                        total_sell_amount = float(total_sell_amount) * float(lots_limit)
                        rebalance_min_investment = total_buy_amount - total_sell_amount

                    else:
                        return HttpResponseRedirect('/portfolio/')
                #########################################################
                #-----------Calculate Minimum Investment-----#
                no_of_unit = 1
                if request.method == "POST":
                    no_of_unit = request.POST.get('no_of_unit', 1)
                new_min_investment_value = 0.00
                min_investment_value = 0.00

                #-----------Find Minimum Investment from OrderHoldDetails------------#
                current_stock_details = OrderBuyDetails.objects.filter(buy_id=order_buy_id,order_id=order_id).exclude(old_weigh=0).order_by('current_unit_price').reverse()
                if current_stock_details:
                    min_investment_value = math.ceil((current_stock_details[0].current_unit_price/current_stock_details[0].old_weigh)*100)
                min_investment_value = float(min_investment_value)


                #cluster_stock_details = ClusterDetails.objects.filter(cluster_id_id=cluster_id).exclude(weightage=0).order_by('ltp').reverse()
                #if cluster_stock_details:
                #    min_investment_value = math.ceil((cluster_stock_details[0].ltp/cluster_stock_details[0].weightage)*100)
                #min_investment_value = float(min_investment_value)

                #----------Calculate New Weightage-----------#
                investment_arr = []
                new_weigh_arr = []
                print '--------------order_buy_id----------',order_buy_id,'-----------order_hold_id------------',order_hold_id
                if int(order_buy_id) > 0 and int(order_hold_id) > 0:
                    current_stock_details = OrderBuyDetails.objects.filter(buy_id=order_buy_id,order_id=order_id).order_by('current_unit_price').reverse()
                    if current_stock_details:
                        for csd in current_stock_details:
                            d = float((float(csd.old_weigh) * float(min_investment_value))/100)

                            #---------Get Bought Share Unit from OrderHoldDetails Table-----#
                            order_hold_details_data = OrderHoldDetails.objects.filter(hold_id=order_hold_id, order_id=order_id, co_code=csd.co_code).first()
                            if order_hold_details_data:
                                e = int(order_hold_details_data.unit_share)
                                #--------------------------------------------------------#
                                new_min_investment_value += ((e*float(no_of_unit)) * float(csd.current_unit_price))
                                #----------Get Symbol----------------#
                                co_code = csd.co_code

                                if csd.symbol:
                                    symbol = csd.symbol
                                else:
                                    company = Company.objects.filter(co_code=co_code).first()
                                    symbol = company.symbol
                                #------------------------------------#
                                #----------Get Exchange--------------#
                                co_code = csd.co_code
                                if csd.exchange:
                                    exchange = csd.exchange
                                else:
                                    exchange = 'NSE'
                                    #print '----------exchange----------',exchange
                                    cluster = ClusterDetails.objects.filter(co_code=co_code,cluster_id_id=cluster_id).first()
                                    if cluster:
                                        exchange = cluster.exchange
                                #------------------------------------#
                                investment_arr.append({
                                    'co_code': csd.co_code,
                                    'co_name': csd.co_name,
                                    'd': d,
                                    'value_since_share_fraction': float(csd.current_unit_price),
                                    'no_of_share': (e*float(no_of_unit)),
                                    'value': ((e*float(no_of_unit)) * float(csd.current_unit_price)),
                                    'old_weightage': csd.old_weigh,
                                    'exchange': exchange,
                                    'symbol': symbol
                                })

                    if len(investment_arr) > 0:
                        for ia in investment_arr:
                            new_weigh_value = 0.00
                            if int(new_min_investment_value) > 0:
                                new_weigh_value = float(ia['value']/new_min_investment_value)*100
                            new_weigh_arr.append({
                                'new_weigh_value': new_weigh_value
                            })
                zip_list = zip(investment_arr, new_weigh_arr)

                #---------Find Transaction Cost----------#
                turnover = float(new_min_investment_value)
                stt = turnover * (0.10 / 100)
                sebi = turnover * (0.0001 / 100)
                stamp_duty = turnover * (0.01 / 100)
                swach_bharat = turnover * (0.5 / 100)
                transaction_cost = stt + sebi + stamp_duty + swach_bharat

                #--------Find Cash Available--------#
                cash_available = calculate_cash_available(user_id) #calling function for cash in hand#
                #-----------------------------------#
                expected_cash_flow = float(new_min_investment_value)
                final_balance = cash_available + expected_cash_flow - transaction_cost
                #-----------------------------------#
                return render_to_response('rebalance_cluster.html', {'sell_order_list': zip_list,
                                          #'minimum_investment': (math.ceil(new_min_investment_value*float(no_of_unit))),
                                          'minimum_investment': new_min_investment_value,
                                          'int_minimum_investment': int(new_min_investment_value),
                                          'display_minimum_investment': '{:0,.2f}'.format(new_min_investment_value),
                                          'order_id': order_id,
                                          'order_buy_id': order_buy_id,
                                          'cluster_id': cluster_id,
                                          'no_of_unit': int(no_of_unit),
                                          'lots_limit': range(lots_limit),
                                          'no_of_lots': lots_limit,
                                          'cluster_name': cluster_name,
                                          'sell_print_date': sell_print_date,
                                          'transaction_cost': '{:0,.2f}'.format(transaction_cost),
                                          'cash_available': cash_available,
                                          'expected_cash_flow':  '{:0,.2f}'.format(expected_cash_flow),
                                          'display_cash_available': '{:0,.2f}'.format(cash_available),
                                          'final_balance': '{:0,.2f}'.format(final_balance),
                                          'buy_cluster_data': buy_cluster_data,
                                          'sell_cluster_data': sell_cluster_data,
                                          'cluster_status': current_cluster_status,
                                          'clusterlog_status': existing_cluster_status,
                                          'total_sell_amount': total_sell_amount,
                                          'total_buy_amount': total_buy_amount,
                                          'rebalance_min_investment': rebalance_min_investment},
                                          context_instance=RequestContext(request))