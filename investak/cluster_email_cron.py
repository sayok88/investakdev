import os, sys
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "investak_new.settings")
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from django.core.wsgi import get_wsgi_application

os.environ['DJANGO_SETTINGS_MODULE'] = 'investak_new.settings'
application = get_wsgi_application()

from django.template.loader import get_template
from django.template import Context
from django.core.mail import EmailMultiAlternatives
from django.conf import settings

from investak.models import ClusterTemplateEmailLog, Cluster, ClusterDetails, User, AllClusters

cluster_email_template_details = ClusterTemplateEmailLog.objects.filter(send_status=0).order_by('id')
if cluster_email_template_details.count() > 0:
    for cetd in cluster_email_template_details:
        cluster_general_details = Cluster.objects.get(pk=int(cetd.cluster_id))
        cluster_stock_details_email = ClusterDetails.objects.filter(cluster_id_id=int(cetd.cluster_id)).order_by('co_code')
        user_details = User.objects.get(pk=int(cluster_general_details.created_by))
        all_cluster_details = AllClusters.objects.filter(cluster_id=int(cetd.cluster_id))
        full_name = user_details.full_name.split(" ")
        html_content = get_template('cluster_email_template.html').render(
            Context({
                     'cluster_id': cluster_general_details.id,
                     'cluster_name': cluster_general_details.cluster_name,
                     'cluster_description': cluster_general_details.cluster_desc,
                     'cluster_image': cluster_general_details.cluster_image,
                     'one_year_return': all_cluster_details[0].r1_yearret,
                     'five_year_return': all_cluster_details[0].r5_yearret,
                     'cluster_stock_details': cluster_stock_details_email,
                     'user_name': user_details.full_name}))

        text_content = '...'
        msg = EmailMultiAlternatives("Check this new cluster : "+str(cluster_general_details.cluster_name)+" (by "+str(full_name[0])+")", text_content, 'Investak '+settings.CONTACT_EMAIL, [str(cetd.user_email_id)])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        print cetd.user_email_id
        ClusterTemplateEmailLog.objects.get(pk=cetd.id).delete() 