from investak.models import *
from django.db.models import Q
from django.conf import settings
import requests
from django.core.cache import cache
import datetime
from datetime import date

def site_year(request):
    return {'site_year' : date.today().year}

def sitesettings(request):
    # Create fixed data structures to pass to template
    # data could equally come from database queries
    # web services or social APIs
    settingList     = Sitesetting.objects.all()
    AllEditSetList  = []
    for se in settingList:
        AllEditSetList.append({
                        'id'          : se.id,
                        'description' : se.description,
                        'phone'       : se.phone,
                        'email'       : se.email,
                        'address'     : se.address,
                        'banner_main_heading'    : se.banner_main_heading,
                        'banner_sub_heading'     : se.banner_sub_heading,
                        'banner_text' : se.banner_text,
                        'about_us'    : se.about_us,
                        'aboutus_customer'    : se.aboutus_customer,
                        'aboutus_aum'    : se.aboutus_aum,
                        'aboutus_page1_heading'    : se.aboutus_page1_heading,
                        'aboutus_page2_heading'    : se.aboutus_page2_heading,
                        'aboutus_page3_heading'    : se.aboutus_page3_heading,
                        'aboutus_page4_heading'    : se.aboutus_page4_heading,
                        'aboutus_page5_heading'    : se.aboutus_page5_heading,
                        'footer_copyright': se.footer_copyright,
                        'footer_sub_heading': se.footer_sub_heading,
                        'about_footer': se.about_footer,
                        'status'      : se.status,
                        'created_date': se.created_date,
                    })
    return {'AllEditSetList': AllEditSetList}


def notifications(request):
    user_id = request.session.get('id', 0)
    notification_arr = []
    notification_shortlist = []
    user_name = ''
    cluster_id = 0
    cluster_name = ''
    link = ''
    style =''
    noti_cnt = 0
    counter = 0
    cluster_id_arr = []
    if int(user_id) > 0:
        total_notification_details = UserNotification.objects.filter(Q(user_id=int(request.session['id'])) & (Q(notification_type='A') | Q(notification_type='R')) | (Q(notification_type='G') & ~Q(user_id=int(request.session['id'])))).filter(status=0).order_by('-id')
        request.session['total_notification'] = int(total_notification_details.count())


        notification_details = UserNotification.objects.filter(Q(user_id=int(request.session['id'])) & (Q(notification_type='A') | Q(notification_type='R')) | (Q(notification_type='G') & ~Q(user_id=int(request.session['id'])))).order_by('-notification_date')
        request.session['notification_range'] = int(notification_details.count())
        for nd in notification_details:
            if nd.notification_type == "A" or nd.notification_type == "R":
                user_name = "Investak"
                image_name = settings.SITE_URL+'static/media/images/clusters.png'
            else:
                user_details = User.objects.get(pk=nd.user_id)
                user_name = user_details.full_name
                if user_details.profile_img != "":
                    image_name = settings.SITE_URL+'static/media/images/profile/'+str(user_details.profile_img)
                else:
                    image_name = settings.SITE_URL+'static/investak/images/user_name_icon.png'
            date1 = nd.notification_date.replace(tzinfo=None)
            if int(nd.comment_id) > 0:
                #-------------comments----------#
                try:
                    if nd.notification_type == 'G':
                        comment_on_cluster = CommentOnCluster.objects.get(pk=int(nd.comment_id))
                        if comment_on_cluster:
                            cluster_name = comment_on_cluster.cluster_id.cluster_name
                            cluster_image = settings.SITE_URL+"static/media/images/cluster/"+comment_on_cluster.cluster_id.cluster_image
                            cluster_id = comment_on_cluster.cluster_id_id
                            link = '/cluster_list/'+str(cluster_id)+'/'+str(nd.comment_id)
                            style = ''
                        else:
                            cluster_id = 0
                            cluster_image = ""
                            link = 'javascript:void(0)'
                            style = 'pointer-events: none!important;'
                    #-------------rebalance-------------#
                    if nd.notification_type == 'R':
                        cluster_id = nd.comment_id
                        check_array = cluster_id in cluster_id_arr
                        if check_array:
                            counter = counter + 1
                        else:
                            counter = 0
                            cluster_id_arr.append(cluster_id)
                        #print cluster_id, counter
                        cluster_order_details = Order.objects.filter(cluster_id=int(cluster_id), user_id=int(request.session['id']), sold_out='NO')[int(counter):1]
                        if int(cluster_order_details.count()) > 0:
                            link = '/rebalance_cluster/'+str(cluster_order_details[0].order_buy_id)
                            style = ''
                        else:
                            cluster_details = Cluster.objects.get(pk=int(nd.comment_id))
                            cluster_name = cluster_details.cluster_name
                            cluster_image = settings.SITE_URL+"static/media/images/cluster/"+cluster_details.cluster_image
                            link = '/buy_cluster/'+str(cluster_id)
                            style = ''
                except:
                    pass
                #-----------------------------------#
            else:
                cluster_id = 0
                link = 'javascript:void(0)'
                style = 'pointer-events: none!important;'

            notification_arr.append({
                'notification_text': nd.notification_text,
                'user_name': user_name,
                'image': image_name,
                'weekday_name': date1.strftime("%A"),
                'month_name': date1.strftime("%b"),
                'posted_time': date1.strftime('%I:%M%p').lstrip('0'),
                'day_name': date1.day,
                'notification_type': nd.notification_type,
                'cluster_id': cluster_id,
                'notification_date' : date1.strftime("%d-%m-%Y"),
                'link': link,
                'style': style,
                'cluster_name': cluster_name
            })

            if int(noti_cnt) < 5:
                notification_shortlist.append({
                'notification_text': nd.notification_text,
                'user_name': user_name,
                'image': image_name,
                'weekday_name': date1.strftime("%A"),
                'month_name': date1.strftime("%b"),
                'posted_time': date1.strftime('%I:%M%p').lstrip('0'),
                'day_name': date1.day,
                'notification_type': nd.notification_type,
                'cluster_id': cluster_id,
                'notification_date' : date1.strftime("%d-%m-%Y"),
                'link': link,
                'style': style,
                'cluster_name': cluster_name
            })
            noti_cnt = noti_cnt + 1

        request.session['user_notification'] = notification_arr
        request.session['notification_shortlist'] = notification_shortlist
        return {'user_notification': notification_arr}
    else:
        return {'user_notification': notification_arr}

def get_current_path(request):
    return {
       'current_path': request.get_full_path()
     }

def request_path(request):
    return {
        'request_path' : request.path
    }     
    
def is_login(request):
    if 'id' in request.session:
        return { 'is_login':True }
    else:
        return { 'is_login':False }

def login_user_details(request):
    user_id = request.session.get('id', 0)

    LgnUserDetailsList  = []
    if int(user_id) > 0:
        if 'id' in request.session and request.session['id'] != '' and 'angel_id' in request.session and request.session['angel_id'] != '':
            user=LiveUserAngelBrokingApi.objects.filter(angel_id=request.session['angel_id'])
            if user:
                user_details=user.first()
                se=User.objects.get(id=user_details.user_id)
                if user_details.dob != '' or user_details.dob != None:  
                    try:
                        dob = datetime.datetime.strptime(str(user_details.dob) , '%d/%m/%Y')
                    except:
                        try:
                            dob = datetime.datetime.strptime(str(user_details.dob) , '%Y-%m-%d')
                        except:
                            try:
                                dob = datetime.datetime.strptime(str(user_details.dob) , '%d-%m-%Y')
                            except:
                                dob=datetime.datetime.now()
                    dob = dob.replace(tzinfo=None)
                    dob = dob.strftime("%d/%m/%Y")
                else:
                    dob = ''
                if user_details.angel_id != "" and user_details.angel_id != None and request.session["toggle_button_status"] == "live":
                    full_name = user_details.angel_id
                elif user_details.user_name != '' and user_details.user_name != None and request.session["toggle_button_status"] == "virtual":
                    full_name = user_details.user_name
                else:
                    full_name = ''
                if user_details.email_id != '' and user_details.email_id != None:
                    email_id = user_details.email_id
                else:
                    email_id = ''
                LgnUserDetailsList.append({
                    'full_name': full_name,
                    'email_id': email_id,
                    'dob': dob,
                    'status': se.status,
                    'created_date': se.created_date,
                    'phone':user_details.phone_number,
                    'mobile':user_details.mobile_number,
                    'gender':user_details.gender,
                    'pan':user_details.pan_Number,
                })
            return {'LgnUserDetailsList': LgnUserDetailsList}
        else:
            user_details = User.objects.filter(id=int(request.session['id']))
            image_url = ''
            for se in user_details:
                if se.profile_img != "":
                    image_url = settings.SITE_URL+"static/media/images/profile/"+str(se.profile_img)
                elif se.social_img != "":
                    image_url = se.social_img
                else:
                    image_url = settings.SITE_URL+"static/investak/images/user_icon.jpg"
                if se.dob != '' or se.dob != None:    
                    try:
                        dob = datetime.datetime.strptime(str(se.dob) , '%d/%m/%Y')
                    except:
                        try:
                            dob = datetime.datetime.strptime(str(se.dob) , '%Y-%m-%d')
                        except:
                            try:
                                dob = datetime.datetime.strptime(str(user_details.dob) , '%d-%m-%Y')
                            except:
                                dob=datetime.datetime.now()                    
                    dob = dob.replace(tzinfo=None)
                    dob = dob.strftime("%d/%m/%Y")
                else:
                    dob = ''
                if se.full_name != '' and se.full_name != None:
                    full_name = se.full_name
                else:
                    full_name = ''
                if se.email_id != '' and se.email_id != None:
                    email_id = se.email_id
                else:
                    email_id = '' 
                LgnUserDetailsList.append({
                                'full_name'   : full_name,
                                'email_id'    : email_id,
                                'dob'         : dob,
                                'profile_img' : image_url,
                                'status'      : se.status,
                                'created_date': se.created_date,
                            })
            return {'LgnUserDetailsList': LgnUserDetailsList}
    else:
        return {'LgnUserDetailsList': LgnUserDetailsList}

def my_built_cluster(request):
    my_cluster_list_arr = []
    if 'id' in request.session:
        result = cache.get('my_built_cluster')
        if result is None:
            my_cluster_list = Cluster.objects.filter(created_by=int(request.session['id']), owner_id=int(request.session['id'])).filter(Q(is_deleted=0) | Q(is_deleted=2)).count() #[0:2]

            '''for item in my_cluster_list:
                user_details = User.objects.get(pk=item.created_by)
                watch_list_status=WatchList.objects.filter(cluster_id=item.id, user_id=int(request.session['id'])).count()
                my_cluster_list_arr.append({
                    'cluster_id': item.id,
                    'cluster_name': item.cluster_name,
                    'cluster_image': item.cluster_image,
                    'one_year_return': AllClusters.objects.filter(cluster_id=item.id).values('r1_yearret'),
                    'created_by': user_details.full_name.split(" ")[0],
                    'watch_list_status':watch_list_status
                })'''
            cache.set('my_built_cluster', my_cluster_list, settings.CACHE_TIME) #my_cluster_list_arr
            print "Cache Set"
            return {'my_built_cluster': my_cluster_list} #my_cluster_list_arr
        else:
            print "Cache Set Already"
            return {'my_built_cluster': cache.get('my_built_cluster')}
    else:
        return {'my_built_cluster': 0}

def my_watch_list(request):
    my_watch_list_arr = []
    if 'id' in request.session:
        result = cache.get('my_watch_list')
        if result is None:
            my_watch_list = WatchList.objects.select_related().filter(user_id_id=int(request.session['id'])).count() #[0:2]
            '''for wc in my_watch_list:
                cluster_details = Cluster.objects.filter(id=wc.cluster_id_id)
                if int(cluster_details.count()) > 0:
                    watch_list_status=WatchList.objects.filter(cluster_id=wc.cluster_id_id, user_id=int(request.session['id'])).count()
                    user_details = User.objects.get(pk=cluster_details[0].created_by)
                    my_watch_list_arr.append({
                        'cluster_id': cluster_details[0].id,
                        'cluster_name': cluster_details[0].cluster_name,
                        'cluster_image': cluster_details[0].cluster_image,
                        'one_year_return': AllClusters.objects.filter(cluster_id=cluster_details[0].id).values('r1_yearret'),
                        'created_by': user_details.full_name.split(" ")[0],
                        'watch_list_status':watch_list_status
                    })'''
            cache.set('my_watch_list', my_watch_list, settings.CACHE_TIME) #my_watch_list_arr
            return {'my_watch_list': my_watch_list} #my_watch_list_arr
        else:
            return {'my_watch_list': cache.get('my_watch_list')}
    else:
        return {'my_watch_list': 0} #my_watch_list_arr


def top_rating_cluster_by_month(request):
    top_rating_cluster_by_month_arr = []
    if 'id' in request.session:
        result = cache.get('thirty_day_winners')
        if result is None:
            top_rating_cluster_by_month = AllClusters.objects.select_related().order_by('r1_mret').reverse().count() #[0:30]

            '''for item in top_rating_cluster_by_month:
                cluster_details = Cluster.objects.filter(id=item.cluster_id, is_published=1, public_cluster=1,is_deleted=0)
                if len(cluster_details) > 0:
                    watch_list_status=WatchList.objects.filter(cluster_id=item.cluster_id, user_id=int(request.session['id'])).count()
                    top_rating_cluster_by_month_arr.append({
                        'cluster_id': cluster_details[0].id,
                        'cluster_name': cluster_details[0].cluster_name,
                        'cluster_image': cluster_details[0].cluster_image,
                        'cluster_desc': cluster_details[0].cluster_desc,
                        'user_details': User.objects.get(pk=cluster_details[0].created_by),
                        'one_month_return': item.r1_mret,
                        'watch_list_status':watch_list_status

                    })'''
            cache.set('thirty_day_winners', top_rating_cluster_by_month, settings.CACHE_TIME) #top_rating_cluster_by_month_arr
            return {'thirty_day_winners': top_rating_cluster_by_month} #top_rating_cluster_by_month_arr
        else:
            return {'thirty_day_winners': cache.get('thirty_day_winners')}
    else:
        return {'thirty_day_winners': 0} #top_rating_cluster_by_month_arr


def bottom_rating_cluster_by_month(request):
    bottom_rating_cluster_month_arr = []
    if 'id' in request.session:
        result = cache.get('thirty_day_loosers')
        if result is None:
            bottom_rating_cluster_month = AllClusters.objects.select_related().order_by('r1_mret').count() #[0:30]
            '''for item in bottom_rating_cluster_month:
                cluster_details = Cluster.objects.filter(id=item.cluster_id, is_published=1, public_cluster=1, is_deleted=0)
                if len(cluster_details) > 0:
                    watch_list_status=WatchList.objects.filter(cluster_id=item.cluster_id, user_id=int(request.session['id'])).count()
                    bottom_rating_cluster_month_arr.append({
                        'cluster_id': cluster_details[0].id,
                        'cluster_name': cluster_details[0].cluster_name,
                        'cluster_image': cluster_details[0].cluster_image,
                        'cluster_desc': cluster_details[0].cluster_desc,
                        'user_details': User.objects.get(pk=cluster_details[0].created_by),
                        'one_month_return': item.r1_mret,
                        'watch_list_status':watch_list_status
                    })'''
            cache.set('thirty_day_loosers', bottom_rating_cluster_month, settings.CACHE_TIME) #bottom_rating_cluster_month_arr
            return {'thirty_day_loosers': bottom_rating_cluster_month} #bottom_rating_cluster_month_arr
        else:
            return {'thirty_day_loosers': cache.get('thirty_day_loosers')}
    else:
        return {'thirty_day_loosers': 0} #bottom_rating_cluster_month_arr


def user_dashboard_drilldown(request):
    drilldown_graph_arr = []
    if 'id' in request.session:
        cat_obj_arr = Cluster.objects.filter(cluster_status=1, is_deleted=0).order_by('cluster_category').reverse()

        for cat in cat_obj_arr:
            category_details = Category.objects.get(pk=cat.cluster_category)
            cluster_return_details = AllClusters.objects.filter(cluster_id=cat.id)
            one_month_return = 0.00
            if cluster_return_details.count() > 0:
                one_month_return = cluster_return_details[0].r1_mret
            else:
                one_month_return = 0.00
            drilldown_graph_arr.append({
                'cat_id': category_details.id,
                'cat_name': category_details.cat_name,
                'cluster_name': cat.cluster_name,
                'cluster_id': cat.id,
                'cluster_image': cat.cluster_image,
                'recently_viewed': cat.recently_viewed,
                'one_month_return': one_month_return,
                'cluster_details': Cluster.objects.filter(cluster_category=int(category_details.id), is_deleted=0).values('id','cluster_name','cluster_image','total_stock_price', 'recently_viewed')
                })
        return {'drilldown_graph_arr': drilldown_graph_arr}
    else:
        return {'drilldown_graph_arr': drilldown_graph_arr}


def amazon_assets_url(request):
    return {'ASSETS_URL': settings.ASSETS_URL}

def investak_base_url(request):
    return {'BASE_URL': settings.SITE_URL}

def trading_application_status_old(request):
    if 'id' in request.session:
        user_trading = UserTradingAccount.objects.filter(user_id=int(request.session['id']))
        if int(user_trading.count()) > 0:
            return {'user_trading_application_status': user_trading[0].application_status}
        else:
            return {'user_trading_application_status': 0}
    else:
        return {'user_trading_application_status': 0}
def api_broker_linked_status(request):
    if 'id' in request.session:
        if 'user_code' in request.session and request.session["user_code"] != "":
            return {'broker_linked_status': 'YES', 'investak_broker_code': request.session['investak_broker_code'], 'broker_name' : 'Angel Broking'} 
        else:
            return {'broker_linked_status': 'NO', 'investak_broker_code': '', 'broker_name' : ''}
    else:
        return {'user_trading_application_status': 0, 'broker_linked_status': 'NO'}
def trading_application_status(request):
    if 'id' in request.session:
        user_trading_document = UserTradingDocument.objects.filter(user_id=int(request.session['id']))
        if int(user_trading_document.count()) > 0:
            user_trading = UserTradingAccount.objects.filter(user_id=int(request.session['id']))
            if int(user_trading.count()) > 0:
                return {'user_trading_application_status': user_trading[0].application_status, 'tso_linked_status': user_trading[0].tso_linked}
            else:
                return {'user_trading_application_status': 0, 'tso_linked_status': 'NO'}
        else:
            return {'user_trading_application_status': 0, 'tso_linked_status': 'NO'}
        '''if 'user_code' in request.session:
            return {'user_trading_application_status': 0, 'broker_linked_status': 'YES', 'broker_name': 'Angel Broking'} 
        else:
            return {'user_trading_application_status': 0, 'broker_linked_status': 'YES', 'broker_name': 'TSO'}''' 
    else:
        return {'user_trading_application_status': 0, 'tso_linked_status': 'NO'}

def api_broker_info(request):
    user_profile_details_arr = []
    account_profile_details_arr = []
    if 'id' in request.session and 'angel_id' in request.session:
        user_profile_details_arr.append({
            'stat': '1'
            })
        account_profile_details_arr.append({
            'stat': '1'
            })
    else:
        user_profile_details_arr.append({
            'stat': '0'
            })
        account_profile_details_arr.append({
            'stat': '0'
            })
    return {'user_profile_details_arr': user_profile_details_arr, 'account_profile_details_arr': account_profile_details_arr}

def user_broker_info(request):
    user_profile_details_arr = []
    account_profile_details_arr = []
    ##############----------JAYATI---------##############
    api_url = settings.API_ORDER_URL
    api_user_profile_url = str(api_url) + '/user_profile/'
    api_account_info_url = str(api_url) + '/account_info/'
    dpname = ''
    addr = ''
    bankaddr = ''
    depo = ''
    bbname = ''
    celladr = ''
    bankname = ''
    offcaddr = ''
    bsePcode = ''
    dpid = ''
    dpnum = ''
    nsePcode = ''
    nsePcode = ''
    bankaccid = ''
    email = ''
    pan = ''
    name = ''
    #############-------------------------###############
    if 'initial_token' in request.session and 'client_id' in request.session and 'access_token' in request.session:
        user_profile_json = requests.post(api_user_profile_url, json={"actid": request.session['client_id']}, headers={"Authorization": request.session['access_token'], "Content-Type": "application/json"})
        account_profile_json = requests.post(api_account_info_url, json={"acctId": request.session['client_id']}, headers={"Authorization": request.session['access_token'], "Content-Type": "application/json"})
        user_profile_details = user_profile_json.json()
        account_profile_details = account_profile_json.json()

        if user_profile_details['stat'] == "Ok":

            if "dpname" in user_profile_details:
                dpname = user_profile_details['dpname']
            if "addr" in user_profile_details:
                addr = user_profile_details['addr']
            if "bankaddr" in user_profile_details:
                bankaddr = user_profile_details['bankaddr']
            if "depo" in user_profile_details:
                depo = user_profile_details['depo']
            if "bbname" in user_profile_details:
                bbname = user_profile_details['bbname']
            if "celladr" in user_profile_details:
                celladr = user_profile_details['celladr']
            if "bankname" in user_profile_details:
                bankname = user_profile_details['bankname']
            if "offcaddr" in user_profile_details:
                offcaddr = user_profile_details['offcaddr']
            if "bsePcode" in user_profile_details:
                bsePcode = user_profile_details['bsePcode']
            if "dpid" in user_profile_details:
                dpid = user_profile_details['dpid']
            if "dpnum" in user_profile_details:
                dpnum = user_profile_details['dpnum']
            if "nsePcode" in user_profile_details:
                nsePcode = user_profile_details['nsePcode']
            if "bankaccid" in user_profile_details:
                bankaccid = user_profile_details['bankaccid']
            if "email" in user_profile_details:
                email = user_profile_details['email']
            if "pan" in user_profile_details:
                pan = user_profile_details['pan']
            if "email" in user_profile_details:
                name = user_profile_details['name']

            user_profile_details_arr.append({
                'stat': '1',
                'dpname': dpname,
                'addr': addr,
                'bankaddr': bankaddr,
                'depo': depo,
                'bbname': bbname,
                'celladr': celladr,
                'bankname': bankname,
                'offcaddr': offcaddr,
                'bsePcode': bsePcode,
                'dpid': dpid,
                'dpnum': dpnum,
                'nsePcode': nsePcode,
                'bankaccid': bankaccid,
                'email': email,
                'pan': pan,
                'name': name
                })
            #live user data
            live_data = LiveUserProfileDetails.objects.filter(live_user_id=request.session['client_id'])
            if live_data.count() == 0:
                live_data = LiveUserProfileDetails(live_user_id=request.session['client_id'], dp_name=dpname, nse_pcode=nsePcode, addr=addr, bank_addr=bankaddr,
                                                   depo=depo, bb_name=bbname, cell_adr=celladr, bank_name=bankname, offc_addr=offcaddr,
                                                   bse_pcode=bsePcode, dp_id=dpid, dp_num=dpnum, bank_accid=bankaccid, email=email, pan=pan, name=name, user_id=request.session['id'])
            ####--------------------------------------------------------####
            bank_addres = ''
            bank_name = ''
            dob_account = ''
            cell_addr = ''
            account_status = ''
            bank_account_no = ''
            email_addr = ''
            bank_branch_name = ''
            exch_enabled = ''
            account_type = ''
            address = ''
            pan_no = ''
            account_name = ''
            account_id = ''
            ####--------------------------------------------------------####
            if "bankAddres" in user_profile_details:
                bank_addres = user_profile_details['bankAddres']
            if "bankName" in user_profile_details:
                bank_name = user_profile_details['bankName']
            if "dobAccount" in user_profile_details:
                dob_account = user_profile_details['dobAccount']
            if "cellAddr" in user_profile_details:
                cell_addr = user_profile_details['cellAddr']
            if "accountStatus" in user_profile_details:
                account_status = user_profile_details['accountStatus']
            if "bankAccountNo" in user_profile_details:
                bank_account_no = user_profile_details['bankAccountNo']
            if "emailAddr" in user_profile_details:
                email_addr = user_profile_details['emailAddr']
            if "bankBranchName" in user_profile_details:
                bank_branch_name = user_profile_details['bankBranchName']
            if "exchEnabled" in user_profile_details:
                exch_enabled = user_profile_details['exchEnabled']
            if "accountType" in user_profile_details:
                account_type = user_profile_details['accountType']
            if "address" in user_profile_details:
                address = user_profile_details['address']
            if "panNo" in user_profile_details:
                pan_no = user_profile_details['panNo']
            if "accountName" in user_profile_details:
                account_name = user_profile_details['accountName']
            if "accountId" in user_profile_details:
                account_id = user_profile_details['accountId']
            ####--------------------------------------------------------#####
            account_profile_details_arr.append({
                'stat': '1',
                'bankAddres': bank_addres,
                'bankName': bank_name,
                'dobAccount': dob_account,
                'cellAddr': cell_addr,
                'accountStatus': account_status,
                'bankAccountNo': bank_account_no,
                'emailAddr': email_addr,
                'bankBranchName': bank_branch_name,
                'exchEnabled': exch_enabled,
                'accountType': account_type,
                'address': address,
                'panNo': pan_no,
                'accountName': account_name,
                'accountId': account_id
                })

            #request.session["toggle_button_status"] = "live"
            #live user data
            live_data = LiveUserAccountDetails.objects.filter(live_user_id=request.session['client_id'])
            if live_data.count() == 0:
                live_data = LiveUserAccountDetails(live_user_id=request.session['client_id'], bank_address=bank_addres, bank_name=bank_name, dob_account=dob_account,
                                                   cell_addr=cell_addr, account_status=account_status, bank_account_no=bank_account_no, email_addr=email_addr,
                                                   bank_branch_name=bank_branch_name, exch_enabled=exch_enabled, account_type=account_type, address=address,
                                                   pan_no=pan_no, account_name=account_name, account_id=account_id, user_id=request.session['id'])
        else:
            user_profile_details_arr.append({
                'stat': '0'
                })
            account_profile_details_arr.append({
                'stat': '0'
                })
            #request.session["toggle_button_status"] = "virtual"
    else:
        user_profile_details_arr.append({
            'stat': '0'
        })
        account_profile_details_arr.append({
            'stat': '0'
        })
        #request.session["toggle_button_status"] = "virtual"
    #print '-----user_profile_details_arr---------',user_profile_details_arr



    return {'user_profile_details_arr': user_profile_details_arr, 'account_profile_details_arr': account_profile_details_arr}


def user_holding(request):
    holding_arr = []
    holding_val_arr = []
    ##############----------JAYATI---------##############
    api_url = settings.API_ORDER_URL
    api_holding_url = str(api_url) + '/holding/'
    #############-------------------------###############
    account_id = "UTEST3"
    brkname = "VNS"
    s_prdt_ali = "CNC:CNC||CO:CO||MIS:MIS||NRML:NRML"

    total_bse_holding_value = ''
    total_nse_holding_value = ''
    total_ysx_holding_value = ''
    total_cse_holding_value = ''
    total_mcx_holding_value = ''
    bsetsym = ''
    holdqty = ''
    pcode = ''
    exch1 = ''
    exch2 = ''
    ltnse = ''
    ltp = ''
    bse_holding_value = ''
    nse_holding_value = ''

    #############-------------------------###############
    if 'initial_token' in request.session and 'client_id' in request.session and 'access_token' in request.session:
        client_id = request.session['client_id']
        access_token = request.session['access_token']
        if "account_id" in request.session:
            account_id = request.session['account_id']
        r = requests.post(api_holding_url, json={"uid":client_id,"acctid":account_id,"brkname":brkname,"s_prdt_ali":s_prdt_ali}, headers={"Authorization": access_token, "Content-Type": "application/json"})
        holding_details = r.json()
        if holding_details['stat'] == "Ok":

            if "Totalval" in holding_details:
                # ------------------------------------------------------------------------------#
                if "TotalBSEHoldingValue" in holding_details['Totalval']:
                    total_bse_holding_value = holding_details['Totalval']['TotalBSEHoldingValue']
                # ------------------------------------------------------------------------------#
                if "TotalYSXHoldingValue" in holding_details['Totalval']:
                    total_ysx_holding_value = holding_details['Totalval']['TotalYSXHoldingValue']
                # ------------------------------------------------------------------------------#
                if "TotalNSEHoldingValue" in holding_details['Totalval']:
                    total_nse_holding_value = holding_details['Totalval']['TotalNSEHoldingValue']
                # ------------------------------------------------------------------------------#
                if "TotalCSEHoldingValue" in holding_details['Totalval']:
                    total_cse_holding_value = holding_details['Totalval']['TotalCSEHoldingValue']
                # ------------------------------------------------------------------------------#
                if "TotalMCXHoldingValue" in holding_details['Totalval']:
                    total_mcx_holding_value = holding_details['Totalval']['TotalMCXHoldingValue']
                # ------------------------------------------------------------------------------#
                holding_arr.append({
                    'TotalBSEHoldingValue': total_bse_holding_value,
                    'TotalYSXHoldingValue': total_ysx_holding_value,
                    'TotalNSEHoldingValue': total_nse_holding_value,
                    'TotalCSEHoldingValue': total_cse_holding_value,
                    'TotalMCXHoldingValue': total_mcx_holding_value
                    })
                # ------------------------------------------------------------------------------#

            if "HoldingVal" in holding_details:
                for x in holding_details['HoldingVal']:
                    # ------------------------------------------------------------------------------#
                    if "Bsetsym" in x:
                        bsetsym = x['Bsetsym']
                    # ------------------------------------------------------------------------------#
                    if "Holdqty" in x:
                        holdqty = x['Holdqty']
                    # ------------------------------------------------------------------------------#
                    if "Pcode" in x:
                        pcode = x['Pcode']
                    # ------------------------------------------------------------------------------#
                    if "Exch1" in x:
                        exch1 = x['Exch1']
                    # ------------------------------------------------------------------------------#
                    if "Exch2" in x:
                        exch2 = x['Exch2']
                    # ------------------------------------------------------------------------------#
                    if "BSEHOldingValue" in x:
                        bse_holding_value = x['BSEHOldingValue']
                    # ------------------------------------------------------------------------------#
                    if "NSEHOldingValue" in x:
                        nse_holding_value = x['NSEHOldingValue']
                    # ------------------------------------------------------------------------------#
                    if "LTnse" in x:
                        ltnse = x['LTnse']
                    # ------------------------------------------------------------------------------#
                    if "Ltp" in x:
                        ltp = x['Ltp']
                    # ------------------------------------------------------------------------------#
                    holding_val_arr.append({
                        'Bsetsym': bsetsym,
                        'Holdqty': holdqty,
                        'Pcode': pcode,
                        'Exch1': exch1,
                        'Exch2': exch2,
                        'BSEHOldingValue': bse_holding_value,
                        'LTnse': ltnse,
                        'Ltp': ltp,
                        'NSEHOldingValue': nse_holding_value
                        })

        return {'holding_arr': holding_arr, 'holding_val_arr': holding_val_arr}
    else:
        return {'holding_arr': holding_arr, 'holding_val_arr': holding_val_arr}

def idle_time_check(request):
    return { "IDLE_TIME_CHK" : settings.IDLE_TIME_CHK }