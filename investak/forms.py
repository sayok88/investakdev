from django import forms
#from django.forms import ModelForm
from .models import *
from django.core.validators import *
from django.core.mail import send_mail
from django.template import Context,Template
from django.conf import settings
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
class RegistrationForm(forms.Form):
    name = forms.CharField(label="Name",widget=forms.TextInput(attrs={'placeholder': 'Name','class':'form-control'}),max_length=30,min_length=3)

   # email = forms.EmailField(label="Email",widget=forms.EmailInput(attrs={'placeholder': 'Email','class':'form-control'}),max_length=100,error_messages={'invalid': ("invalid Email")})
    phone = forms.CharField(label="Phone Number",max_length=12,min_length=10,
                                widget=forms.TextInput(attrs={'placeholder': 'Phone Number','class':'form-control'}))
    location = forms.CharField(label="Nearest City", widget=forms.TextInput(attrs={'placeholder': 'Nearest City', 'class': 'form-control'}),
                           max_length=30, min_length=3)

    #recaptcha = ReCaptchaField()

    #Override of clean method for password check
    def clean(self):
        phone = self.cleaned_data.get('phone')
        name =self.cleaned_data.get('name')
        if phone is not None:
            if not phone.isnumeric() :
                self._errors['phone'] = [u"Enter phone number in numerals"]
        if name is not None:
            name=name.replace(" ", "")
            if not name.isalpha():
                self._errors['name'] = [u"Numbers not allowed in this field"]

        return self.cleaned_data


    #Override of save method for saving both User and Profil objects
    def save(self, datas=[]):
        # u = User.objects.create_user(datas['username'],
        #                              datas['email'],
        #                              datas['password1'])
        # u.is_active = False
        # u.first_name=datas['first_name']
        # u.last_name = datas['last_name']
        # u.save()
        # profil=Profil()
        # profil.user=u
        # profil.activation_key=datas['activation_key']
        # profil.key_expires=datetime.datetime.strftime(timezone.now() + datetime.timedelta(days=2), "%Y-%m-%d %H:%M:%S")
        # profil.save()
        return None

    #Handling of activation email sending ------>>>!! Warning : Domain name is hardcoded below !!<<<------
    #I am using a text file to write the email (I write my email in the text file with templatetags and then populate it with the method below)
    def sendEmail(self, datas=[]):

        msg_plain = render_to_string('emails.txt', {'phone':self.cleaned_data.get('phone'),
                                                             'name':self.cleaned_data.get('name'),
                                                             'location':self.cleaned_data.get('location')})
        msg_html = render_to_string('email_template_new_user.html', {'phone': self.cleaned_data.get('phone'),
                                                    'name': self.cleaned_data.get('name'),
                                                    'location': self.cleaned_data.get('location')})
        #print unicode(message).encode('utf8')
        send_mail('New Account Request', msg_plain, 'support@investak.in', ['ankitkanoria1@gmail.com','ghoshdeepjoy2013@gmail.com'], fail_silently=False,html_message=msg_html)
      #  email = EmailMessage(datas['email_subject'], 'message', [datas['email']])
      #  email.send()