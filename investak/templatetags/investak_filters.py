from django import template
from investak.models import *
register = template.Library()

@register.filter(name='div')
def div(value, arg):
    try:
        value = float(value)
        arg = float(arg)
        if arg: return float(value / arg)
    except: pass
    return ''

@register.filter(name='replace_special_char')
def replace_special_char(value):
    try:
        return ''.join(e for e in value if e.isalnum())
    except: pass
    return ''

@register.filter(name='to_float')
def to_float(value):
    if  value != None:
        value = str(value)
        value = value.replace(',','')
    else: 
        value = 0.00
    return float(value)

@register.filter(name='only_float')
def only_float(value):
    if  value != None:
        value = str(value)
        value = value.replace('-','')
        value = value.replace('+','')
        value = value.replace(',','')
    else:
        value = 0.00
    return float(value) 

@register.filter
def addi( value, arg ):
    '''
    Divides the value; argument is the divisor.
    Returns empty string on any error.
    '''
    try:
        value = int( value )
        arg = int( arg )
        if arg is not None: return value + arg
    except: pass
    return ''