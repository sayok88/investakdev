from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response,render
from investak.models import *
from django.db.models import Avg, Sum, Count, Max, Min
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.core.context_processors import csrf
import json
import hashlib
from django.http import JsonResponse
from random import randint
def Home(request):
	posted = {}
	vtd=None
	if request.is_secure():
		protocol = 'https://'
	else:
		protocol = 'http://'
	host=protocol+request.META["HTTP_HOST"]
	try:
		if request.method=="GET":
			 try:
				 name=request.GET["pym_name"]
				 phone=request.GET["pym_phone"]
				 email=request.GET["pym_email"]
				 pym_vtd_id = request.GET["pym_vtd_id"]
				 vti = TransactionInitiate.objects.get(id=int(pym_vtd_id))
				 amount=round(vti.profit/100,2)
				 vtd=VirtualTransactionDump(vti=vti,amount=amount)
				 vtd.save()
				 posted=create_post(email,name,phone,amount,host)

			 except Exception as e:
				 print e
	except Exception as e :
		print e
	MERCHANT_KEY = "rjQUPktU"
	key="rjQUPktU"
	SALT = "e5iIg1jwi8"
	PAYU_BASE_URL = "https://test.payu.in/_payment"

	if request.method=="POST" and request.is_ajax():
		name = request.POST["name"]
		phone = request.POST["phone"]
		email = request.POST["email"]
		pym_vtd_id = request.POST["vti_id"]
		transaction_cost=float(request.POST["transaction_cost"])
		no_of_unit=int(request.POST["no_of_unit"])
		minimum_investment_amount=float(request.POST["minimum_investment_amount"])
		vti = TransactionInitiate.objects.get(id=int(pym_vtd_id))
		vti.minimum_investment=minimum_investment_amount
		vti.transaction_cost=transaction_cost
		vti.no_of_unit=no_of_unit
		vti.save()
		amount = round(vti.profit / 100, 2)
		vtd = VirtualTransactionDump(vti=vti, amount=amount)
		vtd.save()
		posted = create_post(email, name, phone, amount,host)

	#PAYU_BASE_URL = "https://secure.payu.in/_payment"
	action = ''

	for i in request.POST:
		posted[i]=request.POST[i]
	xyz=str(vtd.id)
	lenx=len(xyz)
	for x in range(0,20-lenx):
		xyz='0'+xyz
	print len(xyz)
	hash_object = hashlib.sha256(xyz)
	txnid=hash_object.hexdigest()[0:20]

	hashh = ''
	try:
		vtd.txnid=txnid
		vtd.save()
	except:
		pass
	posted['txnid']=txnid
	hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10"
	posted['key']=key
	hash_string=''
	hashVarsSeq=hashSequence.split('|')
	for i in hashVarsSeq:
		try:
			hash_string+=str(posted[i])
		except Exception:
			hash_string+=''
		hash_string+='|'
	hash_string+=SALT
	hashh=hashlib.sha512(hash_string).hexdigest().lower()
	action =PAYU_BASE_URL
	if request.method == "POST" and request.is_ajax():
		return JsonResponse([{"posted":posted,"hashh":hashh,"MERCHANT_KEY":MERCHANT_KEY,"txnid":txnid,"hash_string":hash_string,"action":PAYU_BASE_URL }],safe=False)
	if(posted.get("key")!=None and posted.get("txnid")!=None and posted.get("productinfo")!=None and posted.get("firstname")!=None and posted.get("email")!=None):
		return render_to_response('current_datetime.html',RequestContext(request,{"posted":posted,"hashh":hashh,"MERCHANT_KEY":MERCHANT_KEY,"txnid":txnid,"hash_string":hash_string,"action":PAYU_BASE_URL }))
	else:
		return render_to_response('current_datetime.html',RequestContext(request,{"posted":posted,"hashh":hashh,"MERCHANT_KEY":MERCHANT_KEY,"txnid":txnid,"hash_string":hash_string,"action":"." }))
def create_post(email,name,phone,price,host):

	posted={}
	posted["email"]=email
	posted["phone"]=phone
	names=name.split(' ')
	posted["firstname"]=names[0]
	posted["amount"]=price
	posted["productinfo"]="Research fee"
	posted["surl"] = host+"/Success/"
	posted["furl"] = host+"/Failure/"
	posted["service_provider"] = "payu_paisa"
	try:
		posted["lastname"] = names[1]
	except:
		posted["lastname"] = ''
	posted["curl"] = ''
	posted["address1"] = ''
	posted["address2"] = ''
	posted["city"] = ''
	posted["state"] = ''
	posted["country"] = ''
	posted["zipcode"] = ''
	posted["udf1"] = ''
	posted["udf2"] = ''
	posted["udf3"] = ''
	posted["udf4"] = ''
	posted["udf5"] = ''
	posted["udf6"] = ''
	posted["udf7"] = ''
	posted["udf8"] = ''
	posted["udf9"] = ''
	posted["udf10"] = ''
	posted["pg"] = ''

	return posted
@csrf_protect
@csrf_exempt
def success(request):
	c = {}
	c.update(csrf(request))
	status=request.POST["status"]
	firstname=request.POST["firstname"]
	amount=request.POST["amount"]
	txnid=request.POST["txnid"]
	posted_hash=request.POST["hash"]
	key=request.POST["key"]
	productinfo=request.POST["productinfo"]
	email=request.POST["email"]
	salt="e5iIg1jwi8"
	redirect_url='/sell_cluster/'
	vtd=None
	try:
		vtd=VirtualTransactionDump.objects.filter(txnid=txnid).first()
		vtd.dump=str(json.dumps(dict(zip(request.POST.keys(), request.POST.values()))))
		if status=='success':
			vtd.vti.paid=True
			vtd.vti.save()
			redirect_url+=str(vtd.vti.order_id)
		vtd.save()
		additionalCharges=request.POST["additionalCharges"]
		retHashSeq=additionalCharges+'|'+salt+'|'+status+'|||||||||||'+email+'|'+firstname+'|'+productinfo+'|'+amount+'|'+txnid+'|'+key
	except Exception as e:
		print e
		retHashSeq = salt+'|'+status+'|||||||||||'+email+'|'+firstname+'|'+productinfo+'|'+amount+'|'+txnid+'|'+key
	hashh=hashlib.sha512(retHashSeq).hexdigest().lower()
	if(hashh !=posted_hash):
		print "Invalid Transaction. Please try again"
	else:
		print "Thank You. Your order status is ", status
		print "Your Transaction ID for this transaction is ",txnid
		print "We have received a payment of Rs. ", amount ,". Your order will soon be shipped."
	return render_to_response('sucess.html',RequestContext(request,{"txnid":txnid,"status":status,"live_order_id":vtd.vti.live_order_id	,"transaction_cost":vtd.vti.transaction_cost,"no_of_unit":vtd.vti.no_of_unit,"minimum_investment_amount":vtd.vti.minimum_investment,"amount":amount,'cluster_id':vtd.vti.cluster_id,'order_buy_id':vtd.vti.order_buy_id,'order_hold_id':vtd.vti.order_hold_id,'order_id':vtd.vti.order_id,'redirect':redirect_url}))


@csrf_protect
@csrf_exempt
def failure(request):
	c = {}
	c.update(csrf(request))
	status=request.POST["status"]
	firstname=request.POST["firstname"]
	amount=request.POST["amount"]
	txnid=request.POST["txnid"]
	posted_hash=request.POST["hash"]
	key=request.POST["key"]
	productinfo=request.POST["productinfo"]
	email=request.POST["email"]
	salt="e5iIg1jwi8"
	redirect_url = '/sell_cluster/'
	try:
		vtd = VirtualTransactionDump.objects.filter(txnid=txnid).first()
		vtd.dump = str(json.dumps(dict(zip(request.POST.keys(), request.POST.values()))))
		redirect_url += str(vtd.vti.order_id)
		vtd.save()
		c["redirect"]=redirect_url
		additionalCharges=request.POST["additionalCharges"]
		retHashSeq=additionalCharges+'|'+salt+'|'+status+'|||||||||||'+email+'|'+firstname+'|'+productinfo+'|'+amount+'|'+txnid+'|'+key
	except Exception:
		retHashSeq = salt+'|'+status+'|||||||||||'+email+'|'+firstname+'|'+productinfo+'|'+amount+'|'+txnid+'|'+key
	hashh=hashlib.sha512(retHashSeq).hexdigest().lower()
	if(hashh !=posted_hash):
		print "Invalid Transaction. Please try again"
	else:
		print "Thank You. Your order status is ", status
		print "Your Transaction ID for this transaction is ",txnid
		print "We have received a payment of Rs. ", amount ,". Your order will soon be shipped."
 	return render_to_response("Failure.html",RequestContext(request,c))


