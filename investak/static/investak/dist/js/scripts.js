/**
 * Created by admin on 14-12-2016.
 */

	$(document).ready(function(){
        $(window).unload(saveBasicInfo);
        
        loadBasicInfo();
        check_address();
        

        if($("#full_name").val() != "") {
            tabber();
            $('#tab-1').removeClass('tav_active');
      			$('#tab-1').parent().removeClass('tav_active');
            $('ul.tabs li:eq(0)').removeClass('tav_active');

      			$('#tab-2').parent().addClass('tav_active');
      			$("#tab-2").addClass('tav_active');
            $('ul.tabs li:eq(1)').addClass('tav_active done_active');
        }
        colorchange();
		    divopen();
        var current_date = new Date();
        var max_year = current_date.getFullYear()-18;
        var currentDate = new Date(max_year, 1 - 1, 1);
        $("#c_address").keypress(function(e){
            if($(this).val() != $("#address").val()) {
              $("#remember_me").attr('checked',false);
            }
            else {
              $("#remember_me").attr('checked',true);
            }
        });

        $("#c_city").keypress(function(e){
            if($(this).val() != $("#city").val()) {
              $("#remember_me").attr('checked',false);
            }
            else {
              $("#remember_me").attr('checked',true);
            }
        });

        $("#c_zipcode").keypress(function(e){
            if($(this).val() != $("#zipcode").val()) {
              $("#remember_me").attr('checked',false);
            }
            else {
              $("#remember_me").attr('checked',true);
            }
        });

        $("#c_state").change(function(e){
            if($(this).val() != $("#state").val()) {
              $("#remember_me").attr('checked',false);
            }
            else {
              $("#remember_me").attr('checked',true);
            }
        });

         $("#c_address_proof").change(function(e){
            if($(this).val() != $("#address_proof").val()) {
              $("#remember_me").attr('checked',false);
            }
            else {
              $("#remember_me").attr('checked',true);
            }
        });
        $("#adhar_no").mask("9999-9999-9999");
        $('input:radio[name="radio_client"]').change(function(e){
          if($(this).is(':checked')) {
            var value = $(this).val();
            if(value == 1) {
              $("#div_client_name").show();
              $("#div_client_id").show();
              $("#div_client_mobile").show();
            }
            else {
              $("#div_client_name").hide();
              $("#div_client_id").hide();
              $("#div_client_mobile").hide();
            }
          }
        });

        $('input:radio[name="executive_referral"]').change(function(e){
          if($(this).is(':checked')) {
            var value = $(this).val();
            if(value == 1) {
              $("#div_executive_name").show();
              $("#div_executive_id").show();
              $("#div_executive_mobile").show();
            }
            else {
              $("#div_executive_name").hide();
              $("#div_executive_id").hide();
              $("#div_executive_mobile").hide();
            }
          }
        });

        $("#minor").click(function(e){
            if($(this).is(':checked') == true) {
                $(".minor_section").show('slow');
            }
            else {
                $(".minor_section").hide('slow');
            }
        });
        $("#company_details").click(function(e){
            if($(this).is(':checked') == true) {
                $(".company_section").show('slow');
            }
            else {
                $(".company_section").hide('slow');
            }
        });

        $("#nominee_details").click(function(e){
            if($(this).is(':checked') == true) {
                $(".nominee_section").show('slow');
                $(".minor_aside").show('slow');
            }
            else {
                $(".nominee_section").hide('slow');
                $(".minor_aside").hide('slow');
            }
        });

        $("#referrer_details").click(function(e){
            if($(this).is(':checked') == true) {
              $(".referrer_section").show('slow');
            }
            else {
              alert("False");
              $(".referrer_section").hide('slow');
            }
        });
        $("#remember_me").click(function(e){
            if($(this).is(':checked') == true) {
                $("#c_address").val($("#address").val());
                $("#c_state").val($("#state").val());
                $("#c_city").val($("#city").val());
                $("#c_country").val($("#country").val());
                $("#c_zipcode").val($("#zipcode").val());
                $("#c_address_proof").val($("#address_proof").val());
                $("#c_address-error").remove();
                $("#c_state-error").remove();
                $("#c_city-error").remove();
                $("#c_country-error").remove();
                $("#c_zipcode-error").remove();
            }
            else {
                $("#c_address").val('');
                $("#c_state").val('');
                $("#c_city").val('');
                $("#c_country").val('');
                $("#c_zipcode").val('');
                $("#c_address_proof").val('');
            }
        });
        $("#date_of_birth").datepicker(
        {
            dateFormat: 'dd-mm-yy',
            yearRange: '1930:'+max_year+'',
            minDate: new Date(1930, 1 - 1, 1),
            //minDate: start_date, maxDate: end_date,
            changeMonth: true, changeYear: true,
            //yearRange: 'c-100:c-18',
            defaultDate: currentDate,
            font: '9px',
            onSelect: function () {
                var selected = $(this).val();
                $("#date_of_birth").val(selected);
            }
        }
          ).datepicker("setDate", currentDate);

        $('input[type=text]').keydown(function(e){
            if(e.which === 32 && e.target.selectionStart === 0) {
               return false;
           }
        });

        $('input[type=text]').focus(function(e){
            var text_info = $.trim($(this).val());
            var name_regex = /<(.|\n)*?>/g;
            if(name_regex.test(text_info))
            {
                $(this).val('');
                return false;
            }
        });

        $("#email_id").keydown(function(e){
            if(e.which === 32) {
                return false;
            }
        });

        $(".name-field").keyup(function(e){
            var $th = $(this);
            $th.val($th.val().replace(/(\s{2,})|[^a-zA-Z']/g, ' '));
            $th.val($th.val().replace(/^\s*/, ''));
        });
        $(".mobile-field").keyup(function(e){
            var $th = $(this);
            $th.val($th.val().replace(/(\s{2,})|[^0-9']/g, ''));
            $th.val($th.val().replace(/^\s*/, ''));
        });

        $(".pan-field, .ifsc-field").keyup(function(e){
            var $th = $(this);
            $th.val($th.val().replace(/(\s{2,})|[^a-zA-Z0-9']/g, ''));
            $th.val($th.val().replace(/^\s*/, ''));
        });

        $(".address-field").keyup(function(e){
            var $th = $(this);
            $th.val($th.val().replace(/(\s{2,})|[^a-zA-Z0-9-,\/']/g, ' '));
            $th.val($th.val().replace(/^\s*/, ''));
        });

        $(".pan-field").keypress(function(e){
            var PANNumber = $(this).val();
            var count = PANNumber.length;
            var keyCode = (e.which) ? parseInt(e.which) : parseInt(e.keyCode);
            if (count <= 4) {
                if ((keyCode <= 90 && keyCode >= 65) || (keyCode <= 122 && keyCode >= 97) || e.which == 8) {
                    return true;
                }
                else {
                    return false;
                }
            }
            if (count <= 8 && count >= 5) {
                if ((keyCode <= 57 && keyCode >= 48) || e.which == 8) {
                    return true;
                }
                else {
                    return false;
                }
            }
            if (count == 9) {
                if ((keyCode <= 90 && keyCode >= 65) || (keyCode <= 122 && keyCode >= 97) || e.which == 8) {
                    return true;
                }
                else {
                    return false;
                }
            }

        });

        $(".ifsc-field").keypress(function(e){
            var IFSCCode = $(this).val();
            var count = IFSCCode.length;
            var keyCode = (e.which) ? parseInt(e.which) : parseInt(e.keyCode);
            if(count <= 3) {
                if((keyCode <=90 && keyCode >= 65) || (keyCode <= 122 && keyCode >= 97) || e.which == 8) {
                    return true;
                }
                else {
                    return false;
                }
            }

            if(count == 4) {
                if(keyCode == 48 || e.which == 8) {
                    return true;
                }
                else {
                    return false;
                }
            }

            if(count >= 5 && count <= 10) {
                if ((keyCode <= 57 && keyCode >= 48) || e.which == 8) {
                }
                else {
                    return false;
                }
            }
        });

        $(".ifsc-field").blur(function(e){
            if($(".ifsc-field").val().length == 11) {
                $.ajax({
                type:"GET",
                url:"/frontend/check_ifsc_code/",
                data:{ifsc_code: $.trim($(this).val())},
                beforeSend:function() {
                    $.blockUI({
                            message: '<img src="/static/investak/images/loader1.gif" />'
                    });
                },
                success:function(data) {
                    var obj = $.parseJSON(JSON.stringify(data));
                    if(obj.hasOwnProperty('IFSC') == true) {
                        $("#bank_branch_name").val(obj.BRANCH);
                        $("#bank_name").val(obj.BANK);
                    }
                    else {
                        alert("Invalid IFSC Code");
                        $("#bank_branch_name").val('');
                        $("#bank_name").val('');
                    }
                    $.unblockUI();
                },
                error:function(xhr) {
                    alert("Error Occurred");
                    $.unblockUI();
                }
            });
            }

        });

        $("#email_id").blur(function(e){
            var email_id = $.trim($("#email_id").val());
            if(email_id != "") {
                $.ajax({
                    type:"GET",
                    url:"/frontend/check_account_email/",
                    data:{email_id:email_id},
                    beforeSend:function(){
                        $.blockUI({
                            message: '<img src="/static/investak/images/loader1.gif" />'
                    });
                    },
                    success:function(data) {
                        if(data == 1)
                        {
                            if(!$("#hidden_trading_id").val()) {
                                alert("Email ID Already Exists");
                                ("#email_id").val('');
                            }   
                           
                            $.unblockUI();
                        }
                        else {
                            $.unblockUI();
                        }
                    },
                    error:function(xhr) {
                        alert("Error Occurred");
                        ("#email_id").val('');
                        $.unblockUI();
                    }
                });
            }
        });

        $(".pincode-field, #bank_account_no").keyup(function(e){
            var $th = $(this);
            $th.val($th.val().replace(/(\s{2,})|[^0-9']/g, ''));
            $th.val($th.val().replace(/^\s*/, ''));
        });

        $("#investing_exp").keyup(function(e){
            var val = $(this).val();
            if(isNaN(val)){
                 val = val.replace(/[^0-9\.]/g,'');
                 if(val.split('.').length>2)
                     val =val.replace(/\.+$/,"");
            }
            if(val > 99) {
                $(this).val(99);
            }
            else {
                $(this).val(val);
            }
        });

        $("#net_worth").keyup(function(e){
            var $th = $(this);
            var value = $th.val().replace(/(\s{2,})|[^a-zA-Z0-9.']/g, ' ');
            $th.val(value);
            if(value.split('.').length>2)
                     $th.val(value.replace(/\.+$/,""));
            $th.val($th.val().replace(/^\s*/, ''));
        });

        /****************Basic Info Validation*************/
        $("#btnProfileInfo").click(function(e){
            $("#frmBasicInfo").submit();
        });
        $.validator.addMethod('checked_income_or_worth', function(value, element){
           return $("#gross_annual_income").val() != "" ||  $("#net_worth").val() != ""
        }, 'Please Enter Either Annual Income Or Net Worth');

        $("#frmBasicInfo").validate({
           rules: {
                full_name: {
                    required: true
                },
               father_name: {
                    required: true
                },
               mother_name: {
                    required: true
                },
               date_of_birth: {
                    required: true
                },
               address: {
                   required: true
               },
               state: {
                   required: true
               },
               country: {
                   required: true
               },
               zipcode: {
                   required: true,
                   number: true,
                   maxlength: 6,
                   minlength: 6
               },
               city: {
                   required: true
               },

               occupation: {
                   required: true
               },
               cst_number: {
                   required: true,
                   number: true
               },
               pan_number: {
                   required: true,
                   alphanumeric: true,
                   maxlength: 10,
                   minlength: 10
               },
               adhar_no: {
                   required: true,
                   maxlength: 14,
                   minlength: 14
               },
               mobile_no: {
                   required: true,
                   number: true,
                   maxlength: 10,
                   minlength: 10
               },
               email_id: {
                   required: true,
                   email: true
               },
               bank_name: {
                   required: true
               },
               account_type: {
                   required: true
               },
               ifsc_code: {
                   required: true,
                   alphanumeric: true,
                   maxlength: 11,
                   minlength: 11
               },
               gender: {
                   required: true
               },
               marital_status: {
                   required: true
               },
               gross_annual_income: {
                   checked_income_or_worth: true
               },
               bank_account_no: {
                   required: true,
                   number: true
               },
               /*nominee_name: {
                   required: true
               },
               nominee_relation: {
                   required: true
               },
               nominee_contact: {
                   required: true,
                   number: true,
                   maxlength: 10,
                   minlength: 10
               },
               nominee_pan_no: {
                   required: true,
                   alphanumeric: true,
                   maxlength: 10,
                   minlength: 10
               },
               guardian_name: {
                   required: true
               },
               guardian_address: {
                   required: true
               },
               guardian_contact: {
                   required: true,
                   number: true,
                   maxlength: 10,
                   minlength: 10
               },*/
               income_source: {
                   required: true
               },
               identity_proof: {
                   required: true
               },
               resdent_status: {
                   required: true
               },
               education: {
                   required: true
               },
               nationality: {
                   required: true
               },
               address_proof: {
                   required: true
               },
               bank_branch_name: {
                   required: true
               },
               bank_proof: {
                   required: true
               },
               /*company_name: {
                   required: true
               },
               company_address: {
                   required: true
               },*/
               client_name: {
                   required: true
               },
               client_id: {
                   required: true
               },
               client_mobile_no: {
                   required: true
               },
               executive_name: {
                   required: true
               },
               executive_id: {
                   required: true
               },
               executive_mobile_no: {
                   required: true
               }
           },
           messages: {
              full_name: {
                    required: "Please Enter Full Name"
                },
               father_name: {
                    required: "Please Enter Father's Name"
                },
               mother_name: {
                    required: "Please Enter Mother's Name"
                },
               date_of_birth: {
                    required: "Please Enter Date Of Birth"
                },
               address: {
                   required: "Please Enter Address Details"
               },
               state: {
                   required: "Please Enter State Name"
               },
               country: {
                   required: "Please Select Country"
               },
               zipcode: {
                   required: "Please Enter Zipcode"
               },
               city: {
                   required: "Please Enter City"
               },
               occupation: {
                   required: "Please Enter Occupation"
               },
               cst_number: {
                   required: "Please Enter CST Number"
               },
               pan_number: {
                   required: "Please Enter Pan Card Number",
                   alphanumeric: "Please Enter Proper Pancard No",
                   maxlength: "Pancard Must Be 10 Characters Long",
                   minlength: "Pancard Must Be 10 Characters Long"
               },
               adhar_no: {
                   required: "Please Enter Adhar Card Number"
               },
               mobile_no: {
                   required: "Please Enter Mobile No"
               },
               email_id: {
                   required: "Please Enter Email ID"
               },
               bank_name: {
                   required: "Please Enter Bank Name"
               },
               account_type: {
                   required: "Please Select Account Type"
               },
               ifsc_code: {
                   required: "Enter Bank IFSC Code",
                   maxlength: "IFSC Code Must Be 11 Characters Long",
                   minlength: "IFSC Code Must Be 11 Characters Long"
               },
               gender: {
                   required: "Please Select Gender"
               },
               marital_status: {
                   required: "Please Select Marital Status"
               },
               bank_account_no: {
                   required: "Please Enter Bank Account No",
                   number: "Account No Must Be Numeric"
               },
               nominee_name: {
                   required: "Please Enter Nominee Name"
               },
               nominee_relation: {
                   required: "Please Enter Nominee Relation"
               },
               nominee_contact: {
                   required: "Please Enter Nominee Contact No",
                   number: "Nominee Contact No Must Be Numeric",
                   maxlength: "Nominee Contact No Must have 10 Digit",
                   minlength: "Nominee Contact No Must have 10 Digit"
               },
               nominee_pan_no: {
                   required: "Please Enter Nominee Pancard No",
                   alphanumeric: "Please Enter Valid Pancard No",
                   maxlength: "Pancard Must Be 10 Characters Long",
                   minlength: "Pancard Must Be 10 Characters Long"
               },
               guardian_name: {
                   required: "Please Enter Guardian Name"
               },
               guardian_address: {
                   required: "Please Enter Guardian Address"
               },
               guardian_contact: {
                   required: "Please Enter Guardian Contact No",
                   number: "Contact No Must Be Numeric",
                   maxlength: "Contact No Must Be 10 Digit",
                   minlength: "Contact No Must Be 10 Digit"
               },
               income_source: {
                   required: "Please Enter Income Source"
               },
               identity_proof: {
                   required: "Please Select Identity Proof"
               },
               resdent_status: {
                   required: "Please Select Residential Status"
               },
               education: {
                   required: "Please Select Education"
               },
               nationality: {
                   required: "Please Select Nationality"
               },
               address_proof: {
                   required: "Please Select Permanent Address Proof"
               },
               bank_branch_name: {
                   required: "Please Enter Bank Branch Name"
               },
               bank_proof: {
                   required: "Please Select bank Proof"
               },
               company_name: {
                   required: "Please Enter Company Name"
               },
               company_address: {
                   required: "Please Enter Company Address"
               },
               client_name: {
                   required: "Please Enter Client Name"
               },
               client_id: {
                   required: "Please Enter Client ID"
               },
               client_mobile_no: {
                   required: "Please Enter Client Mobile No"
               },
               executive_name: {
                   required: "Please Enter Executive Name"
               },
               executive_id: {
                   required: "Please Enter Executive ID"
               },
               executive_mobile_no: {
                   required: "Please Enter Executive Mobile No"
               }
           },
           submitHandler:function(form) {
                var data = new FormData($("#frmBasicInfo").get(0));
               $.ajax({
                   type: "POST",
                   url: "/frontend/add_user_account_details/",
                   data: data,
                   cache: false,
                   processData: false,
                   contentType: false,
                   beforeSend: function () {
                        $.blockUI({
                            message: '<img src="/static/investak/images/loader1.gif" />'
                        });
                   },
                   success: function (data) {
                        if(data == -1) {
                            alert("Un-Authorized User Access");
                            $.unblockUI();
                        }
                       else {
                            $("#hidden_trading_id").val(data);
                           $(".return_account_id").val(data);
                           $.unblockUI();
                           $('#tab-2').removeClass('tav_active');
                            $('#tab-2').parent().removeClass('tav_active');
                            $('ul.tabs li:eq(1)').removeClass('tav_active');

                            $('#tab-3').parent().addClass('tav_active');
                            $("#tab-3").addClass('tav_active');
                            $('ul.tabs li:eq(2)').addClass('tav_active done_active');
                        }

                   },
                   error:function(xhr) {
                       alert("Error Occurred");
                       $.unblockUI();
                   }
               });
           }
        });
        /****************End*******************************/

        /*----------Upload Proof Section-----*/
        $("#fldPanCard").change(function(e){
            var filetype = e.target.files[0].type;
            var filesize = e.target.files[0].size;
            $("#fldPanCard-error").remove();
            if(filetype != "application/pdf") {
                $(".PanCard").append('<label id="fldPanCard-error" class="error" for="fldPanCard">File Type Must Be PDF</label>');
            }
            else if(parseInt(filesize) > 2097152) {
                $(".PanCard").append('<label id="fldPanCard-error" class="error" for="fldPanCard">File Size Must Be Less Than Or Equal To 2 MB</label>');
            }
            else {
                var data = new FormData($("#frmUploadPanCard").get(0));
                $.ajax({
                    type: "POST",
                    url: "/frontend/upload_pancard/",
                    data: data,
                    contentType: false,
                    cache: false,
                    processData:false,
                    async: false,
                    success:function(data) {
                        if(data == 404) {
                            alert("Pancard not uploaded successfully");
                        }
                        else {
                            $(".aside-pancard").css('display','block');
                        }
                    }
                });
            }
        });

        $("#frmUploadAdharCard").change(function(e){
            var filetype = e.target.files[0].type;
            var filesize = e.target.files[0].size;
            $("#fldAdharCard-error").remove();
            if(filetype != "application/pdf") {
                $(".AdharCard").append('<label id="fldAdharCard-error" class="error" for="fldAdharCard">File Type Must Be PDF</label>');
            }
            else if(parseInt(filesize) > 2097152) {
                $(".AdharCard").append('<label id="fldAdharCard-error" class="error" for="fldAdharCard">File Size Must Be Less Than Or Equal To 2 MB</label>');
            }
            else {
                var data = new FormData($("#frmUploadAdharCard").get(0));
                $.ajax({
                    type: "POST",
                    url: "/frontend/upload_adhar_card/",
                    data: data,
                    contentType: false,
                    cache: false,
                    processData:false,
                    async: false,
                    success:function(data) {
                        if(data == 404) {
                            alert("Adhar Card not uploaded successfully");
                        }
                        else {
                            $(".aside-adharcard").css('display','block');
                        }
                    }
                });
            }
        });

        $("#fldCancelledCheque").change(function(e){
            var filetype = e.target.files[0].type;
            var filesize = e.target.files[0].size;
            $("#fldCancelledCheque-error").remove();
            if(filetype != "application/pdf") {
                $(".CancelledCheque").append('<label id="fldCancelledCheque-error" class="error" for="fldCancelledCheque">File Type Must Be PDF</label>');
            }
            else if(parseInt(filesize) > 2097152) {
                $(".CancelledCheque").append('<label id="fldCancelledCheque-error" class="error" for="fldCancelledCheque">File Size Must Be Less Than Or Equal To 2 MB</label>');
            }
            else {
                var data = new FormData($("#frmUploadCancelledCheque").get(0));
                $.ajax({
                    type: "POST",
                    url: "/frontend/upload_cancelled_cheque/",
                    data: data,
                    contentType: false,
                    cache: false,
                    processData:false,
                    async: false,
                    success:function(data) {
                        if(data == 404) {
                            alert("Cancelled Cheque not uploaded successfully");
                        }
                        else {
                            $(".aside-cancelcheque").css('display','block');
                        }
                    }
                });
            }
        });

        $("#fldBankStmt").change(function(e){
            var filetype = e.target.files[0].type;
            var filesize = e.target.files[0].size;
            $("#fldBankStmt-error").remove();
            if(filetype != "application/pdf") {
                $(".BankStmt").append('<label id="fldBankStmt-error" class="error" for="fldBankStmt">File Type Must Be PDF</label>');
            }
            else if(parseInt(filesize) > 2097152) {
                $(".BankStmt").append('<label id="fldBankStmt-error" class="error" for="fldBankStmt">File Size Must Be Less Than Or Equal To 2 MB</label>');
            }
            else {
                var data = new FormData($("#frmUploadBankStmt").get(0));
                $.ajax({
                    type: "POST",
                    url: "/frontend/upload_bank_stmt/",
                    data: data,
                    contentType: false,
                    cache: false,
                    processData:false,
                    async: false,
                    success:function(data) {
                        if(data == 404) {
                            alert("Bank Statement not uploaded successfully");
                        }
                        else {
                            $(".aside-bankstmt").css('display','block');
                        }
                    }
                });
            }
        });

        $("#fldPermAddPrf").change(function(e){
            var filetype = e.target.files[0].type;
            var filesize = e.target.files[0].size;
            $("#fldPermAddPrf-error").remove();
            if(filetype != "application/pdf") {
                $(".PermAddr").append('<label id="fldPermAddPrf-error" class="error" for="fldPermAddPrf">File Type Must Be PDF</label>');
            }
            else if(parseInt(filesize) > 2097152) {
                $(".PermAddr").append('<label id="fldPermAddPrf-error" class="error" for="fldPermAddPrf">File Size Must Be Less Than Or Equal To 2 MB</label>');
            }
            else {
                var data = new FormData($("#frmUploadPermAddPrf").get(0));
                $.ajax({
                    type: "POST",
                    url: "/frontend/upload_perm_addr/",
                    data: data,
                    contentType: false,
                    cache: false,
                    processData:false,
                    async: false,
                    success:function(data) {
                        if(data == 404) {
                            alert("Permanent Address Proof not uploaded successfully");
                        }
                        else {
                            $(".aside-permaddr").css('display','block');
                        }
                    }
                });
            }
        });

        $("#fldTempAddPrf").change(function(e){
            var filetype = e.target.files[0].type;
            var filesize = e.target.files[0].size;
            $("#fldTempAddPrf-error").remove();
            if(filetype != "application/pdf") {
                $(".TempAddr").append('<label id="fldTempAddPrf-error" class="error" for="fldTempAddPrf">File Type Must Be PDF</label>');
            }
            else if(parseInt(filesize) > 2097152) {
                $(".TempAddr").append('<label id="fldTempAddPrf-error" class="error" for="fldTempAddPrf">File Size Must Be Less Than Or Equal To 2 MB</label>');
            }
            else {
                var data = new FormData($("#frmUploadTempAddPrf").get(0));
                $.ajax({
                    type: "POST",
                    url: "/frontend/upload_corres_addr/",
                    data: data,
                    contentType: false,
                    cache: false,
                    processData:false,
                    async: false,
                    success:function(data) {
                        if(data == 404) {
                            alert("Correspondence Address Proof not uploaded successfully");
                        }
                        else {
                            $(".aside-tempaddr").css('display','block');
                        }
                    }
                });
            }
        });

        $("#fldSignature").change(function(e){
            var filetype = e.target.files[0].type;
            var file_size_array = ['image/gif','image/jpeg','image/png','image/bmp'];
            var filesize = e.target.files[0].size;
            $("#fldSignature-error").remove();
            if($.inArray(filetype,file_size_array) < 0) {
                $(".Signature").append('<label id="fldSignature-error" class="error" for="fldSignature">File Type Must Be JPG</label>');
            }
            else if(parseInt(filesize) > 2097152) {
                $(".Signature").append('<label id="fldSignature-error" class="error" for="fldSignature">File Size Must Be Less Than Or Equal To 2 MB</label>');
            }
            else {
                var data = new FormData($("#frmUploadSignature").get(0));
                $.ajax({
                    type: "POST",
                    url: "/frontend/upload_signature/",
                    data: data,
                    contentType: false,
                    cache: false,
                    processData:false,
                    async: false,
                    success:function(data) {
                        if(data == 404) {
                            alert("Signature not uploaded successfully");
                        }
                        else {
                            $(".aside-signature").css('display','block');
                        }
                    }
                });
            }
        });

        $("#btnRecord").click(function(e){
            if(!$("#hidden_trading_id").val() && $("#fldPanCard").val() == "" && $("#fldAdharCard").val() == "" && ($("#fldCancelledCheque").val() == "" || $("#frmUploadBankStmt").val() == "") && $("#frmUploadPermAddPrf").val() == "") {
              alert("Please Enter All of Your Details Before submit");
            }
            else if($("#ipvRecord").attr('src') == "") {
              alert("Please Record IPV");
            }
            else {
              $("#hidden_trading_id").val(0);
              $(".return_account_id").val(0);
              clearBasicInfo();
              window.location.href='/account_complete/';
            }
            
        });

        $("#btnInstruction").click(function(e){
            $('#tab-1').removeClass('tav_active');
			      $('#tab-1').parent().removeClass('tav_active');
            $('ul.tabs li:eq(0)').removeClass('tav_active');

      			$('#tab-2').parent().addClass('tav_active');
      			$("#tab-2").addClass('tav_active');
            $('ul.tabs li:eq(1)').addClass('tav_active done_active');
        });

        $("#btnUploadDoc").click(function(e){
            if($("#return_account_id").val() == "") {
                alert("Please enter all your basic information");
            }
            else {
                    if(fldPanCard == "" && fldAdharCard == "" && fldCancelledCheque == "" && fldBankStmt == "" && fldPermAddPrf == "" && fldTempAddPrf == "" && fldSignature == "")
                    {
                        alert("Please Upload Necessary Document");
                    }
                    else {
                        $.ajax({
                        type:"GET",
                        url:"/frontend/check_upload_doc_status/",
                        data:{return_account_id:$("#return_account_id").val()},
                        beforeSend: function () {
                            $.blockUI({
                                message: '<img src="/static/investak/images/loader1.gif" />'
                            });
                         }
                    }).done(function(response){
                        if(response == '') {
                            $('#tab-3').removeClass('tav_active');
                            $('#tab-3').parent().removeClass('tav_active');
                            $('ul.tabs li:eq(2)').removeClass('tav_active');

                            $('#tab-4').parent().addClass('tav_active');
                            $("#tab-4").addClass('tav_active');
                            $('ul.tabs li:eq(3)').addClass('tav_active done_active');
                        }
                        else {
                            alert(response);
                        }
                        $.unblockUI();
                    }).fail(function(){
                        alert("Error Occurred");
                    });
                }
            }




        });


        /*----------End Upload Proof Section-----*/
	});
    function saveBasicInfo() {
        localStorage.setItem('father_name', $("#father_name").val());
        localStorage.setItem('mother_name', $("#mother_name").val());
        localStorage.setItem('date_of_birth', $("#date_of_birth").val());
        localStorage.setItem('gender', $("#gender").val());
        localStorage.setItem('marital_status', $("#marital_status").val());
        localStorage.setItem('occupation', $("#occupation").val());
        localStorage.setItem('pan_number', $("#pan_number").val());
        localStorage.setItem('adhar_no', $("#adhar_no").val());
        localStorage.setItem('mobile_no', $("#mobile_no").val());
        localStorage.setItem('email_id', $("#email_id").val());
        localStorage.setItem('gross_annual_income', $("#gross_annual_income").val());
        localStorage.setItem('net_worth', $("#net_worth").val());
        localStorage.setItem('income_source', $("#income_source").val());
        localStorage.setItem('investing_exp', $("#investing_exp").val());
        localStorage.setItem('identity_proof', $("#identity_proof").val());
        localStorage.setItem('resdent_status', $("#resdent_status").val());
        localStorage.setItem('education', $("#education").val());
        localStorage.setItem('nationality', $("#nationality").val());
        localStorage.setItem('address', $("#address").val());
        localStorage.setItem('state', $("#state").val());
        localStorage.setItem('city', $("#city").val());
        localStorage.setItem('country', $("#country").val());
        localStorage.setItem('zipcode', $("#zipcode").val());
        localStorage.setItem('address_proof', $("#address_proof").val());
        localStorage.setItem('c_address', $("#c_address").val());
        localStorage.setItem('c_state', $("#c_state").val());
        localStorage.setItem('c_city', $("#c_city").val());
        localStorage.setItem('c_country', $("#c_country").val());
        localStorage.setItem('c_zipcode', $("#c_zipcode").val());
        localStorage.setItem('c_address_proof', $("#c_address_proof").val());
        localStorage.setItem('ifsc_code', $("#ifsc_code").val());
        localStorage.setItem('account_type', $("#account_type").val());
        localStorage.setItem('bank_name', $("#bank_name").val());
        localStorage.setItem('bank_account_no', $("#bank_account_no").val());
        localStorage.setItem('bank_branch_name', $("#bank_branch_name").val());
        localStorage.setItem('bank_proof', $("#bank_proof").val());
        localStorage.setItem('company_name', $("#company_name").val());
        localStorage.setItem('company_address', $("#company_address").val());
        localStorage.setItem('nominee_name', $("#nominee_name").val());
        localStorage.setItem('nominee_relation', $("#nominee_relation").val());
        localStorage.setItem('nominee_contact', $("#nominee_contact").val());
        localStorage.setItem('nominee_pan_no', $("#nominee_pan_no").val());
        localStorage.setItem('guardian_name', $("#guardian_name").val());
        localStorage.setItem('guardian_address', $("#guardian_address").val());
        localStorage.setItem('guardian_contact', $("#guardian_contact").val());
        localStorage.setItem('client_name', $("#client_name").val());
        localStorage.setItem('client_id', $("#client_id").val());
        localStorage.setItem('client_mobile_no', $("#client_mobile_no").val());
        localStorage.setItem('executive_name', $("#executive_name").val());
        localStorage.setItem('executive_id', $("#executive_id").val());
        localStorage.setItem('executive_mobile_no', $("#executive_mobile_no").val());
    }
    function loadBasicInfo() {
        //$("#full_name").val(localStorage.getItem('full_name'));
        $("#father_name").val(localStorage.getItem('father_name'));
        $("#mother_name").val(localStorage.getItem('mother_name'));
        $("#date_of_birth").val(localStorage.getItem('date_of_birth'));
        $("#gender").val(localStorage.getItem('gender'));
        $("#marital_status").val(localStorage.getItem('marital_status'));
        $("#occupation").val(localStorage.getItem('occupation'));
        $("#pan_number").val(localStorage.getItem('pan_number'));
        $("#adhar_no").val(localStorage.getItem('adhar_no'));
        $("#mobile_no").val(localStorage.getItem('mobile_no'));
        $("#email_id").val(localStorage.getItem('email_id'));
        $("#gross_annual_income").val(localStorage.getItem('gross_annual_income'));
        $("#net_worth").val(localStorage.getItem('net_worth'));
        $("#income_source").val(localStorage.getItem('income_source'));
        $("#investing_exp").val(localStorage.getItem('investing_exp'));
        $("#identity_proof").val(localStorage.getItem('identity_proof'));
        $("#resdent_status").val(localStorage.getItem('resdent_status'));
        $("#education").val(localStorage.getItem('education'));
        $("#nationality").val(localStorage.getItem('nationality'));
        $("#address").val(localStorage.getItem('address'));
        $("#state").val(localStorage.getItem('state'));
        $("#city").val(localStorage.getItem('city'));
        $("#country").val(localStorage.getItem('country'));
        $("#zipcode").val(localStorage.getItem('zipcode'));
        $("#address_proof").val(localStorage.getItem('address_proof'));
        $("#c_address").val(localStorage.getItem('c_address'));
        $("#c_state").val(localStorage.getItem('c_state'));
        $("#c_city").val(localStorage.getItem('c_city'));
        $("#c_country").val(localStorage.getItem('c_country'));
        $("#c_zipcode").val(localStorage.getItem('c_zipcode'));
        $("#c_address_proof").val(localStorage.getItem('c_address_proof'));
        $("#ifsc_code").val(localStorage.getItem('ifsc_code'));
        $("#account_type").val(localStorage.getItem('account_type'));
        $("#bank_name").val(localStorage.getItem('bank_name'));
        $("#bank_account_no").val(localStorage.getItem('bank_account_no'));
        $("#bank_branch_name").val(localStorage.getItem('bank_branch_name'));
        $("#bank_proof").val(localStorage.getItem('bank_proof'));
        $("#company_name").val(localStorage.getItem('company_name'));
        $("#company_address").val(localStorage.getItem('company_address'));
        $("#nominee_name").val(localStorage.getItem('nominee_name'));
        $("#nominee_relation").val(localStorage.getItem('nominee_relation'));
        $("#nominee_contact").val(localStorage.getItem('nominee_contact'));
        $("#nominee_pan_no").val(localStorage.getItem('nominee_pan_no'));
        $("#guardian_name").val(localStorage.getItem('guardian_name'));
        $("#guardian_address").val(localStorage.getItem('guardian_address'));
        $("#guardian_contact").val(localStorage.getItem('guardian_contact'));

        $("#client_name").val(localStorage.getItem('client_name'));
        $("#client_id").val(localStorage.getItem('client_id'));
        $("#client_mobile_no").val(localStorage.getItem('client_mobile_no'));

        $("#executive_name").val(localStorage.getItem('executive_name'));
        $("#executive_id").val(localStorage.getItem('executive_id'));
        $("#executive_mobile_no").val(localStorage.getItem('executive_mobile_no'));
    }
    function clearBasicInfo() {
        localStorage.clear();
    }
	function tabber()
	{
		$('ul.tabs li a').click(function(){
			var tab_id = $(this).attr('data-tab');

			$('ul.tabs li').removeClass('tav_active');
			$('.tab-content').removeClass('tav_active');

			$(this).parent().addClass('tav_active');
			$("#"+tab_id).addClass('tav_active');
		});
	}


    function colorchange()
    {
        $(".corsAdd").find(".ball").click(function(){

            var atall = $(this).attr('data-id');

            $(this).toggleClass('onclick');
			$(this).parent().toggleClass('onclick_bg');

			if($(this).hasClass('onclick')){
				$(".corspon_area").fadeIn();
			}else{
				$(".corspon_area").fadeOut();
			}
        });
    }

	function divopen()
	{
		$(".newdoc").click(function(){
			$(".perAdd_area").fadeIn();
		});

		$(".corsBt").click(function(){
			$(".cors_area").fadeIn();
		});
	}

  function check_address() {
    if($("#address").val() == $("#c_address").val() && $("#city").val() == $("#c_city").val() && $("#c_zipcode").val() == $("#zipcode").val() && $("#c_state").val() == $("#state").val() && $("#c_address_proof").val() == $("#address_proof").val()) {
      $("#remember_me").attr('checked',true);
    }
    else {
      $("#remember_me").attr('checked',false);
    }

    /*if($("#hidden_trading_id").val()) {
      $("#email_id").attr('readonly', true);
    }
    else {
      $("#email_id").attr('readonly', false);
    }*/
  }

