$(document).ready(function(){
	tabber();
	$(".mysubmit_btn2").hide();
	$("input:not([name=onoffswitch])").prop('disabled', true);
	$("select").prop('disabled', true);
	$("#referrer_details").prop('checked', true);
	$(".referrer_section").show();
	$('input:radio[name="radio_client"]').change();
	$('input:radio[name="executive_referral"]').change();
});
function tabber()
{
	$('ul.tabs li a').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs li').removeClass('tav_active');
		$('.tab-content').removeClass('tav_active');

		$(this).parent().addClass('tav_active');
		$("#"+tab_id).addClass('tav_active');
	});
}