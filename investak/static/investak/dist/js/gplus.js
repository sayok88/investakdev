function logout() {
    gapi.auth.signOut(), window.location.href = "/logout/"
}

function login() {
    var a = {
        clientid: GPLUS_APP_ID, /*"1021666202809-k9kfq9b1v90pc444l0h5jlsmqfisks57.apps.googleusercontent.com"*/
        cookiepolicy: "single_host_origin",
        callback: "loginCallback",
        approvalprompt: "force",
        scope: "https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read"
    };
    gapi.auth.signIn(a)
}

function loginCallback(a) {
    if (a.status.signed_in) {
        var e = gapi.client.plus.people.get({
            userId: "me"
        });
        e.execute(function(a) {
            console.log(a);
            if (a.result.emails[0].value) {
                var e = a.result.emails[0].value,
                    o = a.result.displayName,
                    t = "S",
                    mm = a.result.image.url.split("?"),
                    m = a.result.image.isDefault == true?'':mm[0]+"?sz=150";
                e && o && $.ajax({
                    type: "GET",
                    url: "/frontEnd/social_login/",
                    data: {
                        email_id: e,
                        full_name: o,
                        image_url: m,
                        reg_type: t
                    },
                    async: !1,
                    success: function(a) {
                        1 == a ? window.location.href = "/login_2fa/" : 2 == a ? alert("Your account is deactivated by admin") : alert("Error Occurred During G+ Sign In")
                    },
                    error: function(a) {
                        alert("Error Occurred During G+ Sign In")
                    }
                })
            }
        })
    }
}

function onLoadCallback() {
    gapi.client.setApiKey(null), gapi.client.load("plus", "v1", function() {})
}! function() {
    var a = document.createElement("script");
    a.type = "text/javascript", a.async = !0, a.src = "https://apis.google.com/js/client.js?onload=onLoadCallback";
    var e = document.getElementsByTagName("script")[0];
    e.parentNode.insertBefore(a, e)
}();