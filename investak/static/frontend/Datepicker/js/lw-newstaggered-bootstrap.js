function addValidationRules() {
    $.validator.addMethod("eFormEmploymentTypeValidator", function(J, I) {
        if ($("input[name='form.details.applicant.employment.type']").val() == BBConstants.salaried) {
            return $("#eForm input[name='form.applicantPlaceHolder.companyName']").val() != ""
        }
        return true
    }, constructErrorMessage("Hi! Please provide an input."));
    $.validator.addMethod("eFormOtherCountryValidator", function(J, I) {
        if ($(".residency-status-slide select[name='form.applicantPlaceHolder.residenceCountry.value']").val() == BBConstants.otherValue) {
            return $(".residency-status-slide input[name='form.applicantPlaceHolder.residenceCountry.fallback']").val() != ""
        }
        return true
    }, constructErrorMessage("Uh Oh! Please enter a country to proceed"));
    $.validator.addMethod("eFormOtherCoApplicantCountryValidator", function(J, I) {
        if ($(".residency-status-slide select[name='form.coApplicantPlaceHolder.residenceCountry.value']").val() == BBConstants.otherValue) {
            return $(".residency-status-slide input[name='form.coApplicantPlaceHolder.residenceCountry.fallback']").val() != ""
        }
        return true
    }, constructErrorMessage("Uh Oh! Please enter a country to proceed"));
    $.validator.addMethod("eFormEmptyAgeValidator", function(J, I) {
        return !(!$("#mm-yy").is(":visible") && $("#date-of-birth").val() == "")
    }, constructErrorMessage("Hi! Please select your age on the slider"));
    $.validator.addMethod("eFormAgeValidator", function(J, I) {
        return !($("#mm-yy").is(":visible") && $("#date-of-birth").val() == "")
    }, constructErrorMessage("And your birthdate please.."));
    $.validator.addMethod("eFormMobileEmptyAgeValidator", function(J, I) {
        return !($("#date-of-birth").val() == "")
    }, constructErrorMessage("Hi! please provide your date of birth"));
    $.validator.addMethod("eFormDateValidator", function(N, K) {
        var I = N.split(" ")[0];
        var O = $.inArray(N.split(" ")[1], BBConstants.monthNames);
        var L = N.split(" ")[2];
        try {
            var J = new Date(L, O, I)
        } catch (M) {
            return false
        }
        return J.getDate() == I && J.getMonth() == O && J.getFullYear() == L && L >= 1900 && L != getCurrentYear()
    }, constructErrorMessage("Hi! Please enter date in DD/MM/YYYY format"));
    $.validator.addMethod("eFormNameValidator", function(J, I) {
        return J.length <= 50 && !(/[^a-zA-Z- .]/.test(J))
    }, constructErrorMessage("Please enter a valid name"));
    $.validator.addMethod("eFormMinMonthlyIncomeValidator", function(J, I) {
        var K = J.replace(new RegExp(",", "g"), "");
        if (K <= 3000) {
            return false
        }
        return true
    }, constructErrorMessage("Hi! We need your net monthly salary"));
    $.validator.addMethod("eFormMinAnnualIncomeValidator", function(J, I) {
        var K = J.replace(new RegExp(",", "g"), "");
        if (K <= 150000) {
            return false
        }
        return true
    }, constructErrorMessage("Hi! We need your gross annual salary"));
    $.validator.addMethod("eFormMaximumCreditValidator", function(K, J) {
        var I = K.replace(new RegExp(",", "g"), "");
        if (I < 10000 || I > 2000000) {
            return false
        }
        return true
    }, constructErrorMessage("Enter valid credit limit"));
    var o = {
        required: true,
        messages: {
            required: constructErrorMessage("Hi! please provide your net income to proceed.")
        }
    };
    var w = {
        required: true,
        messages: {
            required: constructErrorMessage("Hi! please provide the maximum credit of your existing cards.")
        }
    };
    var u = {
        required: true,
        messages: {
            required: constructErrorMessage("Hi! Please enter a value to proceed")
        }
    };
    var A = {
        required: true,
        messages: {
            required: constructErrorMessage("Uh-oh! Please enter a valid email ID")
        }
    };
    var a = {
        required: true,
        messages: {
            required: constructErrorMessage("Uh-oh! Please enter a bank name")
        }
    };
    var r = {
        required: true,
        messages: {
            required: constructErrorMessage("Uh-oh! Please enter a value to proceed")
        }
    };
    var F = {
        required: true,
        messages: {
            required: constructErrorMessage("Uh-oh! Please enter your gross fixed monthly income")
        }
    };
    var B = {
        required: true,
        messages: {
            required: constructErrorMessage("Uh-oh! Please enter co-applicant's gross fixed monthly income")
        }
    };
    var G = {
        required: true,
        messages: {
            required: constructErrorMessage("Aww! C'mon! It's easy! Pick one!")
        }
    };
    var H = {
        required: true,
        messages: {
            required: constructErrorMessage("Uh-oh! Go ahead and pick any one option to proceed")
        }
    };
    var g = {
        maxlength: 12,
        messages: {
            maxlength: constructErrorMessage("Uh-oh! Please enter a valid 9-digit number")
        }
    };
    var q = {
        required: true,
        messages: {
            required: constructErrorMessage("Hi! Please select an option to proceed.")
        }
    };
    var C = {
        required: true,
        messages: {
            required: constructErrorMessage("Uh-oh! Please tell us about your work experience")
        }
    };
    var z = {
        required: true,
        messages: {
            required: constructErrorMessage("Uh-oh! Please tell us about co-applicant's work experience")
        }
    };
    var l = {
        required: true,
        messages: {
            required: constructErrorMessage("Uh-oh! Please enter a valid amount to proceed")
        }
    };
    var h = {
        required: true,
        messages: {
            required: constructErrorMessage("Uh-oh! Please pick a year")
        }
    };
    var j = {
        required: true,
        messages: {
            required: constructErrorMessage("Uh-oh! Please select your joining year")
        }
    };
    var m = {
        required: true,
        messages: {
            required: constructErrorMessage("Uh-oh! Please select co-applicant's joining year")
        }
    };
    var n = {
        required: true,
        messages: {
            required: constructErrorMessage("Uh-oh! Please select your joining month")
        }
    };
    var v = {
        required: true,
        messages: {
            required: constructErrorMessage("Uh-oh! Please select your joining year")
        }
    };
    var i = {
        required: true,
        messages: {
            required: constructErrorMessage("Uh-oh! Please pick any one option to proceed")
        }
    };
    var y = {
        required: true,
        messages: {
            required: constructErrorMessage("Uh-oh! Please pick an employer")
        }
    };
    var E = {
        required: true,
        messages: {
            required: constructErrorMessage("Uh-oh! Please pick a month")
        }
    };
    var f = {
        required: true,
        messages: {
            required: constructErrorMessage("Uh-oh! Please pick a year")
        }
    };
    var p = {
        required: true,
        messages: {
            required: constructErrorMessage("Uh-oh! Please pick a date")
        }
    };
    var e = {
        required: true,
        messages: {
            required: constructErrorMessage("Uh-oh! Please select a city! We've got tons!")
        }
    };
    var t = {
        required: true,
        messages: {
            required: constructErrorMessage("Uh-oh! Please select an option to proceed")
        }
    };
    var x = {
        required: true,
        messages: {
            required: constructErrorMessage("Uh-oh! Select closest year for an accurate quote.")
        }
    };
    var d = {
        digits: true,
        minlength: 10,
        maxlength: 10,
        range: [7000000000, 9999999999],
        messages: {
            digits: constructErrorMessage("Are you sure that is your number?"),
            minlength: constructErrorMessage("Are you sure that is your number?"),
            maxlength: constructErrorMessage("Are you sure that is your number?"),
            range: constructErrorMessage("Are you sure that is your number?")
        }
    };
    var k = {
        email: true,
        messages: {
            email: constructErrorMessage("Please enter a valid email address")
        }
    };
    var b = {
        required: true,
        messages: {
            required: constructErrorMessage("Uh Oh! Please enter a country to proceed")
        }
    };
    $("#newstaggered-carousel .other-city-required-when-selected").each(function() {
        $(this).rules("add", {
            eFormOtherCityValidator: true
        })
    });
    $("#newstaggered-carousel .hl-other-city-required-when-selected").each(function() {
        $(this).rules("add", {
            eFormOtherCityForHlValidator: true
        })
    });
    $("#newstaggered-carousel .hl-coapp-other-city-required-when-selected").each(function() {
        $(this).rules("add", {
            eFormCoappOtherCityForHlValidator: true
        })
    });
    $("#newstaggered-carousel .basic-country-options").each(function() {
        $(this).rules("add", b)
    });
    $("#newstaggered-carousel .other-country-required-when-selected").each(function() {
        $(this).rules("add", {
            eFormOtherCountryValidator: true
        })
    });
    $("#newstaggered-carousel .other-coapp-country-required-when-selected").each(function() {
        $(this).rules("add", {
            eFormOtherCoApplicantCountryValidator: true
        })
    });
    $("#newstaggered-carousel .company-required-for-salaried").each(function() {
        $(this).rules("add", {
            eFormEmploymentTypeValidator: true
        })
    });
    $("input[name='form.coApplicantPlaceHolder.companyName']").each(function() {
        $(this).rules("add", y)
    });
    $("#eForm .complete-eligibility-form input[name='form.applicantPlaceHolder.companyName']").each(function() {
        $(this).rules("add", y)
    });
    $("#newstaggered-carousel .existing-cc-options-required").each(function() {
        $(this).rules("add", {
            required: true,
            messages: {
                required: constructErrorMessage("Select none if you do not have any cards.")
            }
        })
    });
    $("#newstaggered-carousel .basic-required-text").each(function() {
        $(this).rules("add", u)
    });
    $("#newstaggered-carousel .basic-email").each(function() {
        $(this).rules("add", k)
    });
    $("#newstaggered-carousel .basic-email-required").each(function() {
        $(this).rules("add", k);
        $(this).rules("add", u)
    });
    $("#newstaggered-carousel .basic-email-required-loans").each(function() {
        $(this).rules("add", k);
        $(this).rules("add", A)
    });
    $("input[name='form.existingLoanBankName']").each(function() {
        $(this).rules("add", a)
    });
    $("#newstaggered-carousel select[name='form.existingLoanStartDate.year']").each(function() {
        $(this).rules("add", h)
    });
    $.validator.addMethod("eFormOutstandingBalanceValidator", function(K, J) {
        var I = $("#eForm_form_details_property_propertyPurchaseType:checked").val();
        var L;
        switch ($("#eForm_form_details_property_propertyConstructionType:checked").val()) {
            case "CONSTRUCTED":
            case "UNDER_CONSTRUCTION":
                L = parseInt($("input[name='form.details.property.costOfProperty']").val().replace(/,/g, ""));
                break;
            case "PURCHASE_LAND":
                L = parseInt($("input[name='form.details.property.costOfLand']").val().replace(/,/g, ""));
                break;
            case "CONSTRUCT_ON_OWN_LAND":
            case "PURCHASE_LAND_AND_CONSTRUCT":
                L = (parseInt($("input[name='form.details.property.costOfLand']").val().replace(/,/g, "")) + parseInt($("input[name='form.details.property.costOfConstruction']").val().replace(/,/g, "")));
                break
        }
        return I == "TRANSFER_EXISTING_HOME_LOAN" ? parseInt($("#eForm_form_details_loanToTransfer_outstandingLoanAmount").val().replace(/,/g, "")) < L : true
    }, constructErrorMessage("Uh Oh! Can't exceed property/construction cost"));
    $.validator.addMethod("eFormMinimumOutstandingBalanceValidator", function(K, J) {
        var I = $("#eForm_form_details_property_propertyPurchaseType:checked").val();
        return I == "TRANSFER_EXISTING_HOME_LOAN" ? parseInt($("#eForm_form_details_loanToTransfer_outstandingLoanAmount").val().replace(/,/g, "")) >= 500000 : true
    }, constructErrorMessage("Below minimum funding norms"));
    $("#newstaggered-carousel .minimum-outstanding-balance").each(function() {
        $(this).rules("add", l);
        $(this).rules("add", {
            eFormOutstandingBalanceValidator: true
        });
        $(this).rules("add", {
            eFormMinimumOutstandingBalanceValidator: true
        })
    });
    $("#newstaggered-carousel .monthly-income-required").each(function() {
        $(this).rules("add", o);
        $(this).rules("add", {
            eFormMinMonthlyIncomeValidator: true
        })
    });
    $("#newstaggered-carousel .annual-income-required").each(function() {
        $(this).rules("add", {
            required: true,
            messages: {
                required: constructErrorMessage("Hi! please provide your gross annual income to proceed")
            }
        });
        $(this).rules("add", {
            eFormMinAnnualIncomeValidator: true
        })
    });
    $("#newstaggered-carousel .maximum-credit-required").each(function() {
        $(this).rules("add", w);
        $(this).rules("add", {
            eFormMaximumCreditValidator: true
        })
    });
    $.validator.addMethod("eFormBankMonthValidator", function(J, I) {
        return !($.trim(J) == "" && Number($("select[name='form.existingLoanStartDate.year']").val()) != (getServerDate().getFullYear() - 2))
    }, constructErrorMessage("Uh-oh! Please pick a month"));
    $.validator.addMethod("eFormExistingLoanStartDateValidator", function(L, I) {
        var K = parseInt($("select[name='form.existingLoanStartDate.year']").val().replace(/,/g, ""));
        var J = parseInt($("select[name='form.existingLoanStartDate.month']").val().replace(/,/g, ""));
        return !(Number(K) == Number(getCurrentYear()) && Number(J) > Number(getCurrentMonth()))
    }, constructErrorMessage("Uh-oh! Please pick valid date"));
    $("#newstaggered-carousel select[name='form.existingLoanStartDate.month']").each(function() {
        $(this).rules("add", {
            eFormBankMonthValidator: true
        });
        $(this).rules("add", {
            eFormExistingLoanStartDateValidator: true
        })
    });
    $.validator.addMethod("eFormMinIntendedAmountValidator", function(K, I) {
        var J = K.replace(new RegExp(",", "g"), "");
        if (J != "" && (J < 50000 || J > 3000000)) {
            return false
        }
        return true
    }, constructErrorMessage("Enter valid loan Amount"));
    $("#newstaggered-carousel .intended-loan-amount-validator").each(function() {
        $(this).rules("add", {
            eFormMinIntendedAmountValidator: true
        })
    });
    $.validator.addMethod("eFormJoiningMonthValidator", function(L, I) {
        var K = parseInt($("#eForm_form_applicantPlaceHolder_employmentAbroadStartDate_year").val());
        var J = parseInt($("#eForm_form_applicantPlaceHolder_employmentAbroadStartDate_month").val());
        return !(Number(K) == Number(getCurrentYear()) && Number(J) > Number(getCurrentMonth() + 1))
    }, constructErrorMessage("Uh-oh! Please select valid date"));
    $("#newstaggered-carousel .hl-joining-month-required").each(function() {
        $(this).rules("add", {
            eFormJoiningMonthValidator: true
        })
    });
    $("#newstaggered-carousel .hl-joining-year-abroad-required").each(function() {
        $(this).rules("add", {
            required: true,
            messages: {
                required: constructErrorMessage("Uh Oh! Please select joining year")
            }
        })
    });
    $.validator.addMethod("eFormJoinMonthHlValidator", function(J, I) {
        return !($.trim(J) == "" && $("input[name='form.applicantPlaceHolder.employmentStartDate.year']").val() != s)
    }, constructErrorMessage("And your month of joining?"));
    $.validator.addMethod("eFormCoappJoinMonthHlValidator", function(J, I) {
        return !($.trim(J) == "" && $("input[name='form.coApplicantPlaceHolder.employmentStartDate.year']").val() != s)
    }, constructErrorMessage("And your month of joining?"));
    $(".complete-eligibility-form .resident-indian-work-exp#eForm_form_applicantPlaceHolder_residentIndian_totalWorkExperience").each(function() {
        $(this).rules("add", {
            required: true,
            messages: {
                required: constructErrorMessage("And your total years of experience please!!")
            }
        })
    });
    $(".complete-eligibility-form .resident-indian-work-exp#eForm_form_coApplicantPlaceHolder_residentIndian_totalWorkExperience").each(function() {
        $(this).rules("add", {
            required: true,
            messages: {
                required: constructErrorMessage("And co-applicant's total years of experience please!!")
            }
        })
    });
    $("#newstaggered-carousel .hl-joining-month-required0").each(function() {
        $(this).rules("add", {
            eFormJoinMonthHlValidator: true
        });
        $(this).rules("add", {
            eFormHlJoinMonthYearExpCrossValidator: true
        })
    });
    $("#newstaggered-carousel .hl-joining-month-required1").each(function() {
        $(this).rules("add", {
            eFormCoappJoinMonthHlValidator: true
        });
        $(this).rules("add", {
            eFormHlCoappJoinMonthYearExpCrossValidator: true
        })
    });
    $.validator.addMethod("eFormSlideAllFieldsEmptyForHlValidator", function(J, I) {
        return !checkAllFieldsEmptyInASlide($(I).data("class"))
    }, constructErrorMessage("Uh Oh! please select an input to proceed"));
    $.validator.addMethod("eFormJoiningYearValidator", function(J, I) {
        return !(!checkAllFieldsEmptyInASlide($(I).data("class")) && $.trim(J) == "")
    }, constructErrorMessage("Uh Oh! Please select joining year"));
    $.validator.addMethod("eFormJoinDateHlValidator", function(L, I) {
        var K = parseInt($("#eForm_form_applicantPlaceHolder_employmentStartDate_year").val());
        var J = parseInt($("#eForm_form_applicantPlaceHolder_employmentStartDate_month").val());
        return !(Number(K) == Number(getCurrentYear()) && Number(J) > Number(getCurrentMonth()))
    }, constructErrorMessage("Uh Oh! Please select a valid joining date"));
    $.validator.addMethod("eFormCoappJoinDateHlValidator", function(L, I) {
        var K = parseInt($("#eForm_form_coApplicantPlaceHolder_employmentStartDate_year").val());
        var J = parseInt($("#eForm_form_coApplicantPlaceHolder_employmentStartDate_month").val());
        return !(Number(K) == Number(getCurrentYear()) && Number(J) > Number(getCurrentMonth()))
    }, constructErrorMessage("Uh Oh! Please select a valid joining date"));
    $("#newstaggered-carousel .joining-date.hl-joining-year-required0").each(function() {
        $(this).rules("add", {
            eFormJoiningYearValidator: true
        });
        $(this).rules("add", {
            eFormJoinDateHlValidator: true
        })
    });
    $("#newstaggered-carousel .hl-joining-year-required1").each(function() {
        $(this).rules("add", {
            eFormJoiningYearValidator: true
        });
        $(this).rules("add", {
            eFormCoappJoinDateHlValidator: true
        })
    });
    $.validator.addMethod("eFormMobileJoiningMonthRequiredValidator", function(L, I) {
        var K = parseInt($("#eForm_form_applicantPlaceHolder_employmentStartDate_year").val());
        var J = parseInt($("#eForm_form_applicantPlaceHolder_businessStartDate_month").val());
        return !(L == "" && Number(K) != getServerDate().getFullYear() - 5)
    }, constructErrorMessage("Uh-oh! Please select joining month"));
    $("#newstaggered-carousel .hl-joining-month-mobile-required").each(function() {
        $(this).rules("add", {
            eFormMobileJoiningMonthRequiredValidator: true
        })
    });
    $.validator.addMethod("eFormMobileJoiningMonthAbroadRequiredValidator", function(K, I) {
        if ($(".complete-eligibility-form #eForm_form_applicantPlaceHolder_employmentAbroadStartDate_month").attr("disabled") != "disabled") {
            var J = parseInt($("#eForm_form_applicantPlaceHolder_employmentAbroadStartDate_year").val());
            return !(K == "" && Number(J) != getServerDate().getFullYear() - 5)
        }
        return true
    }, constructErrorMessage("Uh-oh! Please select joining month"));
    $("#newstaggered-carousel .hl-joining-month-abroad-mobile-required").each(function() {
        $(this).rules("add", {
            eFormMobileJoiningMonthAbroadRequiredValidator: true
        })
    });
    $.validator.addMethod("eFormCoAppMobileJoiningMonthRequiredValidator", function(L, I) {
        var K = parseInt($("#eForm_form_coApplicantPlaceHolder_employmentStartDate_year").val());
        var J = parseInt($("#eForm_form_coApplicantPlaceHolder_businessStartDate_month").val());
        return !(L == "" && Number(K) != getServerDate().getFullYear() - 5)
    }, constructErrorMessage("Uh-oh! Please select joining month"));
    $("#newstaggered-carousel .work-abroad-date-slide .hl-coapp-joining-month-mobile-required").each(function() {
        $(this).rules("add", {
            eFormCoAppMobileJoiningMonthRequiredValidator: true
        })
    });
    $.validator.addMethod("eFormCoAppMobileJoiningMonthAbroadRequiredValidator", function(L, I) {
        if ($(".complete-eligibility-form #eForm_form_coApplicantPlaceHolder_employmentAbroadStartDate_month").attr("disabled") != "disabled") {
            var K = parseInt($("#eForm_form_coAapplicantPlaceHolder_employmentAbroadStartDate_year").val());
            var J = parseInt($("#eForm_form_coApplicantPlaceHolder_employmentAbroadStartDate_month").val());
            return !(L == "" && Number(K) != getServerDate().getFullYear() - 5)
        }
        return true
    }, constructErrorMessage("Uh-oh! Please select joining month"));
    $("#newstaggered-carousel .hl-coapp-joining-month-abroad-mobile-required").each(function() {
        $(this).rules("add", {
            eFormCoAppMobileJoiningMonthAbroadRequiredValidator: true
        })
    });
    $.validator.addMethod("eFormCoappJoiningMonthValidator", function(L, I) {
        var K = parseInt($("#eForm_form_coApplicantPlaceHolder_employmentStartDate_year").val());
        var J = parseInt($("#eForm_form_coApplicantPlaceHolder_businessStartDate_month").val());
        return !(Number(K) == Number(getCurrentYear()) && Number(J) > Number(getCurrentMonth()) + 1)
    }, constructErrorMessage("Uh-oh! Please select valid date"));
    $("#newstaggered-carousel .hl-coapplicant-joining-month-required").each(function() {
        $(this).rules("add", {
            eFormCoappJoiningMonthValidator: true
        })
    });
    $.validator.addMethod("eFormCoappAbroadJoiningMonthValidator", function(L, I) {
        var K = parseInt($("#eForm_form_coAapplicantPlaceHolder_employmentAbroadStartDate_year").val());
        var J = parseInt($("#eForm_form_coApplicantPlaceHolder_employmentAbroadStartDate_month").val());
        return !(Number(K) == Number(getCurrentYear()) && Number(J) > Number(getCurrentMonth() + 1))
    }, constructErrorMessage("Uh-oh! Please select a valid date"));
    $("#newstaggered-carousel .hl-coapplicant-joining-month-abroad-required").each(function() {
        $(this).rules("add", {
            eFormCoappAbroadJoiningMonthValidator: true
        })
    });
    $.validator.addMethod("eFormJoiningMonthAbroadValidator", function(L, I) {
        var K = parseInt($("#eForm_form_applicantPlaceHolder_employmentAbroadStartDate_year").val());
        var J = parseInt($("#eForm_form_applicantPlaceHolder_employmentAbroadStartDate_month").val());
        return !(Number(K) == Number(getCurrentYear()) && Number(J) > Number(getCurrentMonth() + 1))
    }, constructErrorMessage("Uh-oh! Please select a valid date"));
    $("#newstaggered-carousel .hl-joining-month-abroad-required").each(function() {
        $(this).rules("add", {
            eFormJoiningMonthAbroadValidator: true
        })
    });
    $("#newstaggered-carousel .employment-type-required").each(function() {
        $(this).rules("add", i)
    });
    $("#newstaggered-carousel .hl-dob-day-required").each(function() {
        $(this).rules("add", p);
        $(this).rules("add", {
            eFormDobDayRangeValidator: true
        })
    });
    $("#newstaggered-carousel .hl-dob-month-required").each(function() {
        $(this).rules("add", E);
        $(this).rules("add", {
            eFormDobMonthRangeValidator: true
        })
    });
    $("#newstaggered-carousel .hl-dob-year-required").each(function() {
        $(this).rules("add", f);
        $(this).rules("add", {
            eFormDobYearRangeValidator: true
        });
        $(this).rules("add", {
            eFormDobYearValidator: true
        });
        $(this).rules("add", {
            eFormHlDobAgeValidator: true
        })
    });
    $("#newstaggered-carousel .hl-coapplicant-dob-year-required").each(function() {
        $(this).rules("add", f);
        $(this).rules("add", {
            eFormDobYearRangeValidator: true
        });
        $(this).rules("add", {
            eFormDobYearValidator: true
        });
        $(this).rules("add", {
            eFormHlCoApplicantDobAgeValidator: true
        })
    });
    $.validator.addMethod("eFormDobDayRangeValidator", function(M, J) {
        var K = J.id.split("_");
        K.pop();
        var L = "#" + K.join("_");
        var I = $(L + "_day").val();
        return (I == "" || I <= 31)
    }, constructErrorMessage("Uh-oh! Are you sure that is the correct day?"));
    $.validator.addMethod("eFormDobMonthRangeValidator", function(L, I) {
        var J = I.id.split("_");
        J.pop();
        var K = "#" + J.join("_");
        var M = $(K + "_month").val();
        if (M.length > 1) {
            return (M == "" || (M <= 12 && M != 0))
        }
        return (M == "" || M <= 12)
    }, constructErrorMessage("Uh-oh! Are you sure that is the correct month?"));
    $.validator.addMethod("eFormDobYearRangeValidator", function(M, I) {
        var J = I.id.split("_");
        J.pop();
        var L = "#" + J.join("_");
        var K = $(L + "_year").val();
        return !((K < 1900) || (K > getServerDate().getFullYear()))
    }, constructErrorMessage("Uh-oh! Are you sure that is the correct year?"));
    $.validator.addMethod("eFormDobYearValidator", function(N, J) {
        var K = J.id.split("_");
        K.pop();
        var M = "#" + K.join("_");
        var L = $(M + "_year").val();
        var P = $(M + "_month").val();
        var I = $(M + "_day").val();
        var O = new Date(L, P - 1, I);
        return ((I == "" || I > 31 || P == "" || P > 12 || P == 0) || !((O.getMonth() + 1 != P) || (O.getDate() != I)))
    }, constructErrorMessage("Uh-oh! Please pick a valid Date!"));
    $.validator.addMethod("eFormHlDobAgeValidator", function(O, K) {
        var L = K.id.split("_");
        L.pop();
        var N = "#" + L.join("_");
        var M = $(N + "_year").val();
        var P = $(N + "_month").val();
        var J = $(N + "_day").val();
        var I = getServerDate().getFullYear() - parseInt(M);
        if (P > (getServerDate().getMonth() + 1) || (P == (getServerDate().getMonth() + 1) && getServerDate().getDate() < parseInt(J))) {
            I--
        }
        return ((J == "" || J > 31 || P == "" || P > 12) || I > 17)
    }, constructErrorMessage("Uh-oh! You are below the minimum age requirement"));
    $.validator.addMethod("eFormHlCoApplicantDobAgeValidator", function(O, K) {
        var L = K.id.split("_");
        L.pop();
        var N = "#" + L.join("_");
        var M = $(N + "_year").val();
        var P = $(N + "_month").val();
        var J = $(N + "_day").val();
        var I = getServerDate().getFullYear() - parseInt(M);
        if (P > (getServerDate().getMonth() + 1) || (P == (getServerDate().getMonth() + 1) && getServerDate().getDate() < parseInt(J))) {
            I--
        }
        return ((J == "" || J > 31 || P == "" || P > 12) || I > 17)
    }, constructErrorMessage("Uh-oh! Co-applicant is below the minimum age requirement"));
    var D = getCurrentYear();
    var s = D - 5;
    $.validator.addMethod("eFormHlJoinMonthYearExpCrossValidator", function(O, K) {
        var N;
        var M;
        if ($("input[name='form.details.applicant.citizenshipStatus']").val() == BBConstants.residentIndian) {
            M = $("#eForm_form_applicantPlaceHolder_employmentStartDate_month").val();
            N = $("#eForm_form_applicantPlaceHolder_employmentStartDate_year").val()
        } else {
            M = $("#eForm_form_applicantPlaceHolder_employmentAbroadStartDate_month").val();
            N = $("#eForm_form_applicantPlaceHolder_employmentAbroadStartDate_year").val()
        }
        var L = parseInt($.trim(N).replace(new RegExp(",", "g"), ""));
        var I = parseInt($.trim(M).replace(new RegExp(",", "g"), "") == "" ? -1 : M) + 1;
        var J = parseInt($.trim($("#eForm_form_applicantPlaceHolder_residentIndian_totalWorkExperience").val()));
        var P = D - L;
        return ((P - (I > 0 ? (I / 12) : 0)) > J ? false : true)
    }, constructErrorMessage("Uh Oh! Your total work experience should be more than time with your current company"));
    $.validator.addMethod("eFormAbroadJoinMonthYearExpCrossValidator", function(N, K) {
        var J = $("#eForm_form_applicantPlaceHolder_employmentAbroadStartDate_month");
        var M = parseInt($.trim($("#eForm_form_applicantPlaceHolder_employmentAbroadStartDate_year").val()).replace(new RegExp(",", "g"), ""));
        var L = parseInt($.trim(J.val().replace(new RegExp(",", "g"), ""))) == "" ? -1 : J.val();
        var I = parseInt($("select[name='form.applicantPlaceHolder.totalWorkExperience']").val());
        var O = D - M;
        return ((O - (L > 0 ? (L / 12) : 0)) > I ? false : true)
    }, constructErrorMessage("Uh Oh! Your total work experience should be more than time with your current company"));
    $.validator.addMethod("eFormHlCoappJoinMonthYearExpCrossValidator", function(O, K) {
        var N;
        var M;
        if ($("input[name='form.details.coApplicant.citizenshipStatus']").val() == BBConstants.residentIndian) {
            M = $("#eForm_form_coApplicantPlaceHolder_employmentStartDate_month").val();
            N = $("#eForm_form_coApplicantPlaceHolder_employmentStartDate_year").val()
        } else {
            M = $("#eForm_form_coApplicantPlaceHolder_employmentAbroadStartDate_month").val();
            N = $("#eForm_form_coAapplicantPlaceHolder_employmentAbroadStartDate_year").val()
        }
        var L = parseInt($.trim(N).replace(new RegExp(",", "g"), ""));
        var I = parseInt($.trim(M).replace(new RegExp(",", "g"), "") == "" ? -1 : M) + 1;
        var J = parseInt($.trim($("#eForm_form_coApplicantPlaceHolder_residentIndian_totalWorkExperience").val()));
        var P = D - L;
        return ((P - (I > 0 ? (I / 12) : 0)) > J ? false : true)
    }, constructErrorMessage("Uh Oh! Co-applicant's total work experience should be more than time with their current company"));
    $.validator.addMethod("eFormCoAppAbroadJoinMonthYearExpCrossValidator", function(N, K) {
        var J = $("#eForm_form_coApplicantPlaceHolder_employmentAbroadStartDate_month");
        var M = parseInt($.trim($("#eForm_form_coAapplicantPlaceHolder_employmentAbroadStartDate_year").val()).replace(new RegExp(",", "g"), ""));
        var L = parseInt($.trim(J.val()).replace(new RegExp(",", "g"), "") == "" ? -1 : J.val()) + 1;
        var I = parseInt($("#eForm_form_coApplicantPlaceHolder_totalWorkExperience").val());
        var O = D - M;
        return ((O - (L > 0 ? (L / 12) : 0)) > I ? false : true)
    }, constructErrorMessage("Uh Oh! Your total work experience should be more than time with your current company"));
    $("#newstaggered-carousel .monthly-gross-income-required").each(function() {
        $(this).rules("add", {
            required: true,
            messages: {
                required: constructErrorMessage("Hi! please provide your gross monthly income to proceed")
            }
        });
        $(this).rules("add", {
            eFormMinMonthlyIncomeValidator: true,
            messages: {
                eFormMinMonthlyIncomeValidator: constructErrorMessage("Hi! We need your gross monthly salary")
            }
        })
    });
    $("#newstaggered-carousel .applicant-profit-after-tax-required").each(function() {
        $(this).rules("add", {
            required: true,
            messages: {
                required: constructErrorMessage("Hi! please provide your Latest year's profit after tax to proceed")
            }
        });
        $(this).rules("add", {
            eFormMinMonthlyIncomeValidator: true,
            messages: {
                eFormMinMonthlyIncomeValidator: constructErrorMessage("Hi! We need your profit after tax")
            }
        })
    });
    $("#newstaggered-carousel .name-required").each(function() {
        $(this).rules("add", {
            required: true,
            messages: {
                required: constructErrorMessage("Uh-oh! Please enter your name here")
            }
        });
        $(this).rules("add", {
            eFormNameValidator: true
        })
    });
    $("#newstaggered-carousel .basic-required-options").each(function() {
        $(this).rules("add", q)
    });
    $("#newstaggered-carousel .basic-pick-options").each(function() {
        $(this).rules("add", i)
    });
    $("#newstaggered-carousel .basic-city-options").each(function() {
        $(this).rules("add", e)
    });
    $("#newstaggered-carousel .hl-year-required").each(function() {
        $(this).rules("add", v)
    });
    $("#newstaggered-carousel .basic-select-options").each(function() {
        $(this).rules("add", t)
    });
    $("#newstaggered-carousel .hl-work-experience-required:enabled").each(function() {
        $(this).rules("add", C);
        $(this).rules("add", {
            eFormAbroadJoinMonthYearExpCrossValidator: true
        })
    });
    $("#newstaggered-carousel .hl-coapp-work-experience-required:enabled").each(function() {
        $(this).rules("add", z);
        $(this).rules("add", {
            eFormCoAppAbroadJoinMonthYearExpCrossValidator: true
        })
    });
    $("#newstaggered-carousel .easy-options").each(function() {
        $(this).rules("add", G)
    });
    $("#newstaggered-carousel .goahead-options").each(function() {
        $(this).rules("add", H)
    });
    $("#newstaggered-carousel .value-required-text").each(function() {
        $(this).rules("add", r)
    });
    $("#newstaggered-carousel .hl-monthly-gross-income-required").each(function() {
        $(this).rules("add", F);
        $(this).rules("add", {
            eFormMinMonthlyIncomeValidator: true,
            messages: {
                eFormMinMonthlyIncomeValidator: constructErrorMessage("Uh Oh! We need your gross monthly income")
            }
        })
    });
    $("#newstaggered-carousel .coapp-monthly-gross-income-required").each(function() {
        $(this).rules("add", B);
        $(this).rules("add", {
            eFormMinMonthlyIncomeValidator: true,
            messages: {
                eFormMinMonthlyIncomeValidator: constructErrorMessage("Uh Oh! We need co-applicant's gross monthly income")
            }
        })
    });
    $("#newstaggered-carousel .max-char-limit-12").each(function() {
        $(this).rules("add", g)
    });
    $("#newstaggered-carousel .basic-mobile-phone").each(function() {
        $(this).rules("add", d)
    });
    $("#newstaggered-carousel .basic-mobile-phone-required").each(function() {
        $(this).rules("add", d);
        $(this).rules("add", {
            required: true,
            messages: {
                required: constructErrorMessage("Your number? We’ll call you only to process your loan")
            }
        })
    });
    $("#newstaggered-carousel .basic-mobile-phone-required-for-cc").each(function() {
        $(this).rules("add", d);
        $(this).rules("add", {
            required: true,
            messages: {
                required: constructErrorMessage("Your number? We’ll call you only to process your card")
            }
        })
    });
    $.validator.addMethod("eFormDobAgeValidator", function(O, L) {
        var J = O.split(" ")[0];
        var P = $.inArray(O.split(" ")[1], BBConstants.monthNames) + 1;
        var M = O.split(" ")[2];
        try {
            var K = new Date(M, P, J)
        } catch (N) {
            return false
        }
        var I = getServerDate().getFullYear() - parseInt(M);
        if (P > (getServerDate().getMonth() + 1) || (P == (getServerDate().getMonth() + 1) && getServerDate().getDate() < parseInt(J))) {
            I--
        }
        return (I >= 18)
    }, constructErrorMessage("You are below the minimum age requirement"));
    $("#newstaggered-carousel .basic-required-date").each(function() {
        if ($(window).width() <= 640) {
            $(this).rules("add", {
                eFormMobileEmptyAgeValidator: true
            });
            $(this).rules("add", {
                eFormDateValidator: true
            });
            $(this).rules("add", {
                eFormDobAgeValidator: true
            })
        } else {
            $("#newstaggered-carousel .basic-required-date").each(function() {
                $(this).rules("add", {
                    eFormEmptyAgeValidator: true
                });
                $(this).rules("add", {
                    eFormAgeValidator: true
                });
                $(this).rules("add", {
                    eFormDateValidator: true
                });
                $(this).rules("add", {
                    eFormDobAgeValidator: true
                })
            })
        }
    });
    $("#newstaggered-carousel .authorize-to-call").each(function() {
        $(this).rules("add", {
            required: true,
            messages: {
                required: constructErrorMessage("Please accept to view offers")
            }
        })
    });
    $("#newstaggered-carousel .select-option-required").each(function() {
        $(this).rules("add", {
            required: true,
            messages: {
                required: constructErrorMessage("Hi! Please select an option to proceed")
            }
        })
    });
    $("#newstaggered-carousel .select-option-required-for-hl").each(function() {
        $(this).rules("add", {
            required: true,
            messages: {
                required: constructErrorMessage("Uh-oh! Please select an option")
            }
        })
    });
    $("#newstaggered-carousel .basic-manufacture-year").each(function() {
        $(this).rules("add", x)
    })
}

function addMonthYearComponentValidationRules() {
    var b = getCurrentYear();
    var a = b - 5;
    $.validator.addMethod("eFormJoinMonthYearExpCrossValidator", function(i, f) {
        var e = $("#eForm_form_applicantPlaceHolder_employmentStartDate_month");
        var h = parseInt($("#eForm_form_applicantPlaceHolder_employmentStartDate_year").val());
        var g = parseInt($.trim(e.val()) == "" ? -1 : e.val()) + 1;
        var d = parseInt($("#eForm_form_applicantPlaceHolder_totalWorkExperience").val());
        var j = b - h;
        return ((j - (g > 0 ? (g / 12) : 0)) > d ? false : true)
    }, constructErrorMessage("Hi! Your total work experience should be more than time with your current company"));
    $.validator.addMethod("eFormJoinDateValidator", function(g, d) {
        var f = parseInt($("#eForm_form_applicantPlaceHolder_employmentStartDate_year").val());
        var e = parseInt($("#eForm_form_applicantPlaceHolder_employmentStartDate_month").val());
        return !(Number(f) == Number(getCurrentYear()) && Number(e) > Number(getCurrentMonth()))
    }, constructErrorMessage("Hi! Please select a valid joining date"));
    $.validator.addMethod("eFormResidenceMonthYearCrossValidator", function(g, f) {
        var d = parseInt($("#eForm_form_applicantPlaceHolder_residenceStartDate_year").val());
        var i = parseInt($("#eForm_form_applicantPlaceHolder_cityStartDate_year").val());
        var h = parseInt($("#eForm_form_applicantPlaceHolder_residenceStartDate_month").val());
        var e = parseInt($("#eForm_form_applicantPlaceHolder_cityStartDate_month").val());
        if (d != "" && i != "" && (d < i)) {
            return false
        } else {
            if ((d == getCurrentYear() && h > Number(getCurrentMonth())) || (i == getCurrentYear() && e > Number(getCurrentMonth()))) {
                return false
            } else {
                if ((d == i) && d != a && i != a) {
                    if (h < e) {
                        return false
                    }
                }
            }
        }
        return true
    }, constructErrorMessage("Uh-oh! Are you sure that's the correct date?"));
    $.validator.addMethod("eFormSlideAllFieldsEmptyValidator", function(e, d) {
        return !checkAllFieldsEmptyInASlide($(d).data("class"))
    }, constructErrorMessage("Hi! please select an input to proceed"));
    $.validator.addMethod("eFormYearValidator", function(e, d) {
        return !(!checkAllFieldsEmptyInASlide($(d).data("class")) && $.trim(e) == "")
    }, constructErrorMessage("Hi! Please select your year of joining"));
    $.validator.addMethod("eFormJoinMonthValidator", function(e, d) {
        return !($.trim(e) == "" && $("input[name='form.applicantPlaceHolder.employmentStartDate.year']").val() != a)
    }, constructErrorMessage("And your month of joining?"));
    $.validator.addMethod("eFormResidenceMonthValidator", function(e, d) {
        if ($.trim(e) == "" && $.trim($("#" + $(d).data("year-id")).val()) != a) {
            return false
        }
        return true
    }, constructErrorMessage("And your month of move to current residence?"));
    $("#newstaggered-carousel  #eForm_form_applicantPlaceHolder_employmentStartDate_year").rules("add", {
        eFormSlideAllFieldsEmptyValidator: true
    });
    $("#newstaggered-carousel  #eForm_form_applicantPlaceHolder_employmentStartDate_year").rules("add", {
        eFormYearValidator: true,
        eFormJoinDateValidator: true
    });
    $("#newstaggered-carousel  #eForm_form_applicantPlaceHolder_employmentStartDate_month").rules("add", {
        eFormJoinMonthValidator: true,
        eFormJoinMonthYearExpCrossValidator: true,
        eFormJoinDateValidator: true
    });
    $("#newstaggered-carousel  #eForm_form_applicantPlaceHolder_totalWorkExperience").rules("add", {
        required: true,
        messages: {
            required: constructErrorMessage("And your total years of experience please!!")
        }
    });
    $("#newstaggered-carousel  #eForm_form_applicantPlaceHolder_residenceStartDate_year").rules("add", {
        eFormSlideAllFieldsEmptyValidator: true
    });
    $("#newstaggered-carousel  #eForm_form_applicantPlaceHolder_residenceStartDate_year").rules("add", {
        eFormYearValidator: true,
        messages: {
            eFormYearValidator: constructErrorMessage("Hi! please select your year of move to current residence")
        }
    });
    $("#newstaggered-carousel #eForm_form_applicantPlaceHolder_cityStartDate_year").rules("add", {
        required: true,
        messages: {
            required: constructErrorMessage("Hi! please select your year of move to current city")
        }
    });
    $("#newstaggered-carousel #eForm_form_applicantPlaceHolder_residenceStartDate_month").rules("add", {
        eFormResidenceMonthValidator: true
    });
    $("#newstaggered-carousel #eForm_form_applicantPlaceHolder_cityStartDate_month").rules("add", {
        eFormResidenceMonthValidator: true,
        eFormResidenceMonthYearCrossValidator: true,
        messages: {
            eFormResidenceMonthValidator: constructErrorMessage("And your month of move to current city?")
        }
    });
    $("#newstaggered-carousel #eForm_form_details_applicant_residenceType").rules("add", {
        required: true,
        messages: {
            required: constructErrorMessage("Hi! please select your residence type")
        }
    })
}

function addCarValidationRules() {
    $.validator.addMethod("eFormOtherCarValidator", function(b, a) {
        if ($("input[name='form.carName']:checked").val().toUpperCase() == BBConstants.otherValue) {
            return ($("#staggeredCarNameID").val() != "" && $("#carName_fallback_value_textbox").val() != "")
        }
        return true
    }, constructErrorMessage("Hi! Please enter a car to proceed"));
    $("#newstaggered-carousel .other-car-required-when-selected").each(function() {
        $(this).rules("add", {
            eFormOtherCarValidator: true
        })
    })
}

function addPropertyCostValidationRules() {
    $.validator.addMethod("eFormPropertyCostValidator", function(b, a) {
        if ($.trim(b) != "" && $.trim(b).replace(new RegExp(",", "g"), "") < 580000) {
            return false
        }
        return true
    }, constructErrorMessage("Below minimum funding norms"));
    $("#newstaggered-carousel .minimum-property-cost-validation").each(function() {
        $(this).rules("add", {
            eFormPropertyCostValidator: true
        })
    })
}

function addStandAlonePropertyCostValidationRules() {
    $.validator.addMethod("eFormStandAlonePropertyCostValidator", function(b, a) {
        if ($.trim(b) != "" && $.trim(b).replace(new RegExp(",", "g"), "") < 500000) {
            return false
        }
        return true
    }, constructErrorMessage("Below minimum funding norms"));
    $("#newstaggered-carousel .minimum-land-cost").each(function() {
        $(this).rules("add", {
            eFormStandAlonePropertyCostValidator: true
        })
    })
}

function checkAllFieldsEmptyInASlide(b) {
    var a = true;
    $("." + b).each(function(d, e) {
        var f = $(e).prop("tagName") == "SELECT" ? $(e).find("option:selected").val() : $(e).val();
        if ($.trim(f) != "" && $.trim(f) != "0") {
            a = false
        }
    });
    return a
}
var cc_slide_queue;
var cl_slide_queue;
var REMOVEVALIDATION = "remove";
var ADDVALIDATION = "add";
var EXISTINGCCSLIDEINDEX = 5;
var addCustomEventHandlers = function() {
    var k = new RegExp("^#", "g");
    var d = k.test($("#selfEmployedToggleBtn").attr("href"));
    var p = function() {
        if ($("#form_applicantPlaceHolder_residenceCity_fallback").val() != "") {
            e($("#form_applicantPlaceHolder_residenceCity_fallback").val());
            slideNext()
        }
    };
    var a = function() {
        if ($("#staggeredCarNameID").val() != "") {
            slideNext()
        }
    };
    var j = 1000;

    function b() {
        if ($("#productNameSpace").val() == "personal-loan" || $("#productNameSpace").val() == "car-loan") {
            $('#newstaggered-carousel input[name="form.details.applicant.employment.type"]').val("SALARIED");
            n($('#newstaggered-carousel input[name="form.details.applicant.employment.type"]').val())
        }
    }

    function o(r) {
        if (r.val() == "SELF_EMPLOYED_BUSINESS") {
            $(".annual-income-slider").slider("setValue", $("input[name='form.details.applicant.income.latestProfitAfterTax']").val())
        } else {
            $(".monthly-income-slider").slider("setValue", $("input[name='form.details.applicant.income.monthlyTakeHomeSalary']").val())
        }
    }

    function f(r) {
        r.attr("disabled", true).parents().filter("li:first").addClass("dontshow");
        r.removeClass("validate");
        r.addClass("dontvalidate")
    }

    function l(s, r) {
        f(s);
        if ($("#productNameSpace").val() == "car-loan") {
            moveToNextSlide()
        } else {
            slideNext()
        }
    }

    function n(r) {
        if ($(".btn-toggle").find(".active").data("value") != r) {
            $(".btn-toggle").find(".btn").toggleClass("active");
            if ($(".btn-toggle").find(".btn-primary").size() > 0) {
                $(".btn-toggle").find(".btn").toggleClass("btn-primary")
            }
            $(".btn-toggle").find(".btn").toggleClass("btn-default");
            $('#newstaggered-carousel input[name="form.details.applicant.employment.type"]').val($(".btn-toggle").find(".active").data("value"));
            if (d) {
                o($('#newstaggered-carousel input[name="form.details.applicant.employment.type"]'));
                $('#newstaggered-carousel input[name="form.details.applicant.employment.type"]').trigger("change")
            }
        }
    }

    function m() {
        var s = $('input[name="form.applicantPlaceHolder.employmentStartDate.year"]');
        var t = $('input[name="form.applicantPlaceHolder.employmentStartDate.month"]');
        var r = $('input[name="form.applicantPlaceHolder.totalWorkExperience"]');
        var u = true;
        if ($.trim(s.val()) == "" || $.trim(r.val()) == "" || (Number(s.val()) > Number(getMinValueForYearComponent()) && $.trim(t.val()) == "")) {
            u = false
        } else {
            if ((Number(getCurrentYear()) - Number(s.val())) > Number(r.val())) {
                u = false
            } else {
                if ((Number(s.val()) == Number(getCurrentYear())) && (Number(t.val()) > Number(getCurrentMonth()))) {
                    u = false
                }
            }
        }
        if (u) {
            slideNext()
        }
    }

    function q() {
        var s = $('input[name="form.applicantPlaceHolder.residenceStartDate.year"]');
        var u = $('input[name="form.applicantPlaceHolder.residenceStartDate.month"]');
        var t = $('input[name="form.applicantPlaceHolder.cityStartDate.year"]');
        var r = $('input[name="form.applicantPlaceHolder.cityStartDate.month"]');
        var v = true;
        if ($.trim(s.val()) == "" || $.trim(t.val()) == "" || (s.val() > getMinValueForYearComponent() && $.trim(u.val()) == "") || (t.val() > getMinValueForYearComponent() && $.trim(r.val()) == "")) {
            v = false
        } else {
            if (s.val() < t.val()) {
                v = false
            } else {
                if ((s.val() == getCurrentYear() && u.val() > Number(getCurrentMonth())) || (t.val() == getCurrentYear() && r.val() > Number(getCurrentMonth()))) {
                    v = false
                } else {
                    if (s.val() != getMinValueForYearComponent() && t.val() != getMinValueForYearComponent() && s.val() == t.val() && u.val() < r.val()) {
                        v = false
                    }
                }
            }
        }
        if (v) {
            slideNext()
        }
    }

    function h(r) {
        $("#join_date_header").find("span:nth-child(1)").html(r);
        slideNext()
    }

    function i(r) {
        $("#join_date_header").find("span:nth-child(1)").html(r)
    }

    function e(r) {
        $("#prev_resident_city").find("span:nth-child(1)").html(changeToCamelCase(r))
    }
    $("#newstaggered-carousel select[name='form.applicantPlaceHolder.residenceCountry.value']").on("change", function() {
        if ($(this).val() != BBConstants.otherValue) {
            $("#fallbackForCountry0").addClass("dontshow");
            hideFields($("#fallbackForCountry0 input[name*=form]"));
            l($(".residency-status-slide select[name='form.applicantPlaceHolder.residenceCountry.recommend']"), $(".residency-status-slide input[name='form.applicantPlaceHolder.residenceCountry.fallback']"))
        } else {
            if ($(this).val() == BBConstants.otherValue) {
                $("#fallbackForCountry0").removeClass("dontshow");
                showFields($("#fallbackForCountry0 input[name*=form]"))
            }
        }
    });
    $("#newstaggered-carousel select[name='form.coApplicantPlaceHolder.residenceCountry.value']").on("change", function() {
        if ($(this).val() != BBConstants.otherValue) {
            $("#fallbackForCountry1").addClass("dontshow");
            hideFields($("#fallbackForCountry1 input[name*=form]"));
            l($(".residency-status-slide select[name='form.coApplicantPlaceHolder.residenceCountry.recommend']"), $(".residency-status-slide input[name='form.coApplicantPlaceHolder.residenceCountry.fallback']"))
        } else {
            if ($(this).val() == BBConstants.otherValue) {
                $("#fallbackForCountry1").removeClass("dontshow");
                showFields($("#fallbackForCountry1 input[name*=form]"))
            }
        }
    });
    $("#newstaggered-carousel select[name='form.applicantPlaceHolder.car.recommend']").on("change", function() {
        $("#newstaggered-carousel #fallBackName2").val($(this).val());
        l($("select[name='form.applicantPlaceHolder.car.recommend']"), $("input[name='carName_fallback_value_textbox']"))
    });
    $(".cityNameDiv .touch-icon").click(function(r) {
        $("#cityOther").val("");
        $(".item.active .recommendedSearchImg").remove()
    });
    $("#carSlide .carNameDiv").click(function(r) {
        $("#carName_fallback_value_textbox").val("");
        $(".item.active .recommendedSearchImg").remove()
    });
    $("#newstaggered-carousel input[name='carName_fallback_value_textbox']").on("change blur", function() {
        $(".newstaggered-modal input[name='fallBackName2']").val($(this).val().toUpperCase())
    });
    $("#eForm_form_applicantPlaceHolder_car_recommend").on("change blur", function() {
        $(".newstaggered-modal input[name='fallBackName2']").val($(this).val().toUpperCase())
    });
    $("#newstaggered-carousel input[name='form.applicantPlaceHolder.residenceCity.fallback1']").on({
        "typeahead:selected typeahead:autocompleted typeahead:closed": function(s, r) {
            if (r == undefined && $("#form_applicantPlaceHolder_residenceCity_fallback").val() == "") {
                checkExactMatch("CITY", $(this).val(), $("#form_applicantPlaceHolder_residenceCity_fallback"), p)
            }
        },
        input: function() {
            $("#form_applicantPlaceHolder_residenceCity_fallback").val("");
            c
        }
    });
    $("#newstaggered-carousel input[name='carName_fallback_value_textbox']").on({
        "typeahead:selected typeahead:autocompleted typeahead:closed": function(s, r) {
            if (r == undefined && $("#staggeredCarNameID").val() == "") {
                checkExactMatch("CAR", $(this).val(), $("#staggeredCarNameID"), a())
            }
        },
        input: function() {
            $("#staggeredCarNameID").val("");
            c
        }
    });
    $("#newstaggered-carousel input[name='form.applicantPlaceHolder.residenceCountry.fallback']").on("typeahead:selected typeahead:autocompleted", function() {
        slideNext()
    });
    $("#newstaggered-carousel input[name='form.coApplicantPlaceHolder.residenceCountry.fallback']").on("typeahead:selected typeahead:autocompleted", function() {
        slideNext()
    });
    $("#newstaggered-carousel input[name='form.carName']").change(function() {

        if ($(this).attr("checked") == "checked" && $(this).val().toUpperCase() != BBConstants.otherValue) {
            $("#newstaggered-carousel #staggeredCarNameID").val($(this).val());
            f($("select[name='form.applicantPlaceHolder.car.recommend']"));
            $("input[name='fallBackName2']").val("");
            moveToNextSlide()
        }
    });
    $("#newstaggered-carousel input[name='form.details.carType']").change(function() {
        if ($(this).attr("checked") == "checked") {
            $("#carType").val($(this).val());
            moveToNextSlide()
        }
    });
    $("#newstaggered-carousel #selfEmployedRadio").change(function() {
        if ($(this).attr("checked") == "checked") {
            var r = $("input[name='form.applicantPlaceHolder.companyName']");
            l($("select[name='form.applicantPlaceHolder.companyName.recommend']"), r)
        }
    });
    $("#newstaggered-carousel input[name='form.applicantPlaceHolder.companyName']").on("typeahead:selected typeahead:autocompleted", function() {
        h($(this).val())
    });
    $("#newstaggered-carousel input[name='form.coApplicantPlaceHolder.companyName']").on("typeahead:selected typeahead:autocompleted", function() {
        $("#join_date_header_coapplicant").find("span:nth-child(1)").html($(this).val());
        slideNext()
    });
    $("#newstaggered-carousel select[name='form.coApplicantPlaceHolder.companyName.recommend']").change(function() {
        $("#join_date_header_coapplicant").find("span:nth-child(1)").html($(this).val());
        l($("select[name='form.coApplicantPlaceHolder.companyName.recommend']"), $("input[name='form.coApplicantPlaceHolder.companyName']"))
    });
    $("#newstaggered-carousel input[name='form.builderProject']").on("typeahead:selected typeahead:autocompleted", function() {
        slideNext()
    });
    $("#newstaggered-carousel select[name='form.applicantPlaceHolder.companyName.recommend']").change(function() {
        h($(this).val());
        l($("select[name='form.applicantPlaceHolder.companyName.recommend']"), $("input[name='form.applicantPlaceHolder.companyName']"))
    });
    $("[name='form.coApplicantPlaceHolder.companyName']").on("change", function() {
        $("#join_date_header_coapplicant").find("span:nth-child(1)").html($(this).val())
    });
    $("[name='form.applicantPlaceHolder.companyName']").on("change", function() {
        i($(this).val().toUpperCase())
    });
    $('#newstaggered-carousel input[name="form.details.applicant.employment.type"]').change(function() {
        if ($(this).val() == "SELF_EMPLOYED_BUSINESS" && $("#productTypeId").val() != "2") {
            slideNext()
        }
    });
    $("#newstaggered-carousel .residence_type_list li.list-group-item").on("click", function() {
        $(this).parent().parent().find("li .radio").removeClass("checked");
        $(this).find(".radio").addClass("checked");
        $("input[name='form.details.applicant.residenceType']").val($(this).find("label").data("val"));
        slideNext()
    });
    $("#newstaggered-carousel div.residence-mo #residence_type_select_list").on("change", function() {
        $("input[name='form.details.applicant.residenceType']").val($(this).val());
        slideNext()
    });
    $("#newstaggered-carousel #totalWorkExperience").on("change", function() {
        $("input[name='form.applicantPlaceHolder.totalWorkExperience']").val($(this).val());
        m()
    });
    $("#newstaggered-carousel #totalWorkExperience1").on("change", function() {
        $("input[name='form.coApplicantPlaceHolder.totalWorkExperience']").val($(this).val());
        slideNextRuleForHlJoiningDate($("#eForm_form_coApplicantPlaceHolder_employmentStartDate_year"), $("#eForm_form_coApplicantPlaceHolder_employmentStartDate_month"))
    });
    $("#newstaggered-carousel #eForm_form_details_applicant_employment_type").on("change", function() {
        if (!($(this).val() == $(".radio.label-icon.checked.hl-emp-type").attr("for"))) {
            return
        }
        addAnimationForCTAButton();
        setTimeout(function() {
            $("#newstaggered-carousel #eForm_form_details_applicant_employment_type").trigger("EmpType")
        }, j)
    });
    $("#newstaggered-carousel #eForm_form_details_coApplicant_employment_type").on("change", function() {
        if (!($(this).val() == $(".radio.label-icon.checked.hl-emp-type-coapp").attr("for"))) {
            return
        }
        addAnimationForCTAButton();
        setTimeout(function() {
            $("#newstaggered-carousel #eForm_form_details_coApplicant_employment_type").trigger("EmpType")
        }, j)
    });
    $("#newstaggered-carousel #eForm_form_details_coApplicant_applicantRelation_radio").on("change", function() {
        if ($("#eForm_form_details_coApplicant_applicantRelation_radio:checked").val()) {
            addAnimationForCTAButton();
            setTimeout(function() {
                $("input[name='form.details.coApplicant.applicantRelation']").val($("#eForm_form_details_coApplicant_applicantRelation_radio:checked").val());
                $("#newstaggered-carousel #eForm_form_details_coApplicant_applicantRelation_radio").trigger("relationship")
            }, j)
        }
    });
    $("#newstaggered-carousel #eForm_form_details_coApplicant_applicantRelation_select").on("change", function() {
        addAnimationForCTAButton();
        $("input[name='form.details.coApplicant.applicantRelation']").val($(this).val());
        setTimeout(function() {
            $("#newstaggered-carousel #eForm_form_details_coApplicant_applicantRelation_select").trigger("relationship")
        }, j)
    });
    $("#newstaggered-carousel .citizenship-radio0").on("change", function() {
        if (!($(this).val() == $(".radio.label-icon.checked").attr("for"))) {
            return
        }
        $("input[name='form.details.applicant.citizenshipStatus']").val($(".citizenship-radio0:checked").val());
        $("#newstaggered-carousel .citizenship-radio0").trigger("citizenshipStatus")
    });
    $("#newstaggered-carousel .citizenship-radio1").on("change", function() {
        if ($(this).attr("checked") != "checked") {
            return
        }
        $("input[name='form.details.coApplicant.citizenshipStatus']").val($(".citizenship-radio1:checked").val());
        $("#newstaggered-carousel .citizenship-radio1").trigger("citizenshipStatusForCoApplicant")
    });
    $("#newstaggered-carousel #eForm_form_details_applicant_citizenshipStatus_select").on("change", function() {
        $("input[name='form.details.applicant.citizenshipStatus']").val($(this).val());
        $("#newstaggered-carousel .citizenship-radio0").trigger("citizenshipStatus")
    });
    $("#newstaggered-carousel #eForm_form_details_coApplicant_citizenshipStatus_select").on("change", function() {
        $("input[name='form.details.coApplicant.citizenshipStatus']").val($(this).val());
        $("#newstaggered-carousel .citizenship-radio1").trigger("citizenshipStatusForCoApplicant")
    });
    $("#newstaggered-carousel #eForm_form_details_property_propertyConstructionType").on("change", function() {
        if ($("#eForm_form_details_property_propertyConstructionType:checked").val()) {
            addAnimationForCTAButton();
            setTimeout(function() {
                $("#newstaggered-carousel #eForm_form_details_property_propertyConstructionType").trigger("propertyConstruction")
            }, j)
        }
    });
    $("#newstaggered-carousel .coApplicant-radio").on("change", function() {
        if ($(".coApplicant-radio:checked").val()) {
            addAnimationForCTAButton();
            $("a[name='moveCarouselButton']").each(function() {
                if (!$(this).hasClass("skip-slide") && !$(this).hasClass("co-applicant-radio-slide")) {
                    $(this).removeClass("loading-circle").html("Continue")
                }
            });
            setTimeout(function() {
                $("#newstaggered-carousel .coApplicant-radio").trigger("coApplicantChange")
            }, j)
        }
    });
    $("#newstaggered-carousel .coApplicant-selection").on("change", function() {
        if ($(".coApplicant-selection:checked").val() == "on") {
            addAnimationForCTAButton();
            setTimeout(function() {
                $("#newstaggered-carousel .coApplicant-selection").trigger("coApplicantCheckboxChange")
            }, j)
        }
    });
    $("#newstaggered-carousel .property-purchase-radio").on("change", function() {
        if ($("#eForm_form_details_property_propertyPurchaseType:checked").val()) {
            addAnimationForCTAButton();
            setTimeout(function() {
                $("#newstaggered-carousel .property-purchase-radio").trigger("propertyPurchaseChange")
            }, j)
        }
    });
    $("#newstaggered-carousel #eForm_form_details_property_seller_radio").on("change", function() {
        if ($("#eForm_form_details_property_seller_radio:checked").val()) {
            $("input[name='form.details.property.seller']").val($("#eForm_form_details_property_seller_radio:checked").val())
        }
        slideNext()
    });
    $("#newstaggered-carousel #eForm_form_details_property_seller_select").on("change", function() {
        addAnimationForCTAButton();
        $("input[name='form.details.property.seller']").val($(this).val());
        setTimeout(function() {
            slideNext()
        }, j)
    });
    $("#newstaggered-carousel #eForm_form_applicantPlaceHolder_employmentAbroadStartDate_year_select").on("change", function() {
        $("input[name='form.applicantPlaceHolder.employmentStartDate.year']").val($(this).val())
    });
    $(".hl-joining-month-abroad").on("change", function() {
        $("input[name='form.applicantPlaceHolder.employmentStartDate.month']").val($(this).val())
    });
    $("#newstaggered-carousel #eForm_form_applicantPlaceHolder_employmentAbroadStartDate_month_select").on("change", function() {
        $("input[name='form.applicantPlaceHolder.employmentStartDate.month']").val($(this).val())
    });
    $("#newstaggered-carousel #eForm_form_coApplicantPlaceHolder_businessStartDate_month_select").on("change", function() {
        $("input[name='form.coApplicantPlaceHolder.employmentStartDate.month']").val($(this).val())
    });
    $("#newstaggered-carousel #eForm_form_applicantPlaceHolder_businessStartDate_month_select").on("change", function() {
        $("input[name='form.applicantPlaceHolder.employmentStartDate.month']").val($(this).val())
    });
    $("#newstaggered-carousel #eForm_form_applicantPlaceHolder_employmentStartDate_year_select").on("change", function() {
        $("input[name='form.applicantPlaceHolder.employmentStartDate.year']").val($(this).val())
    });
    $("#newstaggered-carousel #eForm_form_coApplicantPlaceHolder_employmentStartDate_year_select").on("change", function() {
        $("input[name='form.coApplicantPlaceHolder.employmentStartDate.year']").val($(this).val())
    });
    $("#newstaggered-carousel #eForm_form_coAapplicantPlaceHolder_employmentAbroadStartDate_year_select").on("change", function() {
        $("input[name='form.coApplicantPlaceHolder.employmentStartDate.year']").val($(this).val())
    });
    $("#newstaggered-carousel #eForm_form_coApplicantPlaceHolder_employmentAbroadStartDate_month_select").on("change", function() {
        $("input[name='form.coApplicantPlaceHolder.employmentStartDate.month']").val($(this).val())
    });
    $("#newstaggered-carousel #eForm_form_details_property_locationOfLand_radio").on("change", function() {
        $("input[name='form.details.property.locationOfLand']").val($("#eForm_form_details_property_locationOfLand_radio:checked").val());
        slideNext()
    });
    $("#newstaggered-carousel #eForm_form_details_property_locationOfLand_select").on("change", function() {
        $("input[name='form.details.property.locationOfLand']").val($(this).val());
        slideNext()
    });
    $("#newstaggered-carousel input[name='form.details.applicant.gender']").on("change", function() {
        if ($(this).attr("checked") == "checked") {
            decideGenderImage();
            slideNext()
        }
    });
    $("#newstaggered-carousel .touch-icon .radio").click(function() {
        if ($(this).hasClass("checked") && $(this).find(".ns-other-city").length != 1) {
            moveToNextSlide()
        }
    });
    $("#newstaggered-carousel input[name='form.details.coApplicant.gender']").on("change", function() {
        if ($(this).attr("checked") != "checked") {
            return
        }
        slideNext()
    });
    $("input[name='form.applicantPlaceHolder.existingCCBankId']").change(function() {
        var r = $("input[name='form.applicantPlaceHolder.existingCCBankId']:checked").val();
        if (r == "999") {}
        if (r == "996") {
            moveToNextSlide()
        }
    });
    $("#newstaggered-carousel .total-experience-slider").on("slideStop", function() {
        m()
    });
    $("#newstaggered-carousel #jd_year_picker .table-curved tbody td").click(function() {
        m()
    });
    $("#newstaggered-carousel #jd_month_picker .table-curved tbody td").click(function() {
        m()
    });
    $("#newstaggered-carousel #residence_start_year_picker .table-curved tbody td").click(function() {
        q()
    });
    $("#newstaggered-carousel #residence_start_month_picker .table-curved tbody td").click(function() {
        q()
    });
    $("#newstaggered-carousel #city_start_year_picker .table-curved tbody td").click(function() {
        q()
    });
    $("#newstaggered-carousel #city_start_month_picker .table-curved tbody td").click(function() {
        q()
    });
    $("#newstaggered-carousel input[name='form.applicantPlaceHolder.companyName']").focus(function() {
        if ($("#productTypeId").val() != "2") {
            n("SALARIED")
        }
    });
    $(".noCard").on("click", function() {
        $(".validateCC input").attr("checked", false);
        $(".validateCC input").parents("label").removeClass("checked")
    });
    $(".validateCC").on("click", function() {
        $(".noCard input").attr("checked", false);
        $(".noCard input").parents("label").removeClass("checked")
    });
    var g = function() {
        e($("#form_applicantPlaceHolder_residenceCity_fallback").val());
        slideNext()
    };
    $(".btn-toggle").on("click", function(s) {
        var r = s.target;
        if ($(r).data("value") == "SELF_EMPLOYED_BUSINESS") {
            f($("#newstaggered-carousel select[name='form.applicantPlaceHolder.companyName.recommend']"));
            $("#newstaggered-carousel input[name='form.applicantPlaceHolder.companyName']").val("")
        }
        n($(r).data("value"))
    });
    b()
};

function initKeyEventForHlDobFields() {
    $(".hl-dob-fields").find(".date-input").on("keypress", function(b) {
        var d = 48;
        var a = 57;
        if ($(this).val().length >= 2 && b.which >= d && b.which <= a) {
            $(this).val("")
        }
        moveFocusToNextField($(this), $(".hl-dob-fields").find(".month-input"), 1, b)
    });
    $(".hl-dob-fields").find(".month-input").on("keypress", function(b) {
        var d = 48;
        var a = 57;
        if ($(this).val().length >= 2 && b.which >= d && b.which <= a) {
            $(this).val("")
        }
        moveFocusToNextField($(this), $(".hl-dob-fields").find(".year-input"), 1, b)
    });
    $(".hl-dob-fields").find(".year-input").on("keydown", function(a) {
        if ((a.which == 8 || a.which == 46) && $(this).val() == "") {
            $(this).prev("input").focus()
        }
    });
    $(".hl-dob-fields").find(".month-input").on("keydown", function(a) {
        if ((a.which == 8 || a.which == 46) && $(this).val() == "") {
            $(this).prev("input").focus()
        }
    })
}

function updateValidationRules(b, a) {
    if (b == ADDVALIDATION) {
        a.addClass("validate");
        a.removeClass("dontvalidate");
        a.attr("disabled", false)
    } else {
        if (b == REMOVEVALIDATION) {
            a.addClass("dontvalidate");
            a.removeClass("validate");
            a.attr("disabled", true)
        }
    }
}

function ccsliderChangeQueue(b) {
    var a = b.parentsUntil("div[class='item']").find(".item.active").attr("slidename");
    if (a == "existingcreditcard") {
        var e = "false";
        var d = $('input[type="tel"][name="form.details.applicant.income.maximumCreditOfExistingCards"]');
        $("input[type='checkbox'][name='form.applicantPlaceHolder.existingCCBankId']:checked").each(function() {
            if ($(this).val() != "996") {
                updateValidationRules(ADDVALIDATION, d);
                cc_slide_queue = [0, 1, 2, 3, 4, 5, 6, 7, 8];
                e = "true"
            }
        });
        if (e == "false") {
            initSelectList();
            updateValidationRules(REMOVEVALIDATION, d);
            cc_slide_queue = [0, 1, 2, 3, 4, 5, 7, 8]
        }
    }
}

function clsliderChangeQueue(f) {
    var b = f.parentsUntil("div[class='item']").find(".item.active.newstaggered-modal").attr("slidename");
    if (b == "car" && $("#eForm_form_details_uiParameters_formType").val() == undefined) {
        var a = $("div .item").index().toString();
        var e = $("input[name='form.applicantPlaceHolder.usedCarManufactureDate.year']");
        var d = $('input[type="radio"][name="form.details.carType"]:checked').val();
        var g = $('input[name="form.details.carType"]').val();
        if (d == "OLD" || g == "OLD") {
            updateValidationRules(ADDVALIDATION, e);
            if (a == "12") {
                cl_slide_queue = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
            } else {
                cl_slide_queue = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
            }
            if ($("#usedCarManufactureDate_year").val() != "") {
                $("#manufacturing_year_table td a[data-val =" + $("#usedCarManufactureDate_year").val() + "]").parent().addClass("selected")
            }
        } else {
            updateValidationRules(REMOVEVALIDATION, e);
            cl_slide_queue = [0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
        }
    }
}

function moveToPreviousSlide() {
    if ($("#productNameSpace").val() == "credit-card") {
        var b = parseInt($("#newstaggered-carousel .item.active").index());
        if (typeof cc_slide_queue !== "undefined") {
            var a = jQuery.inArray(b, cc_slide_queue);
            $("#newstaggered-carousel").carousel(cc_slide_queue[a - 1])
        } else {
            $("#newstaggered-carousel").carousel(b - 1)
        }
    }
    if ($("#productNameSpace").val() == "car-loan") {
        var b = parseInt($("#newstaggered-carousel .item.active").index());
        if (typeof cl_slide_queue !== "undefined") {
            var a = jQuery.inArray(b, cl_slide_queue);
            $("#newstaggered-carousel").carousel(cl_slide_queue[a - 1])
        } else {
            $("#newstaggered-carousel").carousel(b - 1)
        }
    }
    if ($("#productNameSpace").val() == "personal-loan") {
        $("div .item #continueButton").each(function() {
            if ($(this).hasClass("loading-circle")) {
                if ($(this).hasClass("search-for-button")) {
                    $(this).removeClass("loading-circle").html("Search for Personal Loans")
                } else {
                    $(this).removeClass("loading-circle").html("Continue")
                }
            }
        })
    }
}
$("#manufacturing_year_table td").click(function() {
    $("#usedCarManufactureDate_year").val($(this).find("a").attr("data-val"));
    $(this).parent().parent().find("td").removeClass("selected");
    $(this).addClass("selected");
    slideNext()
});

function changeResidencyStatusToResidentIndian(d, a) {
    if (d != null && d == true) {
        $("input[name='form.details.applicant.citizenshipStatus']").val(BBConstants.residentIndian);
        $('input:radio[name="citizenshipRadios0"]').filter("[value=" + BBConstants.residentIndian + "]").attr("checked", true)
    } else {
        $("input[name='form.details.coApplicant.citizenshipStatus']").val(BBConstants.residentIndian);
        $('input:radio[name="citizenshipRadios1"]').filter("[value=" + BBConstants.residentIndian + "]").attr("checked", true)
    }
    var b = $("div.item[slidename=" + a + "]");
    b.find("label[for=" + (BBConstants.residentIndian) + "]").addClass("checked");
    b.find(".residency-areas").addClass("dontshow");
    hideFields($("div.item[slidename=" + a + "] .residence-country input"));
    hideFields($("div.item[slidename=" + a + "] .country-dropdown input"))
}

function initSelectList() {
    var b = "";
    var a = $("#multiSelectDropDown input:checked").size();
    if (a == 0) {
        $(".hida").show();
        $(".cc-others .touch-icon .checkbox").removeClass("checked")
    } else {
        $(".cc-others .touch-icon .checkbox").addClass("checked");
        if (a < 3) {
            $("#multiSelectDropDown input:checked").each(function() {
                if (b != "") {
                    b = b + ", "
                }
                b = b + $(this).attr("id")
            });
            $(".hida").hide()
        } else {
            b = $("#multiSelectDropDown input:checked").size() + " Selected"
        }
    }
    $(".multiSel").html(b)
}

function decideGenderImage() {
    var d = $("input[name='form.details.applicant.gender']:checked").val();
    if (d != undefined) {
        d = d.toLowerCase()
    }
    var a = "icon-gender-" + d;
    var b = "co-applicant-own-" + d;
    $(".applicant-image").removeClass("icon-gender-male icon-gender-female");
    $(".applicant-image").addClass(a);
    $(".owner-image").removeClass("co-applicant-own-male co-applicant-own-female");
    $(".owner-image").addClass(b)
}
$(function() {
    decideGenderImage();
    initSelectList();
    $(".dropdown2 dt a").on("click", function() {
        $(".dropdown2 dd ul").slideToggle("fast")
    });
    $(".dropdown2 dd ul li a").on("click", function() {
        $(".dropdown2 dd ul").hide()
    });
    $(".cc-others .touch-icon").on("click", function() {
        $(".dropdown2 dd ul").slideToggle("fast")
    });
    $(".cc-others").on("mouseleave", function() {
        $(".dropdown2 dd ul").hide()
    });

    function a(b) {
        return $("#" + b).find("dt a span.value").html()
    }
    $(document).bind("click", function(b) {});
    $('#multiSelectDropDown input[type="checkbox"]').on("click", function() {
        initSelectList()
    })
});

function toggleIncomeSlideFromMonthlyToAnnualWhenApplicantIsSelfEmployed(j, m, f, g, e) {
    function i(n) {
        n.addClass("dontshow");
        n.find(".validate").each(function() {
            $(this).removeClass("validate");
            $(this).val("");
            $(this).addClass("dontvalidate")
        })
    }

    function l(n) {
        n.removeClass("dontshow");
        n.find(".dontvalidate").each(function() {
            $(this).removeClass("dontvalidate");
            $(this).addClass("validate")
        })
    }

    function b(o, n) {
        if (!o.find(".removeName").attr("name")) {
            o.find(".removeName").attr("name", n)
        }
    }

    function a(n) {
        n.find(".removeName").removeAttr("name")
    }

    function d(n) {
        l(m);
        b(m, n);
        i(f);
        a(f)
    }

    function h(n) {
        l(f);
        b(f, n);
        i(m);
        a(m)
    }

    function k(n) {
        if (n == "SELF_EMPLOYED_BUSINESS") {
            d(e)
        } else {
            h(g)
        }
    }
    j.change(function() {
        k($(this).val())
    });
    k(j.val())
}

function initializeRecommender(d, g, f, a, e, h) {
    var b = {
        callback: h || function(j, i, l) {
            var k = $("div .item.active").attr("slidename");
            if ((j.exactMatch && j.recommendations.length == 0) && ((g == "CITY" && (k == "city" || k == "coapp_city" || k == "propertycity")) || (g == "COMPANY" && k == "employer") || (k == "car" && g == "CAR"))) {
                if ($("#productNameSpace").val() == "car-insurance") {
                    populateVariantCallback();
                    moveToSlideInQueue("next")
                } else {
                    moveToNextSlide()
                }
            }
        },
        showRecommendation: function(j, i, k) {
            k.attr("disabled", false).parents().filter("li:first").removeClass("dontshow").find(".wwlbl");
            if (k != null && typeof k != "undefined") {
                $("button[id=" + k.attr("id") + "]").focus();
                k.addClass("validate");
                k.removeClass("dontvalidate");
                k.siblings("div").removeClass("validate");
                $("button[id='" + k.attr("id") + "']").siblings("ul").find(".selected").removeClass("selected")
            }
        },
        hideRecommendation: function(i) {
            i.attr("disabled", true).parents().filter("li:first").addClass("dontshow");
            i.removeClass("validate");
            i.addClass("dontvalidate")
        }
    };
    b = $.extend(false, {}, {
        urlParams: {
            type: g,
            limit: 60
        },
        showMatchMsg: false,
        showSearchingMsg: true,
        changeMatchMsg: true
    }, b);
    b = $.extend(false, {}, {
        onRecoStart: a || function() {},
        onRecoEnd: e || function() {}
    }, b);
    b = $.extend(false, {}, {
        getDataFromDifferentField: d.dataFromField,
        dataFromField: d.dataFromField
    }, b);
    d.field.recommender("/" + f + "/recommend.html", d.reco, b, g)
}

function initCommonSlideComponents() {
    addValidationRules();
    addValidationRulesForOtherCityCar();
    setNumbering();
    formatNumber();
    reInitializeDOBFiled();

    function a() {
        trackVirtualPagePath();
        triggerSlideVariantAdWord();
        if ($("#fromOffersPage").val() == "true") {
            if (!$("#eForm").valid()) {
                $("#newstaggered-carousel .validate.error").each(function(b, d) {
                    if (b == 0) {
                        $("#newstaggered-carousel .item").removeClass("active");
                        $(d).closest(".item").addClass("active");
                        $(d).focus()
                    }
                })
            } else {
                $("#newstaggered-carousel .item:first-child").removeClass("active");
                $("#newstaggered-carousel .item:last-child").addClass("active");
                $("#right_carousel_control").hide()
            }
        } else {
            $("#left_carousel_control").hide()
        }
    }
    initTypeAhead($("#eForm input[name='form.applicantPlaceHolder.companyName']"), "companies", "/autoComplete.html?ajax=true&type=COMPANY&productTypeId=" + $("#productTypeId").val());
    if ($("#mobileSite").val() == "true") {
        initTypeAhead($("input[name='form.applicantPlaceHolder.residenceCity.fallback1']"), "cities", "/autoComplete.html?ajax=true&type=CITY&productTypeId=" + $("#productTypeId").val(), true)
    }
    initNewStaggeredForm();
    addCustomEventHandlers();
    addCustomEventHandlersForOtherCity();
    initializeRecommender({
        field: $("input[name='form.applicantPlaceHolder.companyName']"),
        reco: $("select[name='form.applicantPlaceHolder.companyName.recommend']")
    }, "COMPANY", $("#productNameSpace").val());
    initializeRecommender({
        field: $("input[name='form.coApplicantPlaceHolder.companyName']"),
        reco: $("select[name='form.coApplicantPlaceHolder.companyName.recommend']")
    }, "COMPANY", $("#productNameSpace").val());
    initializeRecommender({
        field: $("input[name='otherCoAppCityInputField']"),
        reco: $("select[name='form.coApplicantPlaceHolder.residenceCity.recommend']")
    }, "CITY", $("#productNameSpace").val());
    initializeRecommender({
        field: $("input[name='propertyCityFallBackName']"),
        reco: $("select[name='form.propertyCity.recommend']")
    }, "CITY", $("#productNameSpace").val());
    a();
    bindOtherDropdown();
    if ($("#mobileSite").val() != "true") {
        if ($("#productNameSpace").val() != "home-loan") {
            bindOtherItemClick()
        }
    }
}

function initializeMonthYearComponent(j, b) {
    var l = "<tr>";
    var d = "</tr>";
    var g = "<td>";
    var m = "</td>";

    function f(s, r) {
        var p = s.find("input:first");
        var q = r.find("input:first");
        i(s);
        h(r);
        o(s, p);
        k(r, q);
        n(s, p);
        n(r, q);
        a(p, q, r)
    }

    function o(p, q) {
        p.find("td").on("click", function() {
            $(this).parent().parent().find("td").removeClass("selected");
            $(this).addClass("selected");
            q.val($(this).find("a").data("val"));
            a(q, $("#" + q.data("month-id")), $("#" + p.data("month-picker-id")))
        })
    }

    function k(p, q) {
        p.find("td").on("click", function() {
            $(this).parent().parent().find("td").removeClass("selected");
            $(this).addClass("selected");
            q.val($(this).find("a").data("val"))
        })
    }

    function i(r) {
        var q = parseInt(getServerDate().getFullYear());
        var t = q - 5;
        var s = null;
        for (var p = q; p >= t; p -= 2) {
            s = e(p, t);
            r.find("tbody").append(s)
        }
    }

    function e(q, s) {
        var r = l;
        for (var p = 1; p <= 2; p++, q--) {
            r += g + '<a data-val="' + q + '">' + ((q == s) ? "Before " + (q + 1) : q) + "</a>" + m
        }
        r += d;
        return r
    }

    function h(s) {
        var p = BBConstants.monthNames;
        var u = l;
        var t;
        for (var r = 0; r < p.length;) {
            u = l;
            for (var q = 0; q < 3; q++, r++) {
                t = (r < 10 ? new String("0" + r) : new String(r));
                u += g + '<a data-val="' + t + '">' + p[r] + "</a>" + m
            }
            u += d;
            s.find("tbody").append(u)
        }
    }

    function a(p, r, q) {
        if (p.val() > getMinValueForYearComponent()) {
            q.removeClass("dontshow")
        } else {
            r.val("");
            q.find("td").removeClass("selected");
            if (!q.hasClass("dontshow")) {
                q.addClass("dontshow")
            }
        }
    }

    function n(p, q) {
        p.find("td").removeClass("selected");
        p.find("td a").each(function() {
            if (parseInt($(this).data("val")) == q.val() && $.trim(q.val()) != "") {
                $(this).parent().addClass("selected")
            }
        })
    }
    f(j, b)
}

function customSlideEventForTotalExperience(a, e, b, d) {
    if (a == "slideStop") {
        e.val(getFormattedNumber(b))
    }
}

function customSlideEventForMaxValue(a, e, b, d) {
    if (a == "slideStop") {
        e.val(getFormattedNumber(b))
    }
}

function customSlideEventForHlSliders(a, b) {
    return getFormattedNumber(String(a))
}

function reInitializeDOBFiled() {
    var b = $("#date-of-birth").val();
    if (b != undefined && b != null && b != "") {
        var a = b.split(" ");
        $("#eForm_form_applicantPlaceHolder_dob_day").val(a[0]);
        $("#eForm_form_applicantPlaceHolder_dob_month").val(($.inArray(a[1], BBConstants.monthNames)) + 1);
        $("#eForm_form_applicantPlaceHolder_dob_year").val(a[2])
    }
}
$(function() {
    $("[data-toggle=tooltip]").tooltip("hide");
    if ($("#productNameSpace").val() == "car-loan") {
        $(".carousel-inner").children(":first").addClass("active");

        function d() {
            var s = false;
            $("#newstaggered-carousel input[name='form.carName']").each(function(t, u) {
                if ($(this).val() == $("#newstaggered-carousel input[name='staggeredCarName']").val()) {
                    s = true
                }
            });
            if (!s && $.trim($("#newstaggered-carousel input[name='staggeredCarName']").val()) != "") {
                $("#newstaggered-carousel input[name='form.carName'][value='Other']").attr("checked", "checked");
                $("#newstaggered-carousel input[name='form.carName'][value='Other']").closest(".radio").addClass("checked");
                $("#newstaggered-carousel #carOther").val($("#newstaggered-carousel input[name='staggeredCarName']").val())
            }
        }

        function n(s, v, t, u) {
            if (s == "slideStop") {
                if (t < u) {
                    v.val(getFormattedNumber(t));
                    v.valid();
                    v.change();
                    showNextArrow()
                } else {
                    v.val("")
                }
            }
            t = (t == "" ? 5000 : Number(String(t).replace(/,/g, "")));
            initSlider($(".total-emi-slider"), {
                min: 0,
                max: Number(String(t).replace(/,/g, "")),
                value: 0,
                step: 100
            }, $("input[name='form.details.applicant.income.totalEMI']"));
            $(".total-emi-slider-bg .slider-max-value").html(getFormattedWholeNumber(Number(String(t).replace(/,/g, ""))));
            initSlider($(".average-monthly-incentives"), {
                min: 0,
                max: Number(String(t).replace(/,/g, "")) * 2,
                value: 0,
                step: 100
            }, $("input[name='form.details.applicant.income.averageMonthlyIncentives']"));
            $(".average-monthly-incentives-slider .slider-max-value").html(getFormattedWholeNumber(Number(String(t).replace(/,/g, "")) * 2))
        }

        function q(s) {
            n("", s, s.val())
        }
        q($("input[name='form.details.applicant.income.grossMonthlyIncome']"));
        initSlider($(".total-experience-slider"), {
            min: 0,
            max: 7,
            value: 0,
            step: 1
        }, $("input[name='form.applicantPlaceHolder.totalWorkExperience']"), customSliderValueFormatter, customSlideEventForTotalExperience);
        if (($("div.completeElig").length == 0) || ($("#displaySalaryCompleteElig").val() == "true")) {
            initSlider($(".monthly-income-slider"), {
                min: 0,
                max: 120000,
                value: 0,
                step: 1000
            }, $("#eForm input[name='form.details.applicant.income.grossMonthlyIncome']"), undefined, n)
        }
        var h = initSlider($(".age-slider"), {
            min: 18,
            max: 100,
            value: 18,
            step: 1
        });
        initDobSlider($("#mm-yy"), $("#dob-date-picker"), h, $("#date-of-birth"));
        initializeRecommender({
            field: $("input[name='carName_fallback_value_textbox']"),
            reco: $("select[name='form.applicantPlaceHolder.car.recommend']")
        }, "CAR", "car-loan");
        initializeMonthYearComponent($("#jd_year_picker"), $("#jd_month_picker"));
        initializeMonthYearComponent($("#residence_start_year_picker"), $("#residence_start_month_picker"));
        initializeMonthYearComponent($("#city_start_year_picker"), $("#city_start_month_picker"));
        initCommonSlideComponents();
        $("#newstaggered-carousel [slidename='car'] input[name='form.carName']").removeClass("basic-required-options validate").addClass("dontvalidate");
        addMonthYearComponentValidationRules();
        $("#join_date_header").find("span:nth-child(1)").html($("input[name='form.applicantPlaceHolder.companyName']").val());
        d()
    } else {
        if ($("#productNameSpace").val() == "home-loan") {
            if ($("#companyNameForHack").val() != "" && ($("input[name='form.applicantPlaceHolder.companyName']").val() == "")) {
                $("#companyNameFieldSal").val($("#companyNameForHack").val())
            }
            if ($("#eForm_form_applicantPlaceHolder_companyName_recommend").val() != null && ($("#companyNameFieldSal").val() == $("#eForm_form_applicantPlaceHolder_companyName_recommend").val())) {
                $("#eForm_form_applicantPlaceHolder_companyName_recommend").addClass("dontshow")
            }
            if (!($("#eForm_form_details_uiParameters_variant").val() == "slide") && $("#displayCityCompleteElig").val() == "false" && $("#eForm_form_details_uiParameters_formType").val() != null && $("#eForm_form_details_uiParameters_formType").val() == "medium") {
                $("div .gender-input").addClass("active");
                var i = $("div .item[slidenumber='0']");
                i.removeClass("fixedSlides");
                i.find("input[name*=form]").attr("disabled", true);
                i.find("select[name*=form]").attr("disabled", true)
            } else {
                $(".carousel-inner").children(":first").addClass("active")
            }
            typeAheadFunctions();
            initCommonSlideComponents();
            addPropertyCostValidationRules();
            addStandAlonePropertyCostValidationRules();
            initialiseSliders();
            initializeSlideDependencies();
            initializeSlideQueue("home-loan-fixed-slides");
            initKeyEventForHlDobFields();
            initializeMonthYearComponent($("#jd_abroad_year_picker_hl0"), $("#jd_abroad_month_picker_hl0"));
            initializeMonthYearComponent($("#jd_abroad_year_picker_hl1"), $("#jd_abroad_month_picker_hl1"));
            initializeMonthYearComponent($("#jd_year_picker0"), $("#jd_month_picker0"));
            initializeMonthYearComponent($("#jd_year_picker1"), $("#jd_month_picker1"));
            var m = 1000;
            if ($("#newstaggered-carousel .citizenship-radio0:checked").val() != "RESIDENT_INDIAN" && typeof $("#newstaggered-carousel .citizenship-radio0:checked").val() != "undefined") {
                $("#newstaggered-carousel .citizenship-radio0").trigger("citizenshipStatus")
            }
            if ($("#newstaggered-carousel .citizenship-radio1:checked").val() != "RESIDENT_INDIAN" && typeof $("#newstaggered-carousel .citizenship-radio1:checked").val() != "undefined") {
                $("#newstaggered-carousel .citizenship-radio1").trigger("citizenshipStatusForCoApplicant")
            }
            $("select[name='form.applicantPlaceHolder.residenceCountry.value']").selectpicker({
                style: "btn-info"
            });
            $("select[name='form.coApplicantPlaceHolder.residenceCountry.value']").selectpicker({
                style: "btn-info"
            });
            $("#newstaggered-carousel #jd_abroad_year_picker_hl0 .table-curved tbody td").click(function() {
                slideNextRuleForHlJoiningDate($('input[name="abroadJoiningYear0"]'), $('input[name="abroadJoiningMonth0"]'))
            });
            $("#newstaggered-carousel #jd_abroad_month_picker_hl0 .table-curved tbody td").click(function() {
                slideNextRuleForHlJoiningDate($('input[name="abroadJoiningYear0"]'), $('input[name="abroadJoiningMonth0"]'))
            });
            $("#newstaggered-carousel #jd_abroad_year_picker_hl1 .table-curved tbody td").click(function() {
                slideNextRuleForHlJoiningDate($('input[name="abroadJoiningYear1"]'), $('input[name="abroadJoiningMonth1"]'))
            });
            $("#newstaggered-carousel #jd_abroad_month_picker_hl1 .table-curved tbody td").click(function() {
                slideNextRuleForHlJoiningDate($('input[name="abroadJoiningYear1"]'), $('input[name="abroadJoiningMonth1"]'))
            });
            $("#totalExperienceDiv0").find("td").on("click", function() {
                addAnimationForCTAButton();
                var s = $(this);
                setTimeout(function() {
                    s.parent().parent().find("td").removeClass("selected");
                    s.addClass("selected");
                    $(".complete-eligibility-form select[name='form.applicantPlaceHolder.totalWorkExperience']").val(s.find("a").data("val"));
                    slideNext()
                }, m)
            });
            $("#totalExperienceDiv1").find("td").on("click", function() {
                addAnimationForCTAButton();
                var s = $(this);
                setTimeout(function() {
                    s.parent().parent().find("td").removeClass("selected");
                    s.addClass("selected");
                    $(".complete-eligibility-form select[name='form.coApplicantPlaceHolder.totalWorkExperience']").val(s.find("a").data("val"));
                    slideNext()
                }, m)
            });
            $(".complete-eligibility-form #left_carousel_control").click(function() {
                moveToSlideInQueue("prev")
            });
            $(".complete-eligibility-form #right_carousel_control").click(function() {
                addAnimationForCTAButton();
                canMoveToNextSlideForHl = false;
                setTimeout(function() {
                    var s = $("#newstaggered-carousel .item.active").index();
                    var u = [0, 1, 4, 5, 18, 27, 28, 29, 30, 31, 33];
                    var t = u.indexOf(s);
                    if (t > -1) {
                        dependencyForSlides(s)
                    } else {
                        moveToSlideInQueue("next")
                    }
                }, m)
            });
            $(".complete-eligibility-form a[name='moveCarouselButton']").click(function() {
                addAnimationForCTAButton();
                canMoveToNextSlideForHl = false;
                setTimeout(function() {
                    var s = $("#newstaggered-carousel .item.active").index();
                    var u = [0, 1, 4, 5, 18, 27, 28, 29, 30, 31, 33];
                    var t = u.indexOf(s);
                    if (t > -1) {
                        dependencyForSlides(s)
                    } else {
                        moveToSlideInQueue("next")
                    }
                }, m)
            });
            $("select[name='form.existingLoanStartDate.year']").change(function() {
                if ($(this).val() == (getCurrentYear() - 2)) {
                    $("#eForm_form_existingLoanStartDate_month").addClass("dontshow");
                    $("div .bb-search-action-dropdown.pull-right").addClass("dontshow")
                } else {
                    if ($("#eForm_form_existingLoanStartDate_month").hasClass("dontshow")) {
                        $("#eForm_form_existingLoanStartDate_month").removeClass("dontshow");
                        $("div .bb-search-action-dropdown.pull-right").removeClass("dontshow")
                    }
                }
            });
            $("input.integer").each(function() {
                $(this).keypress(function(s) {
                    if (s.which != 8 && s.which != 0 && (s.which < 48 || s.which > 57)) {
                        return false
                    }
                })
            });
            $(".minimum-property-cost").tooltip({
                placement: $(window).width() < 1023 ? "bottom" : "right",
                trigger: "focus",
                title: $("#propertyCost_helpMsg").val()
            });

            function l(s, v, t, u) {
                if (s == "slideStop") {
                    if (t < u) {
                        v.val(getFormattedNumber(t));
                        v.valid();
                        v.change();
                        showNextArrow()
                    } else {
                        v.val("")
                    }
                }
                t = (t == "" ? 100000 : Number(String(t).replace(/,/g, "")));
                initSlider($(".hl-total-emi-coapplicant-true"), {
                    min: 0,
                    max: getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) / 6, 5),
                    value: 0,
                    step: 100
                }, $("input[id='eForm_form_details_coAapplicant_income_totalEMI_true']"), customSlideEventForHlSliders, customSlideEventForMaxValue);
                $(".hl-total-emi-slider-true .maximum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + getFormattedWholeNumber(getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) / 6, 5)));
                initSlider($(".hl-total-emi-coapplicant-false"), {
                    min: 0,
                    max: getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) / 6, 5),
                    value: 0,
                    step: 100
                }, $("input[id='eForm_form_details_coAapplicant_income_totalEMI_false']"), customSlideEventForHlSliders, customSlideEventForMaxValue);
                $(".hl-total-emi-slider-false .maximum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + getFormattedWholeNumber(getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) / 6, 5)));
                initSlider($(".hl-monthly-incentives-coapplicant"), {
                    min: 0,
                    max: getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 2, 5),
                    value: 0,
                    step: 1000
                }, $("input[name='form.details.coApplicant.income.averageMonthlyIncentives']"), customSlideEventForHlSliders, customSlideEventForMaxValue);
                $(".hl-monthly-incentives-slider .maximum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + getFormattedWholeNumber(getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 2, 5)));
                initSlider($(".hl-annual-bonus-coapplicant"), {
                    min: 0,
                    max: getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 4, 5),
                    value: 0,
                    step: 1000
                }, $("input[name='form.details.coApplicant.income.recentAnnualPerformanceBonus']"), customSlideEventForHlSliders, customSlideEventForMaxValue);
                $(".hl-annual-bonus-slider .maximum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + getFormattedWholeNumber(getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 4, 5)))
            }

            function p(s) {
                l("", s, s.val())
            }
            $("input[name='form.details.coApplicant.income.latestProfitAfterTax']").on("change blur", function() {
                p($("input[name='form.details.coApplicant.income.latestProfitAfterTax']"))
            });
            p($("input[name='form.details.coApplicant.income.latestProfitAfterTax']"));

            function b(s, v, t, u) {
                if (s == "slideStop") {
                    if (t < u) {
                        v.val(getFormattedNumber(t));
                        v.valid();
                        v.change();
                        showNextArrow()
                    } else {
                        v.val("")
                    }
                }
                t = (t == "" ? 100000 : Number(String(t).replace(/,/g, "")));
                initSlider($(".hl-total-emi-true"), {
                    min: 0,
                    max: getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) / 6, 5),
                    value: 0,
                    step: 100
                }, $("input[id='eForm_form_details_applicant_income_totalEMI_true']"), customSlideEventForHlSliders, customSlideEventForMaxValue);
                $(".hl-total-emi-slider-true .maximum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + getFormattedWholeNumber(getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) / 6, 5)));
                initSlider($(".hl-total-emi-false"), {
                    min: 0,
                    max: getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) / 6, 5),
                    value: 0,
                    step: 100
                }, $("input[id='eForm_form_details_applicant_income_totalEMI_false']"), customSlideEventForHlSliders, customSlideEventForMaxValue);
                $(".hl-total-emi-slider-false .maximum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + getFormattedWholeNumber(getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) / 6, 5)));
                initSlider($(".hl-monthly-incentives"), {
                    min: 0,
                    max: getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 2, 5),
                    value: 0,
                    step: 1000
                }, $("input[name='form.details.applicant.income.averageMonthlyIncentives']"), customSlideEventForHlSliders, customSlideEventForMaxValue);
                $(".hl-monthly-incentives-slider .maximum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + getFormattedWholeNumber(getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 2, 5)));
                initSlider($(".hl-annual-bonus"), {
                    min: 0,
                    max: getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 4, 5),
                    value: 0,
                    step: 1000
                }, $("input[name='form.details.applicant.income.recentAnnualPerformanceBonus']"), customSlideEventForHlSliders, customSlideEventForMaxValue);
                $(".hl-annual-bonus-slider .maximum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + getFormattedWholeNumber(getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 4, 5)));
                initSlider($(".cost-of-flat"), {
                    min: 400000,
                    max: getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 200, 5),
                    value: 0,
                    step: 10000
                }, $("#eForm input[name='form.details.property.costOfProperty']"), customSlideEventForHlSliders, customSlideEventForMaxValue);
                $(".hl-home-cost-slider .maximum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + getFormattedWholeNumber(getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 200, 6)));
                $(".hl-home-cost-slider .minimum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + 400000);
                initSlider($(".cost-of-construction"), {
                    min: 400000,
                    max: getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 200, 5),
                    value: 0,
                    step: 10000
                }, $("input[name='form.details.property.costOfConstruction']"), customSlideEventForHlSliders, customSlideEventForMaxValue);
                $(".hl-construction-cost-slider .maximum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + getFormattedWholeNumber(getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 200, 6)));
                $(".hl-construction-cost-slider .minimum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + 400000);
                initSlider($(".cost-of-land"), {
                    min: 400000,
                    max: getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 200, 5),
                    value: 0,
                    step: 10000
                }, $("input[name='form.details.property.costOfLand']"), customSlideEventForHlSliders, customSlideEventForMaxValue);
                $(".hl-land-cost-slider .maximum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + getFormattedWholeNumber(getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 200, 6)));
                $(".hl-land-cost-slider .minimum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + 400000)
            }

            function k(s) {
                b("", s, s.val())
            }
            $("input[name='form.details.applicant.income.latestProfitAfterTax']").on("change blur", function() {
                k($("input[name='form.details.applicant.income.latestProfitAfterTax']"))
            });
            k($("input[name='form.details.applicant.income.latestProfitAfterTax']"));

            function o(s, v, t, u) {
                if (s == "slideStop") {
                    if (t < u) {
                        v.val(getFormattedNumber(t));
                        v.valid();
                        v.change();
                        showNextArrow()
                    } else {
                        v.val("")
                    }
                }
                t = (t == "" ? 100000 : Number(String(t).replace(/,/g, "")));
                initSlider($(".hl-total-emi-coapplicant-true"), {
                    min: 0,
                    max: getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 2, 5),
                    value: 0,
                    step: 100
                }, $("input[id='eForm_form_details_coAapplicant_income_totalEMI_true']"), customSlideEventForHlSliders, customSlideEventForMaxValue);
                $(".hl-total-emi-slider-true .maximum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + getFormattedWholeNumber(getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 2, 5)));
                initSlider($(".hl-total-emi-coapplicant-false"), {
                    min: 0,
                    max: getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 2, 5),
                    value: 0,
                    step: 100
                }, $("input[id='eForm_form_details_coAapplicant_income_totalEMI_false']"), customSlideEventForHlSliders, customSlideEventForMaxValue);
                $(".hl-total-emi-slider-false .maximum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + getFormattedWholeNumber(getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 2, 5)));
                initSlider($(".hl-monthly-incentives-coapplicant"), {
                    min: 0,
                    max: getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 2, 5),
                    value: 0,
                    step: 1000
                }, $("input[name='form.details.coApplicant.income.averageMonthlyIncentives']"), customSlideEventForHlSliders, customSlideEventForMaxValue);
                $(".hl-monthly-incentives-slider .maximum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + getFormattedWholeNumber(getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 2, 5)));
                initSlider($(".hl-annual-bonus-coapplicant"), {
                    min: 0,
                    max: getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 4, 5),
                    value: 0,
                    step: 1000
                }, $("input[name='form.details.coApplicant.income.recentAnnualPerformanceBonus']"), customSlideEventForHlSliders, customSlideEventForMaxValue);
                $(".hl-annual-bonus-slider .maximum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + getFormattedWholeNumber(getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 4, 5)))
            }

            function f(s) {
                o("", s, s.val())
            }
            $("input[name='form.details.coApplicant.income.grossMonthlyIncome']").on("change blur", function() {
                f($("input[name='form.details.coApplicant.income.grossMonthlyIncome']"))
            });
            f($("input[name='form.details.coApplicant.income.grossMonthlyIncome']"));
            $("input[name='form.details.coApplicant.income.monthlyTakeHomeSalary']").on("change blur", function() {
                f($("input[name='form.details.coApplicant.income.monthlyTakeHomeSalary']"))
            });
            if ($('input[name="form.details.coApplicant.citizenshipStatus"]').val() != "RESIDENT_INDIAN") {
                f($("input[name='form.details.coApplicant.income.monthlyTakeHomeSalary']"))
            }

            function g(s, v, t, u) {
                if (s == "slideStop") {
                    if (t < u) {
                        v.val(getFormattedNumber(t));
                        v.valid();
                        v.change();
                        showNextArrow()
                    } else {
                        v.val("")
                    }
                }
                t = (t == "" ? 100000 : Number(String(t).replace(/,/g, "")));
                initSlider($(".hl-total-emi-true"), {
                    min: 0,
                    max: getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 2, 5),
                    value: 0,
                    step: 100
                }, $("input[id='eForm_form_details_applicant_income_totalEMI_true']"), customSlideEventForHlSliders, customSlideEventForMaxValue);
                $(".hl-total-emi-slider-true .maximum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + getFormattedWholeNumber(getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 2, 5)));
                initSlider($(".hl-total-emi-false"), {
                    min: 0,
                    max: getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 2, 5),
                    value: 0,
                    step: 100
                }, $("input[id='eForm_form_details_applicant_income_totalEMI_false']"), customSlideEventForHlSliders, customSlideEventForMaxValue);
                $(".hl-total-emi-slider-false .maximum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + getFormattedWholeNumber(getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 2, 5)));
                initSlider($(".hl-monthly-incentives"), {
                    min: 0,
                    max: getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 2, 5),
                    value: 0,
                    step: 1000
                }, $("input[name='form.details.applicant.income.averageMonthlyIncentives']"), customSlideEventForHlSliders, customSlideEventForMaxValue);
                $(".hl-monthly-incentives-slider .maximum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + getFormattedWholeNumber(getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 2, 5)));
                initSlider($(".hl-annual-bonus"), {
                    min: 0,
                    max: getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 4, 5),
                    value: 0,
                    step: 1000
                }, $("input[name='form.details.applicant.income.recentAnnualPerformanceBonus']"), customSlideEventForHlSliders, customSlideEventForMaxValue);
                $(".hl-annual-bonus-slider .maximum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + getFormattedWholeNumber(getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 4, 5)));
                initSlider($(".cost-of-flat"), {
                    min: 400000,
                    max: getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 200, 5),
                    value: 0,
                    step: 10000
                }, $("#eForm input[name='form.details.property.costOfProperty']"), customSlideEventForHlSliders, customSlideEventForMaxValue);
                $(".hl-home-cost-slider .maximum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + getFormattedWholeNumber(getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 200, 6)));
                $(".hl-home-cost-slider .minimum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + 400000);
                initSlider($(".cost-of-construction"), {
                    min: 400000,
                    max: getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 200, 5),
                    value: 0,
                    step: 10000
                }, $("input[name='form.details.property.costOfConstruction']"), customSlideEventForHlSliders, customSlideEventForMaxValue);
                $(".hl-construction-cost-slider .maximum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + getFormattedWholeNumber(getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 200, 6)));
                $(".hl-construction-cost-slider .minimum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + 400000);
                initSlider($(".cost-of-land"), {
                    min: 400000,
                    max: getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 200, 5),
                    value: 0,
                    step: 10000
                }, $("input[name='form.details.property.costOfLand']"), customSlideEventForHlSliders, customSlideEventForMaxValue);
                $(".hl-land-cost-slider .maximum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + getFormattedWholeNumber(getNearestSignificantNumber(Number(String(t).replace(/,/g, "")) * 200, 6)));
                $(".hl-land-cost-slider .minimum-value-slider").html('<img src="/images/complete-eligibility-form/rupee_icon_small_black.png"/>' + 400000)
            }

            function e(s) {
                g("", s, s.val())
            }
            $("#eForm input[name='form.details.applicant.income.grossMonthlyIncome']").on("change blur", function() {
                e($("#eForm input[name='form.details.applicant.income.grossMonthlyIncome']"))
            });
            e($("#eForm input[name='form.details.applicant.income.grossMonthlyIncome']"));
            $("input[name='form.details.applicant.income.monthlyTakeHomeSalary']").on("change blur", function() {
                e($("input[name='form.details.applicant.income.monthlyTakeHomeSalary']"))
            });
            if ($('input[name="form.details.applicant.citizenshipStatus"]').val() != "RESIDENT_INDIAN") {
                e($("input[name='form.details.applicant.income.monthlyTakeHomeSalary']"))
            }
            $("#join_date_header").find("span:nth-child(1)").html($("input[name='form.applicantPlaceHolder.companyName']").val())
        } else {
            if ($("#productNameSpace").val() == "personal-loan") {
                $(".carousel-inner").children(":first").addClass("active");

                function r(s, v, t, u) {
                    if (s == "slideStop") {
                        if (t < u) {
                            v.val(getFormattedNumber(t));
                            v.valid();
                            v.change();
                            showNextArrow()
                        } else {
                            v.val("")
                        }
                    }
                    initSlider($(".total-emi-slider"), {
                        min: 0,
                        max: t == "" ? 5000 : Number(String(t).replace(/,/g, "")),
                        value: 0,
                        step: 100
                    }, $("input[name='form.details.applicant.income.totalEMI']"));
                    $(".total-emi-slider-bg .slider-max-value").html(getFormattedWholeNumber(Number(String(t).replace(/,/g, ""))))
                }

                function j(s) {
                    r("", s, s.val())
                }
                j($("input[name='form.details.applicant.income.monthlyTakeHomeSalary']"));
                initializeMonthYearComponent($("#jd_year_picker"), $("#jd_month_picker"));
                initializeMonthYearComponent($("#residence_start_year_picker"), $("#residence_start_month_picker"));
                initializeMonthYearComponent($("#city_start_year_picker"), $("#city_start_month_picker"));
                initSlider($(".total-experience-slider"), {
                    min: 0,
                    max: 7,
                    value: 0,
                    step: 1
                }, $("input[name='form.applicantPlaceHolder.totalWorkExperience']"), customSliderValueFormatter, customSlideEventForTotalExperience);
                if (($("div.completeElig").length == 0) || ($("#displaySalaryCompleteElig").val() == "true")) {
                    initSlider($(".monthly-income-slider"), {
                        min: 0,
                        max: 120000,
                        value: 0,
                        step: 1000
                    }, $("#eForm input[name='form.details.applicant.income.monthlyTakeHomeSalary']"), undefined, r)
                }
                var h = initSlider($(".age-slider"), {
                    min: 18,
                    max: 100,
                    value: 18,
                    step: 1,
					grid:true,
                });
                initDobSlider($("#mm-yy"), $("#dob-date-picker"), h, $("#date-of-birth"));
                var a = initSlider($(".intended-loan-amount"), {
                    min: 50000,
                    max: 3000000,
                    value: 200000,
                    step: 10000
                }, $("#eForm input[name='form.details.intendedLoanAmount']"), undefined, customSlideEventForMaxValue);
                initCommonSlideComponents();
                addMonthYearComponentValidationRules();
                initializeSlideQueue();
                $("#join_date_header").find("span:nth-child(1)").html($("input[name='form.applicantPlaceHolder.companyName']").val())
            } else {
                if ($("#productNameSpace").val() == "credit-card") {
                    initSlider($(".monthly-income-slider"), {
                        min: 0,
                        max: 120000,
                        value: 0,
                        step: 1000
                    }, $("input[name='form.details.applicant.income.monthlyTakeHomeSalary']"));
                    initSlider($(".maximum-credit-slider"), {
                        min: 0,
                        max: 500000,
                        value: 0,
                        step: 10000
                    }, $("input[name='form.details.applicant.income.maximumCreditOfExistingCards']"), undefined, customSlideEventForMaxValue);
                    initSlider($(".annual-income-slider"), {
                        min: 0,
                        max: 3000000,
                        value: 0,
                        step: 25000
                    }, $("input[name='form.details.applicant.income.latestProfitAfterTax']"));
                    var h = initSlider($(".age-slider"), {
                        min: 18,
                        max: 100,
                        value: 18,
                        step: 1,
						grid:true
                    });
                    initDobSlider($("#mm-yy"), $("#dob-date-picker"), h, $("#date-of-birth"));
                    initCommonSlideComponents();
                    toggleIncomeSlideFromMonthlyToAnnualWhenApplicantIsSelfEmployed($('#newstaggered-carousel input[name="form.details.applicant.employment.type"]'), $(".annual-salary"), $(".monthly-salary"), "form.details.applicant.income.monthlyTakeHomeSalary", "form.details.applicant.income.latestProfitAfterTax");
                    toggleIncomeSlideFromMonthlyToAnnualWhenApplicantIsSelfEmployed($('#newstaggered-carousel input[name="form.details.applicant.employment.type"]'), $(".saving-account"), $(".salary-account"), "form.details.applicant.existingBankAccounts[0].bank.id", "form.applicantPlaceHolder.existingSavingAccountId")
                } else {
                    if ($("#productNameSpace").val() == "debit-card") {
                        initSlider($(".monthly-income-slider"), {
                            min: 0,
                            max: 120000,
                            value: 0,
                            step: 1000
                        }, $("input[name='form.details.applicant.income.monthlyTakeHomeSalary']"));
                        initSlider($(".annual-income-slider"), {
                            min: 0,
                            max: 3000000,
                            value: 0,
                            step: 25000
                        }, $("input[name='form.details.applicant.income.latestProfitAfterTax']"));
                        var h = initSlider($(".age-slider"), {
                            min: 18,
                            max: 100,
                            value: 18,
                            step: 1,
							grid:true,
                        });
                        initDobSlider($("#mm-yy"), $("#dob-date-picker"), h, $("#date-of-birth"));
                        initCommonSlideComponents();
                        toggleIncomeSlideFromMonthlyToAnnualWhenApplicantIsSelfEmployed($('#newstaggered-carousel input[name="form.details.applicant.employment.type"]'), $(".annual-salary"), $(".monthly-salary"), "form.details.applicant.income.monthlyTakeHomeSalary", "form.details.applicant.income.latestProfitAfterTax");
                        toggleIncomeSlideFromMonthlyToAnnualWhenApplicantIsSelfEmployed($('#newstaggered-carousel input[name="form.details.applicant.employment.type"]'), $(".saving-account"), $(".salary-account"), "form.details.applicant.existingBankAccounts[0].bank.id", "form.applicantPlaceHolder.existingSavingAccountId")
                    }
                }
            }
        }
    }
});
$(".grey-out").each(function() {
    $(this).click(function() {
        if (!($("#productNameSpace").val() == "credit-card" && $(this).attr("href") != "#get-quote")) {
            highlightSlide()
        }
        if ($(this).attr("id") == "stickyButton") {
            BB_trackGoogleEventWithLabel(ctaCategory(), ctaClickedAction(), ctaLabel())
        }
    })
});
$(".close-btn").click(function() {
    unHighlightSlide()
});
$("#grey-btn").find("a:first").click(function() {
    highlightSlide()
});
$(window).on("hashchange", function() {
    if (location.hash != "#get-quote") {
        unHighlightSlide()
    }
});
$(function() {
    bindLinksForLeads($("a.lclnk"), $(".nocheckout"))
});

function initialiseSliders() {
    var b = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    function a(g, e, h, f) {
        return b[g - 1]
    }

    function d(g, e, h, f) {
        if (g <= f) {
            return "<" + String(Number(f) + 1)
        }
        return String(g)
    }
    initSlider($(".total-experience-slider0"), {
        min: 0,
        max: 7,
        value: 0,
        step: 1
    }, $("input[name='form.applicantPlaceHolder.totalWorkExperience']"), customSliderValueFormatter, customSlideEventForTotalExperience);
    initSlider($(".total-experience-slider1"), {
        min: 0,
        max: 7,
        value: 0,
        step: 1
    }, $("input[name='form.coApplicantPlaceHolder.totalWorkExperience']"), customSliderValueFormatter, customSlideEventForTotalExperience);
    initSlider($(".hl-joining-year-abroad-coapplicant"), {
        min: (getCurrentYear() - 5),
        max: getCurrentYear(),
        step: 1
    }, $("input[id='eForm_form_coAapplicantPlaceHolder_employmentAbroadStartDate_year_select']"), d, customSlideEventForYearSlider);
    initSlider($(".hl-joining-month-abroad-coapplicant"), {
        min: 1,
        max: 12,
        value: 1,
        step: 1
    }, $("input[id='eForm_form_coApplicantPlaceHolder_employmentAbroadStartDate_month_select']"), a, customSlideEventForMaxValue);
    initSlider($(".hl-joining-year-abroad"), {
        min: (getCurrentYear() - 5),
        max: getCurrentYear(),
        step: 1
    }, $("input[id='eForm_form_applicantPlaceHolder_employmentAbroadStartDate_year_select']"), d, customSlideEventForYearSlider);
    initSlider($(".hl-joining-month-abroad"), {
        min: 1,
        max: 12,
        value: 1,
        step: 1
    }, $("input[id='eForm_form_applicantPlaceHolder_employmentAbroadStartDate_month_select']"), a, customSlideEventForMaxValue);
    initSlider($(".latest-profit-after-tax-coapplicant"), {
        min: 0,
        max: 3000000,
        value: 0,
        step: 1000
    }, $("input[name='form.details.coApplicant.income.latestProfitAfterTax']"));
    initSlider($(".latest-profit-after-tax"), {
        min: 0,
        max: 3000000,
        value: 0,
        step: 1000
    }, $("input[name='form.details.applicant.income.latestProfitAfterTax']"));
    initSlider($(".hl-work-experience"), {
        min: 0,
        max: 7,
        value: 0,
        step: 1
    }, $("select[name='form.applicantPlaceHolder.totalWorkExperience']"), customSliderValueFormatter, customSlideEventForTotalExperience);
    initSlider($(".hl-work-experience-coapplicant"), {
        min: 0,
        max: 7,
        value: 0,
        step: 1
    }, $("select[name='form.coApplicantPlaceHolder.totalWorkExperience']"), customSliderValueFormatter, customSlideEventForTotalExperience);
    initSlider($(".hl-monthly-income-slider-coapplicant"), {
        min: 0,
        max: 120000,
        value: 0,
        step: 1000
    }, $("input[name='form.details.coApplicant.income.grossMonthlyIncome']"));
    initSlider($(".hl-net-monthly-income-slider-coapplicant"), {
        min: 0,
        max: 120000,
        value: 0,
        step: 1000
    }, $("input[name='form.details.coApplicant.income.monthlyTakeHomeSalary']"));
    initSlider($(".hl-monthly-income-slider"), {
        min: 0,
        max: 120000,
        value: 0,
        step: 1000
    }, $("#eForm input[name='form.details.applicant.income.grossMonthlyIncome']"));
    initSlider($(".hl-net-monthly-income-slider"), {
        min: 0,
        max: 120000,
        value: 0,
        step: 1000
    }, $("input[name='form.details.applicant.income.monthlyTakeHomeSalary']"))
}

function typeAheadFunctions() {
    initTypeAhead($("input[name='form.builderProject']"), "builders", "/autoComplete.html?ajax=true&type=BUILDER_PROJECT");
    initTypeAhead($("input[name='form.existingLoanBankName']"), "banks", "/autoComplete.html?ajax=true&type=BANK&vendorInstitutionType=BANK", 10);
    initTypeAhead($("input[name='form.coApplicantPlaceHolder.companyName']"), "companies", "/autoComplete.html?ajax=true&type=COMPANY&productTypeId=" + $("#productTypeId").val());
    initTypeAhead($(".residency-status-slide input[name='form.applicantPlaceHolder.residenceCountry.fallback']"), "countries", "/autoComplete.html?ajax=true&type=COUNTRY");
    initTypeAhead($(".residency-status-slide input[name='form.coApplicantPlaceHolder.residenceCountry.fallback']"), "countries", "/autoComplete.html?ajax=true&type=COUNTRY");
    initializeRecommender({
        field: $("input[name='form.applicantPlaceHolder.residenceCountry.fallback']"),
        reco: $("select[name='form.applicantPlaceHolder.residenceCountry.recommend']")
    }, "COUNTRY", $("#productNameSpace").val())
}

function dependencyForSlides(a) {
    switch (a) {
        case 0:
            dependentSlideForCitySlide(".city-slide input[name='form.applicantPlaceHolder.residenceCity.value']:checked", ".city-slide input[name='form.applicantPlaceHolder.residenceCity.fallback']", 0);
            return;
        case 1:
            if ($("input[name='citizenshipRadios0']:checked").val() == BBConstants.residentIndian || $("#eForm_form_details_applicant_citizenshipStatus_select").val() == BBConstants.residentIndian) {
                disableCountryDropdownsAndMoveToCitySlide(residenceCitySlideNo, ".applicant-city-slide-heading")
            } else {
                moveToSlideInQueue("next")
            }
            return;
        case 4:
            dependentSlidesOnPropertyPurchase();
            return;
        case 5:
            dependentSlidesOnEmpType();
            return;
        case 18:
            dependentSlidesOnPropertyConstruction();
            return;
        case 27:
            dependentSlidesForCoApplicantPresent();
            return;
        case 28:
            dependentSlidesForCoAppRelation();
            return;
        case 29:
            dependentSlidesOnCoAppOwner();
            return;
        case 30:
            dependentSlideForCitySlide(".city-slide input[name='form.coApplicantPlaceHolder.residenceCity.value']:checked", ".city-slide input[name='form.coApplicantPlaceHolder.residenceCity.fallback']", 1);
            return;
        case 31:
            if ($("input[name='citizenshipRadios1']:checked").val() == BBConstants.residentIndian || $("#eForm_form_details_coApplicant_citizenshipStatus_select").val() == BBConstants.residentIndian) {
                disableCountryDropdownsAndMoveToCitySlide(coApplicantResidenceCitySlideNo, ".coapplicant-city-slide-heading")
            } else {
                moveToSlideInQueue("next")
            }
            return;
        case 33:
            dependentSlidesOnCoAppEmpType();
            return;
        default:
            return
    }
}

function slideNextRuleForHlJoiningDate(a, d) {
    a.trigger("change");
    d.trigger("change");
    var b = true;
    if ($.trim(a.val()) == "" || (Number(a.val()) > Number(getMinValueForYearComponent()) && $.trim(d.val()) == "")) {
        b = false
    } else {
        if ((Number(a.val()) == Number(getCurrentYear())) && (Number(d.val()) > Number(getCurrentMonth()))) {
            b = false
        }
    }
    if (b) {
        slideNext()
    }
}
$(function() {
    if (!$("div.item[slidename='citizenship']").find(".residence-country").hasClass("dontshow")) {
        $("div.item[slidename='citizenship'] .flatui-dropdown").find(".disabled").removeClass("disabled")
    }
    if (!$("div.item[slidename='coapp-citizenship']").find(".residence-country").hasClass("dontshow")) {
        $("div.item[slidename='coapp-citizenship'] .flatui-dropdown").find(".disabled").removeClass("disabled")
    }
    if ($("#eForm_form_applicantPlaceHolder_employmentAbroadStartDate_month_select").val() != "") {
        $("#eForm_form_applicantPlaceHolder_employmentAbroadStartDate_month").val($("#eForm_form_applicantPlaceHolder_employmentAbroadStartDate_month_select").val())
    }
    if ($("#eForm_form_applicantPlaceHolder_employmentAbroadStartDate_year_select").val() != "") {
        $("#eForm_form_applicantPlaceHolder_employmentAbroadStartDate_year").val($("#eForm_form_applicantPlaceHolder_employmentAbroadStartDate_year_select").val())
    }
});