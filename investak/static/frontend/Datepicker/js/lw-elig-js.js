(function(a) {
    a.fn.addOption = function() {
        var h = function(p, k, m, q) {
            var n = document.createElement("option");
            n.value = k, n.text = m;
            var r = p.options;
            var j = r.length;
            if (!p.cache) {
                p.cache = {};
                for (var l = 0; l < j; l++) {
                    p.cache[r[l].value] = l
                }
            }
            if (typeof p.cache[k] == "undefined") {
                p.cache[k] = j
            }
            p.options[p.cache[k]] = n;
            if (q) {
                n.selected = true
            }
        };
        var c = arguments;
        if (c.length == 0) {
            return this
        }
        var g = true;
        var b = false;
        var e, d, f;
        if (typeof(c[0]) == "object") {
            b = true;
            e = c[0]
        }
        if (c.length >= 2) {
            if (typeof(c[1]) == "boolean") {
                g = c[1]
            } else {
                if (typeof(c[2]) == "boolean") {
                    g = c[2]
                }
            }
            if (!b) {
                d = c[0];
                f = c[1]
            }
        }
        this.each(function() {
            if (this.nodeName.toLowerCase() != "select") {
                return
            }
            if (b) {
                for (var i in e) {
                    h(this, i, e[i], g)
                }
            } else {
                h(this, d, f, g)
            }
        });
        return this
    };
    a.fn.ajaxAddOption = function(d, f, b, e, c) {
        if (typeof(d) != "string") {
            return this
        }
        if (typeof(f) != "object") {
            f = {}
        }
        if (typeof(b) != "boolean") {
            b = true
        }
        this.each(function() {
            var g = this;
            a.getJSON(d, f, function(h) {
                a(g).addOption(h, b);
                if (typeof e == "function") {
                    if (typeof c == "object") {
                        e.apply(g, c)
                    } else {
                        e.call(g)
                    }
                }
            })
        });
        return this
    };
    a.fn.removeOption = function() {
        var c = arguments;
        if (c.length == 0) {
            return this
        }
        var e = typeof(c[0]);
        var d, f;
        if (e == "string" || e == "object" || e == "function") {
            d = c[0];
            if (d.constructor == Array) {
                var b = d.length;
                for (var g = 0; g < b; g++) {
                    this.removeOption(d[g], c[1])
                }
                return this
            }
        } else {
            if (e == "number") {
                f = c[0]
            } else {
                return this
            }
        }
        this.each(function() {
            if (this.nodeName.toLowerCase() != "select") {
                return
            }
            if (this.cache) {
                this.cache = null
            }
            var h = false;
            var l = this.options;
            if (!!d) {
                var j = l.length;
                for (var k = j - 1; k >= 0; k--) {
                    if (d.constructor == RegExp) {
                        if (l[k].value.match(d)) {
                            h = true
                        }
                    } else {
                        if (l[k].value == d) {
                            h = true
                        }
                    }
                    if (h && c[1] === true) {
                        h = l[k].selected
                    }
                    if (h) {
                        l[k] = null
                    }
                    h = false
                }
            } else {
                if (c[1] === true) {
                    h = l[f].selected
                } else {
                    h = true
                }
                if (h) {
                    this.remove(f)
                }
            }
        });
        return this
    };
    a.fn.sortOptions = function(c) {
        var d = a(this).selectedValues();
        var b = typeof(c) == "undefined" ? true : !!c;
        this.each(function() {
            if (this.nodeName.toLowerCase() != "select") {
                return
            }
            var g = this.options;
            var e = g.length;
            var h = [];
            for (var f = 0; f < e; f++) {
                h[f] = {
                    v: g[f].value,
                    t: g[f].text
                }
            }
            h.sort(function(j, i) {
                o1t = j.t.toLowerCase(), o2t = i.t.toLowerCase();
                if (o1t == o2t) {
                    return 0
                }
                if (b) {
                    return o1t < o2t ? -1 : 1
                } else {
                    return o1t > o2t ? -1 : 1
                }
            });
            for (var f = 0; f < e; f++) {
                g[f].text = h[f].t;
                g[f].value = h[f].v
            }
        }).selectOptions(d, true);
        return this
    };
    a.fn.selectOptions = function(e, b) {
        var d = e;
        var g = typeof(e);
        if (g == "object" && d.constructor == Array) {
            var f = this;
            a.each(d, function() {
                f.selectOptions(this, b)
            })
        }
        var h = b || false;
        if (g != "string" && g != "function" && g != "object") {
            return this
        }
        this.each(function() {
            if (this.nodeName.toLowerCase() != "select") {
                return this
            }
            var k = this.options;
            var c = k.length;
            for (var j = 0; j < c; j++) {
                if (d.constructor == RegExp) {
                    if (k[j].value.match(d)) {
                        k[j].selected = true
                    } else {
                        if (h) {
                            k[j].selected = false
                        }
                    }
                } else {
                    if (k[j].value == d) {
                        k[j].selected = true
                    } else {
                        if (h) {
                            k[j].selected = false
                        }
                    }
                }
            }
        });
        return this
    };
    a.fn.copyOptions = function(d, c) {
        var b = c || "selected";
        if (a(d).size() == 0) {
            return this
        }
        this.each(function() {
            if (this.nodeName.toLowerCase() != "select") {
                return this
            }
            var g = this.options;
            var e = g.length;
            for (var f = 0; f < e; f++) {
                if (b == "all" || (b == "selected" && g[f].selected)) {
                    a(d).addOption(g[f].value, g[f].text)
                }
            }
        });
        return this
    };
    a.fn.containsOption = function(e, c) {
        var d = false;
        var b = e;
        var f = typeof(b);
        var g = typeof(c);
        if (f != "string" && f != "function" && f != "object") {
            return g == "function" ? this : d
        }
        this.each(function() {
            if (this.nodeName.toLowerCase() != "select") {
                return this
            }
            if (d && g != "function") {
                return false
            }
            var k = this.options;
            var h = k.length;
            for (var j = 0; j < h; j++) {
                if (b.constructor == RegExp) {
                    if (k[j].value.match(b)) {
                        d = true;
                        if (g == "function") {
                            c.call(k[j], j)
                        }
                    }
                } else {
                    if (k[j].value == b) {
                        d = true;
                        if (g == "function") {
                            c.call(k[j], j)
                        }
                    }
                }
            }
        });
        return g == "function" ? this : d
    };
    a.fn.selectedValues = function() {
        var b = [];
        this.selectedOptions().each(function() {
            b[b.length] = this.value
        });
        return b
    };
    a.fn.selectedTexts = function() {
        var b = [];
        this.selectedOptions().each(function() {
            b[b.length] = this.text
        });
        return b
    };
    a.fn.selectedOptions = function() {
        return this.find("option:selected")
    }
})(jQuery);

function initTypeAhead(e, g, d, b, f, c) {
    b = b || 30;
    if ($("#mobileSite") && $("#mobileSite").val() == "true") {
        b = 6
    }
    var a = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace("name"),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        limit: b,
        remote: {
            url: d + "&query=%QUERY",
            filter: function(h) {
                return $.map(h, function(i) {
                    return {
                        name: i
                    }
                })
            },
            ajax: {
                dataType: "jsonp",
                jsonp: "jsonp_callback",
                jsonpCallback: "bbCallback",
                data: {
                    limit: b
                }
            }
        }
    });
    a.initialize();
    if (f) {
        e.typeahead(null, {
            name: g,
            displayKey: "name",
            source: a.ttAdapter()
        }).off("blur")
    } else {
        e.typeahead(null, {
            name: g,
            displayKey: "name",
            source: a.ttAdapter()
        })
    }
    e.closest(".twitter-typeahead").on("click", " .empty-message", function() {
        $(this).hide();
        e.closest(".carousel").carousel("next")
    });
    e.closest(".twitter-typeahead").on("click", function() {
        if (g == "cities" && c != undefined) {
            c.hide();
            c.removeClass("validate");
            c.addClass("dontvalidate");
            $(".item.active .recommendedSearchImg").remove()
        }
    })
}

function initSlider(e, h, g, c, d) {
    function b() {
        if (g.prop("tagName") == "SELECT") {
            e.slider("setValue", Number(g.find("option:selected").val().replace(/,/g, "")), false);
            e.change()
        } else {
            if (g.val() != undefined) {
                e.slider("setValue", Number(g.val().replace(/,/g, "")), false)
            }
            e.change()
        }
    }

    function a(i, j) {
        if (i >= Number(j)) {
            return ">" + getFormattedNumber(String(i))
        }
        return getFormattedNumber(String(i))
    }

    function f(i, j) {
        if (i < j) {
            g.val(getFormattedNumber(i));
            g.valid();
            g.change();
            showNextArrow()
        } else {
            g.val("")
        }
    }
    h = h || {};
    e.slider({
        min: h.min || Number(0),
        max: h.max || 100,
        value: h.value || 0,
        orientation: h.orientation || "horizontal",
        step: h.step || 10,
        formater: function(i) {
            if (c || undefined) {
                return c(i, h.max, g, h.min)
            } else {
                return a(i, this.options.max)
            }
        }
    });
    if (g || undefined) {
        b();
        e.on("slideStop", function(j, i) {
            if (d || undefined) {
                d("slideStop", g, j.value, h.max);
                b()
            } else {
                f(j.value, h.max)
            }
        });
        g.on("focusout", function(j, i) {
            if (d || undefined) {
                d("focusout", g, $(this).val(), h.max)
            }
            b();
            showNextArrow()
        })
    }
    return e
}

function customSliderValueFormatter(c, a, d, b) {
    if (c >= a) {
        if (d || undefined) {
            d.focus()
        }
        return ">" + getFormattedNumber(String(Number(c) - 1))
    }
    if (c <= b) {
        return "<" + getFormattedNumber(String(Number(b) + 1))
    }
    return getFormattedNumber(String(c))
}

function customSliderValueFormatter1(c, a, d, b) {
    if (c >= 75) {
        if (d || undefined) {
            d.focus()
        }
        return "Above" + getFormattedNumber(String(Number(75)))
    }
    return getFormattedNumber(String(c))
}

function customSlideEventForYearSlider(a, d, b, c) {
    d.val(b);
    d.valid();
    d.change();
    showNextArrow()
}

function initDobSlider(e, s, q, p) {
    var f = $("input[name$='.dob.day'][type='hidden']");
    var r = $("input[name$='.dob.month'][type='hidden']");
    var m = $("input[name$='.dob.year'][type='hidden']");

    function l(v, t, u) {
        p.removeClass("dontshow");
        p.val(u + " " + BBConstants.monthNames[t - 1] + " " + v)
    }

    function h(t) {
        s.datepicker({
            onSelect: function(u, v) {
                f.val($(this).datepicker("getDate").getDate());
                l(m.val(), r.val(), f.val());
                showNextArrow()
            }
        });
        s.datepicker("setDate", t);
        s.siblings(".dob-sub-heading").show();
        s.show()
    }

    function n(w) {
        var u = $.inArray(w.split(" ")[0], BBConstants.monthNames);
        var v = w.split(" ")[1];
        var t = getServerDate();
        t.setDate(1);
        t.setMonth(u);
        t.setYear(v);
        if (f.val()) {
            t.setDate(f.val())
        }
        m.val(v);
        r.val(u + 1);
        return t
    }

    function j(t, u) {
        e.find("table tbody tr td").each(function() {
            if ($(this).text() == (BBConstants.monthNames[t] + " " + u)) {
                $(this).addClass("selected")
            }
        })
    }

    function o() {
        e.on("click", "table tbody tr td", function() {
            if ($(this).text() != "") {
                var t = n($(this).text());
                e.find("table tbody tr td.selected").removeClass("selected");
                j(t.getMonth(), t.getFullYear());
                h(t);
                f.val("");
                s.find(".ui-state-active").removeClass("ui-state-active");
                l(m.val(), r.val(), f.val())
            }
        })
    }

    function g(z, w, y) {
        var u = 1;
        do {
            var v = 1;
            var t = "<tr>";
            do {
                t += "<td><a>" + BBConstants.monthNames[z] + " " + w + "</a></td>";
                if (z == 11) {
                    z = 0;
                    w = w + 1
                } else {
                    z++
                }
                v++
            } while (v <= 3);
            t += "</tr>";
            e.find("table tbody tr:last").after(t);
            u++
        } while (u <= 4);
        var x = "<tr><td><a>" + BBConstants.monthNames[z] + " " + w + "</a></td><td></td><td></td></tr>";
        e.find("table tbody tr:last").after(x)
    }

    function b() {
        f.val("");
        r.val("");
        m.val("");
        s.find("table tbody td a").removeClass("ui-state-active");
        s.hide();
        s.siblings(".dob-sub-heading").hide();
        p.val("")
    }

    function i(v) {
        b();
        var t = getServerDate().getMonth();
        var u = t;
        e.find("table tbody").html("<tr></tr>");
        g(u, v, t);
        e.removeClass("dontshow")
    }

    function d() {
        q.on("slideStop", function(u, t) {
            i(getServerDate().getFullYear() - u.value - 1)
        })
    }

    function a(A, v, z) {
        v = v - 1;
        var x;
        try {
            x = new Date(A, v, z)
        } catch (y) {
            console.log("Invalid date")
        }
        var w = (x && x.getDate() == z && x.getMonth() == v && x.getFullYear() == A);
        if (w) {
            var u = getServerDate();
            var t = u.getFullYear() - parseInt(A);
            if (v > (u.getMonth()) || (v == (u.getMonth()) && u.getDate() < parseInt(z))) {
                t--
            }
            q.slider("setValue", t);
            i(Number(u.getFullYear() - t - 1));
            j(v, A);
            h(new Date(A, v, z));
            l(A, v + 1, z)
        }
    }

    function k() {
        $(".news-tagger-age-picker-mobile").find(".input-date").on("blur", function() {
            preAppendZeroForDateField($(this));
            f.val($(this).val());
            r.val($(".input-month").val());
            m.val($(".input-year").val());
            l(m.val(), r.val(), f.val())
        });
        $(".news-tagger-age-picker-mobile").find(".input-month").on("blur", function() {
            preAppendZeroForDateField($(this));
            f.val($(".input-date").val());
            r.val($(this).val());
            m.val($(".input-year").val());
            l(m.val(), r.val(), f.val())
        });
        $(".news-tagger-age-picker-mobile").find(".input-year").on("blur", function() {
            f.val($(".input-date").val());
            r.val($(".input-month").val());
            m.val($(this).val());
            l(m.val(), r.val(), f.val())
        })
    }

    function c() {
        $(".news-tagger-age-picker-mobile").find(".input-date").on("keypress", function(t) {
            moveFocusToNextField($(this), $(".news-tagger-age-picker-mobile").find(".input-month"), 1, t)
        });
        $(".news-tagger-age-picker-mobile").find(".input-month").on("keypress", function(t) {
            moveFocusToNextField($(this), $(".news-tagger-age-picker-mobile").find(".input-year"), 1, t)
        })
    }
    c();
    d();
    o();
    a(m.val(), r.val(), f.val());
    k()
}
$(function() {
    if ($("#pageName").val() == "indexPage") {
        $("#flashSale").addClass("dontshow");
        $("#landingPageFlashSale").removeClass("dontshow")
    } else {
        $("#flashSale").removeClass("dontshow");
        $("#landingPageFlashSale").addClass("dontshow")
    }
    $("span[class$=js-flashsale-cls-btn]").click(function() {
        setTimeout(function() {
            $("#landingPageFlashSale").fadeOut()
        }, 300);
        setTimeout(function() {
            $("#flashSale").fadeIn()
        }, 200)
    });
    if ($("#landingPageFlashSale").length > 0) {
        $("#flashSale").addClass("dontshow");
        $("#landingPageFlashSale").removeClass("dontshow")
    }

    function c() {
        $("#newsletter_signup_form").ajaxForm({
            url: "/newsletter_signup.html",
            type: "post",
            dataType: "jsonp",
            jsonp: "jsonp_callback",
            beforeSubmit: function() {
                if ($("#captcha-holder").is(":visible")) {
                    return true
                }
                $("#captcha-holder").removeClass("hide");
                $(".footer-subscribe").addClass("subscribeextend");
                return false
            },
            complete: function(d) {
                $(".footer-subscribe").removeClass("subscribeextend");
                $("#captcha-holder").addClass("hide")
            },
            success: function(d) {
                $(".form-result").text(d.stringResult)
            }
        });
        $("#newsletter_signup_form").validate()
    }
    initTypeAhead($("#company-dropdown-input"), "companies", "/autoComplete.html?ajax=true&type=COMPANY&productTypeId=" + $("#productTypeId").val(), 5);
    initTypeAhead($("#city-dropdown-input"), "cities", "/autoComplete.html?ajax=true&type=CITY&productTypeId=" + $("#productTypeId").val());
    initTypeAhead($("#car-dropdown-input"), "car", "/autoComplete.html?ajax=true&type=CAR&productTypeId=" + $("#productTypeId").val());
    enableDropdownSelectFields();
    bindingForSelectingAutoComplete();
    initializeRecommender({
        field: $("#company-dropdown-input"),
        reco: $("select[name='form.applicantPlaceHolder.companyName.recommend']"),
        getDataFromDifferentField: true,
        dataFromField: $("input[name='form.applicantPlaceHolder.companyName']")
    }, "COMPANY", "credit-card");
    populateCity();
    setCityFieldBindings();
    setDOBFieldBindings();
    setCompanyFieldBindings();
    populateCar();
    setCarFieldBindings();
    setIntendedLoanFieldBindings();
    minIntendedLoanAmountCheck();
    minSalaryCheck();
    setSalaryFieldBindings();
    setDecidedFieldBindings();
    formatSalary();
    clearDropDownField();
    if ($("#productTypeCategory").val() == "INSURANCE") {
        a();
        setUpTrackingForSntcForm()
    }

    function a() {
        if ($("select.trackDropDownChange").length == 0) {
            return
        }
        b();
        $("select.trackDropDownChange").each(function() {
            if (($(this).prev("input.dropDownChangeTracker")).val() == "false") {
                $(this).next("div").find(".filter-option").css({
                    opacity: "0.3"
                })
            }
        });
        $("select.trackDropDownChange").change(function() {
            var d = $(this).next("div");
            d.find(".filter-option").css({
                opacity: "1"
            });
            d.removeClass("error-border-line");
            $(this).prev("input.dropDownChangeTracker").val("true");
            if ($(".error-border-line").length == 0) {
                $(".el-sentence-error label").hide()
            }
        })
    }

    function b() {
        $("select.trackDropDownChange").each(function(d, e) {
            var f = $(this).attr("name");
            if ($("input[name='" + f + "']:hidden").val() != "") {
                $(this).prev("input.dropDownChangeTracker").val("true")
            }
        })
    }
    $("#eForm").submit(function() {
        if (!$("#eForm").valid()) {
            window.scrollTo(0, 0);
            return false
        }
        if ($("#productNameSpace").val() != "car-insurance" && $("#productNameSpace").val() != "life-insurance" && $("#productNameSpace").val() != "health-insurance") {
            $("#waitModal").modal({
                backdrop: "static"
            });
            $("#waitModal").modal("show");
            trackVirtualPagePath(true, "loadingoffers")
        }
    });
    $("#eForm_0").click(function() {
        $("#landingPageFlashSale").css("display", "none");
        if ($("#productTypeCategory").val() == "INSURANCE") {
            BB_trackGoogleEventWithLabel(analyticsConstants.SENTENCE_FORM, analyticsConstants.FORM_SUBMIT_ATTEMPT, $("#productNameSpace").val())
        }
    });
    $("#eFormSentence").submit(function() {
        if (!$("#eFormSentence").valid()) {
            if ($("#productTypeCategory").val() == "INSURANCE") {
                BB_trackGoogleEventWithLabel(analyticsConstants.SENTENCE_FORM, analyticsConstants.FORM_SUBMIT_ERROR, $("#productNameSpace").val())
            }
            window.scrollTo(0, 0);
            return false
        } else {
            if ($("#productTypeCategory").val() == "INSURANCE") {
                BB_trackGoogleEventWithLabel(analyticsConstants.SENTENCE_FORM, analyticsConstants.FORM_SUBMIT, $("#productNameSpace").val());
                $(".el-sentence-form-ins").removeClass("check-eligibility-btn").addClass("loading-circle").html("Please wait..");
                $.ajax({
                    type: "POST",
                    url: "/save-eligibility.html",
                    async: false,
                    data: $("#eFormSentence").serialize()
                })
            }
        }
        if ($("#productNameSpace").val() != "car-insurance") {
            $("#waitModal").modal({
                backdrop: "static"
            });
            $("#waitModal").modal("show");
            trackVirtualPagePath(true, "loadingoffers")
        }
    });
    c();
    if ($("#pageName").val() != "searchPage") {
        initializeRating()
    }
    initializePaginationForReviewLinks();
    initializeVoting();
    lazyLoadCustomerProfilePic();
    SocialLogin.init()
});
$(".js-newsletter-subscribe-form").validate({
    rules: {
        EMAIL: {
            required: true,
            email: true
        }
    },
    messages: {
        EMAIL: constructErrorMessage("Uh-oh! Please enter a valid email")
    },
    unhighlight: function(b, a) {
        $("#newsletter-subscribe-err").css("display", "none")
    },
    errorPlacement: function(a, b) {
        $("#newsletter-subscribe-err").show()
    }
});
$(".js-sticky-subscribe-form").validate({
    rules: {
        EMAIL: {
            required: true,
            email: true
        }
    },
    messages: {
        EMAIL: constructErrorMessage("Please enter a valid email")
    },
    unhighlight: function(b, a) {
        $("#mce-responses").css("display", "none")
    },
    errorPlacement: function(a, b) {
        $("#mce-responses").after(a)
    }
});
$(document).ready(function() {
    $('[name="mc-newsletter-embedded-subscribe"]').click(function(b) {
        var c = $(".js-newsletter-subscribe-form").valid();
        if (c) {
            $(this).prop("disabled", true);
            mailID = $(".js-newsletter-email").val();
            sourceValue = $("#mc_source_value").val();
            sourceURL = window.location.href;
            subscribeToNewsLetterViaMC(mailID, sourceValue, sourceURL);
            alertMCSuccessMessage();
            b.preventDefault()
        } else {
            return false
        }
    });
    $('[name="mc-sticky-embedded-subscribe"]').click(function(b) {
        var c = $(".js-sticky-subscribe-form").valid();
        if (c) {
            $(this).prop("disabled", true);
            mailID = $(".js-sticky-email").val();
            sourceValue = $("#mc_source_value").val();
            sourceURL = window.location.href;
            subscribeToNewsLetterViaMC(mailID, sourceValue, sourceURL);
            alertMCSuccessMessage();
            b.preventDefault();
            $(".get-mail-top").addClass("dontshow")
        } else {
            return false
        }
    });

    function a() {
        $("#mc-embedded-subscribe-non-partner").click(function() {
            $(this).prop("disabled", true);
            mailID = $("#nonPartnerPageEmail").val();
            sourceValue = $("#mc_source_value").val();
            sourceURL = window.location.href;
            subscribeToNewsLetterViaMC(mailID, sourceValue, sourceURL);
            alertMCSuccessMessage();
            trackEnterEmailEvent(mailID, "offerNonPartner");
            event.preventDefault()
        })
    }
    a()
});
$(".banner a[href^='#']").on("click", function(b) {
    b.preventDefault();
    var a = this.hash;
    $("html, body").animate({
        scrollTop: $(a).offset().top
    }, 0, function() {
        window.location.hash = a
    })
});
$(document).ready(function() {
    $(".subscribe-btn").click(function() {
        $(".footer-subscribe").addClass("subscribeextend")
    })
});
$(document).bind("shown.bs.modal", function() {
    var a = $(".showHowBbWorks").find("iframe");
    if (a == null || a.size() == 0) {
        var b = '<iframe rel="0&amp;" autohide="1&amp;" showinfo=0 autoplay=1 allowfullscreen="" src="https://www.youtube.com/embed/J9rI4luiZPc?rel=0" frameborder="0"></iframe>';
        $(".showHowBbWorks").find(".modal-body").append(b)
    }
});
$(document).bind("hidden.bs.modal", function() {
    var a = $(".showHowBbWorks iframe").attr("src");
    $(".showHowBbWorks iframe").attr("src", "");
    $(".showHowBbWorks iframe").attr("src", a)
});
$(document).ready(function() {
    if ($.browser.chrome) {
        $("head").append('<style>/* Web Fonts*/ @font-face {    font-family: "webfont"; src: url("webfont.eot");    font-weight: normal;    font-style: normal; }</style>')
    }
});
$(".dropdown-menu").find(".keep-open").click(function(a) {
    a.stopPropagation()
});

function enableDropdownSelectFields() {
    $(".selectpicker").selectpicker({
        style: "btn-info"
    })
}

function populateCity() {
    var a = $("#eFormSentence_form_applicantPlaceHolder_residenceCity_value").val();
    if (a == "OTHER") {
        $("#city-other-input").removeClass("dontshow");
        $(".cityFieldDropdown").css("display", "none").addClass("dontshow")
    }
}

function setCityFieldBindings() {
    var a = function(c) {
        $("#eFormSentence_form_applicantPlaceHolder_residenceCity_value").val(c)
    };
    var b = function(c) {
        if (c == "") {
            c = "Bangalore"
        }
        $("#eFormSentence_form_applicantPlaceHolder_residenceCity_fallback").val(c)
    };
    $("#city-dropdown-input").keyup(function() {
        var c = $("#city-dropdown-input").val();
        b(c)
    });
    $("#city-dropdown-input").bind("typeahead:selected", function(e, c, d) {
        var f = c.name;
        b(f)
    });
    $("#eFormSentence_form_applicantPlaceHolder_residenceCity_value").change(function() {
        if ($("#eFormSentence_form_applicantPlaceHolder_residenceCity_value").val() == "OTHER") {
            $("#city-other-input").removeClass("dontshow");
            $(".cityFieldDropdown").addClass("dontshow");
            setTimeout(function() {
                $("#city-other-input").addClass("open");
                $("#city-dropdown-input").focus()
            }, 300)
        }
    });
    $("#city-other-input").on("show.bs.dropdown", function() {
        setTimeout(function() {
            $("#city-dropdown-input").focus()
        }, 500)
    })
}

function setCompanyFieldBindings() {
    var a = function(b) {
        if (b == "") {
            b = "IBM CORP (IBM)"
        }
        $("#eFormSentence_form_applicantPlaceHolder_companyName").val(b)
    };
    $("#company-dropdown-input").keyup(function() {
        var b = $("#company-dropdown-input").val();
        a(b)
    });
    $("#company-dropdown-input").bind("typeahead:selected", function(e, c, d) {
        var b = c.name;
        a(b)
    });
    $("#eFormSentence_form_applicantPlaceHolder_companyName_recommend").change(function() {
        var b = $("#eFormSentence_form_applicantPlaceHolder_companyName_recommend").val();
        a(b);
        $("#did-you-mean-company").removeClass("orangedrop")
    });
    $("#company-dropdown-input").live("showRecommendationDropdown", function() {
        if (!$("#eFormSentence_form_applicantPlaceHolder_companyName").parent().hasClass("open")) {
            $("#eFormSentence_form_applicantPlaceHolder_companyName").dropdown("toggle")
        }
    });
    $("#company-dropdown").on("show.bs.dropdown", function() {
        setTimeout(function() {
            $("#company-dropdown-input").focus()
        }, 500)
    })
}

function populateCar() {
    if ($("#carName").val() == "OTHER") {
        $("#car-other-input").removeClass("dontshow");
        $(".carFieldDropdown").addClass("dontshow");
        var a = $("#staggeredCarName").val();
        $("#eFormSentence_form_carName").val(a)
    }
}

function setCarFieldBindings() {
    var a = function(b) {
        if (b == "") {
            b = "MARUTI SUZUKI SWIFT LXI"
        }
        $("#eFormSentence_form_carName").val(b);
        $("#staggeredCarName").val(b)
    };
    $("#car-dropdown-input").keyup(function() {
        var b = $("#car-dropdown-input").val();
        a(b)
    });
    $("#car-dropdown-input").bind("typeahead:selected", function(e, c, d) {
        var b = c.name;
        a(b)
    });
    $("#carName").change(function() {
        if ($("#carName").val() == "OTHER") {
            $("#car-other-input").removeClass("dontshow");
            $(".carFieldDropdown").addClass("dontshow");
            setTimeout(function() {
                $("#car-other-input").addClass("open");
                $("#car-dropdown-input").focus()
            }, 300)
        } else {
            var b = $("#carName").val();
            a(b)
        }
    });
    $("#car-other-input").on("show.bs.dropdown", function() {
        setTimeout(function() {
            $("#car-dropdown-input").focus()
        }, 500)
    })
}

function setDOBFieldBindings() {
    $("#dob-dropdown").on("show.bs.dropdown", function() {
        setTimeout(function() {
            $("#dob-date-input").focus()
        }, 500)
    });
    $(".el-dob-sec").keyup(setDOBFieldBindingsHelper)
}

function isValidDOB(b, d, c, a) {
    if (b != "" && d != "" && c != "" && (validateCurrentDate($("#dob-date-input").val(), $("#dob-date-input"))) && (validDate($("#dob-date-input").val(), $("#dob-date-input")))) {
        return true
    }
    return false
}

function validateCurrentDate(g, d) {
    var c = $(d).parents("li.wwgrp").find("input");
    if (c.length == 0) {
        c = $(d).parents("div.dob").find("input")
    }
    var b = new Date();
    var e = $(c[0]).val();
    var h = $(c[1]).val();
    var f = $(c[2]).val();
    var a = new Date(f, h - 1, e);
    return b >= a
}

function validDate(e, b) {
    var a = $(b).parents("li.wwgrp").find("input");
    if (a.length == 0) {
        a = $(b).parents("div.dob").find("input")
    }
    var c = $(a[0]).val();
    var g = $(a[1]).val();
    var d = $(a[2]).val();
    if (c == "" || g == "" || d == "") {
        return
    }
    var f = new Date(d, g - 1, c);
    if ($(a[0]).is(":visible")) {
        return !((f.getMonth() + 1 != g) || (f.getDate() != c) || (f.getFullYear() != d) || (d < 1900))
    }
    return false
}

function setInputValue(a, b) {
    $(a).val(b).change()
}

function handleInputFieldError(a, b) {
    $(b).show();
    $(a).error()
}

function setDOBFieldBindingsHelper() {
    var c = $("#dob-date-input").val();
    var g = $("#dob-month-input").val();
    var d = $("#dob-year-input").val();
    var a = new Date(d, g - 1, c);
    var b = getAge(a);
    var e = $(".el-dob-validation-message");
    var f = $(".el-dob-validation-message label");
    if (isValidDOB(c, g, d, b)) {
        f.html("");
        if (b < 18) {
            f.html("You are too young to apply!!");
            handleInputFieldError($("#dob-input"), $(".el-dob-validation-message"));
            return
        } else {
            if (b > 70) {
                f.html("You are too old to apply!!");
                handleInputFieldError($("#dob-input"), $(".el-dob-validation-message"));
                return
            } else {
                e.hide()
            }
        }
        setInputValue($("#dob-input"), b)
    } else {
        $("#dob-input").val("");
        if (c != "" && g != "" && (d != "" && d.length == 4)) {
            f.html("Please enter a valid date!!");
            handleInputFieldError($("#dob-input"), $(".el-dob-validation-message"))
        }
    }
}

function setSalaryFieldBindings() {
    $("#monthly-salary-dropdown-input").keyup(setSalaryFieldBindingsHelper);
    $("#salary-dropdown").on("show.bs.dropdown", function() {
        setTimeout(function() {
            $("#monthly-salary-dropdown-input").focus()
        }, 500)
    });
    var b = $("#annual-salary-dropdown-input");
    if (b.length > 0) {
        var a = b.val().replace(/,/g, "");
        if (a != "") {
            setAnnualSalaryDisplay(a)
        }
        $("#annual-earning-btn").click(function() {
            var c = $("#annual-salary-dropdown-input").val().replace(/,/g, "");
            if (c = "" || c < 200000) {
                setAnnualSalaryError($(".el-salary-validation-message"));
                handleInputFieldError($(".life-el-annual"), $(".el-salary-validation-message"))
            }
        });
        b.keyup(setAnnualSalaryFieldBindingsHelper);
        b.keydown(function() {
            $(this).keyup()
        });
        $("#annual-salary-dropdown").on("show.bs.dropdown", function() {
            setTimeout(function() {
                b.focus()
            }, 500)
        })
    }
}

function setAnnualSalaryDisplay(a) {
    var b;
    a = parseFloat(new Number(a / 100000).toFixed(2));
    b = a + " L";
    setInputValue($(".life-el-annual.salary-view-mob"), b);
    $(".life-el-annual.salary-view-mob").width((b.length) * 10);
    b = a + " Lakhs";
    setInputValue($(".life-el-annual.salary-view"), b);
    $(".life-el-annual.salary-view").width((b.length) * 22)
}

function setAnnualSalaryError(a) {
    $(".life-el-annual").val("");
    $(".life-el-annual.salary-view").width(130);
    $(".el-salary-validation-message label").html("Hi, Enter your annual Salary!!");
    a.show()
}

function setAnnualSalaryFieldBindingsHelper() {
    var a = $("#annual-salary-dropdown-input").val().replace(/,/g, "");
    var b = $(".el-salary-validation-message");
    if (a != "" && a >= 200000) {
        setAnnualSalaryDisplay(a);
        b.hide()
    } else {
        setAnnualSalaryError(b)
    }
}

function setSalaryFieldBindingsHelper() {
    if (typeof this != "undefined" && typeof this.value != "undefined") {
        var a = getFormattedNumber(this.value.replace(/\D/g, ""));
        this.value = (a == 0 && $(this).is(".number")) ? "" : a
    }
    var b = $("#monthly-salary-dropdown-input").val();
    if (b == "") {
        b = "50,000"
    }
    if ($("#productNameSpace").val() == "personal-loan") {
        $("#eFormSentence_form_details_applicant_income_monthlyTakeHomeSalary").val(b)
    } else {
        $("#eFormSentence_form_details_applicant_income_grossMonthlyIncome").val(b)
    }
    $("#monthly-salary-dropdown-input").placeholder(b)
}

function setIntendedLoanFieldBindings() {
    $("#intended-loan-amount-dropdown-input").keyup(function() {
        var a = getFormattedNumber(this.value.replace(/\D/g, ""));
        this.value = (a == 0 && $(this).is(".number")) ? "" : a;
        var b = $("#intended-loan-amount-dropdown-input").val();
        if (b == "") {
            b = $("#intended_loan_placeHolder_value").val()
        }
        $("#eFormSentence_form_details_intended_loan_amount").val(b)
    });
    $("#intended-loan-amount-dropdown").on("show.bs.dropdown", function() {
        setTimeout(function() {
            $("#intended-loan-amount-dropdown-input").focus()
        }, 500)
    })
}

function minIntendedLoanAmountCheck() {
    $("#intended-loan-amount-dropdown-input").blur(minIntendedLoanAmountCheckHelper)
}

function minIntendedLoanAmountCheckHelper() {
    var b = $("#eFormSentence_form_details_intended_loan_amount").val();
    var a;
    if (typeof b != "undefined") {
        b = b.replace(/,/g, "");
        if ($("#productNameSpace").val() == "home-loan") {
            if (b < 1000000) {
                $("#eFormSentence_form_details_intended_loan_amount").val("10,00,000");
                $("#intended-loan-amount-dropdown-input").val("10,00,000")
            }
        } else {
            if ($("#productNameSpace").val() == "personal-loan") {
                if (b < 10000) {
                    $("#eFormSentence_form_details_intended_loan_amount").val("10,000");
                    $("#intended-loan-amount-dropdown-input").val("10,000")
                }
            }
        }
        a = $("#eFormSentence_form_details_intended_loan_amount").val();
        if (typeof a != "undefined") {
            if ($("#isMobileSite").val() == "true") {
                $("#eFormSentence_form_details_intended_loan_amount").width((a.length) * 18)
            } else {
                $("#eFormSentence_form_details_intended_loan_amount").width((a.length) * 23)
            }
        }
    }
}

function minSalaryCheck() {
    $("#monthly-salary-dropdown-input").blur(function() {
        var a = $("#monthly-salary-dropdown-input").val();
        if (typeof a != "undefined") {
            a = a.replace(/,/g, "");
            if (a < 5000 && a != "") {
                a = "5,000";
                if ($("#productNameSpace").val() == "personal-loan") {
                    $("#eFormSentence_form_details_applicant_income_monthlyTakeHomeSalary").val(a)
                } else {
                    $("#eFormSentence_form_details_applicant_income_grossMonthlyIncome").val(a)
                }
                $("#monthly-salary-dropdown-input").placeholder(a)
            }
        }
    })
}

function setDecidedFieldBindings() {
    var a = function() {
        if ($("#carUndecided-from-display").val() == "true") {
            $("#car-collection-part").hide();
            $("#not-decided-part").show()
        } else {
            $("#car-collection-part").show();
            $("#not-decided-part").hide()
        }
    };
    a();
    $("#carUndecided-from-display").change(function() {
        a()
    })
}

function formatSalary() {
    var a;
    if ($("#productNameSpace").val() == "personal-loan") {
        a = $("#eFormSentence_form_details_applicant_income_monthlyTakeHomeSalary").val();
        if (a != undefined && (!a.NaN) && (a != "") && (a != "N/A")) {
            $("#eFormSentence_form_details_applicant_income_monthlyTakeHomeSalary").val(getFormattedNumber(a))
        }
    } else {
        a = $("#eFormSentence_form_details_applicant_income_grossMonthlyIncome").val();
        if (a != undefined && (!a.NaN) && (a != "") && (a != "N/A")) {
            $("#eFormSentence_form_details_applicant_income_grossMonthlyIncome").val(getFormattedNumber(a))
        }
    }
}

function clearDropDownField() {
    if ($(".dropdown .dropdown-menu .input-group").find("input[type=text]").val() != undefined || $(".dropdown .dropdown-menu .input-group").find("input[type=text]").val() != "") {
        $(".dropdown .dropdown-menu .input-group").find("input[type=text]").val("")
    }
}

function isVariantOptionPresent(a) {
    var b = $("#variantOptions").val();
    if (typeof b != "undefined" && b.indexOf(a) > -1) {
        return true
    }
    return false
}

function isMobile(d) {
    var b = "" + d;
    var c = /^[9|8|7]\d{9}/;
    var a = c.exec(b);
    console.log(a);
    if (a == null) {
        return false
    } else {
        return true
    }
}

function bindingForSelectingAutoComplete() {
    $(".tt-dropdown-menu").click(function(a) {
        a.stopPropagation()
    })
}
$("document").ready(function() {
    setTimeout(function() {
        $(".product-landing-btn-block a").addClass("animation-target")
    }, 3000);
    if ($("#eFormSentence_form_applicantPlaceHolder_residenceCity_fallback").val() != "") {
        $("#city-dropdown-input").blur()
    }
});
$(window).load(function() {
    minIntendedLoanAmountCheckHelper();
    var a = setTimeout(function() {
        $("div.cityFieldDropdown").addClass("open").hide().fadeIn("slow").css("display", "")
    }, 3000);
    $("div.cityFieldDropdown").click(function() {
        clearTimeout(a)
    });
    if ($("#searchAlreadyDone").val() == "true") {
        clearTimeout(a)
    }
    var b = $("#productNameSpace").val();
    var c = $("#mode").val();
    if (b == "credit-card") {
        preLoadImages(false)
    } else {
        if (b == "car-insurance" || b == "life-insurance" || b == "health-insurance") {
            preLoadImages(true)
        } else {
            if (b == "personal-loan" || b == "home-loan" || b == "car-loan") {
                if ($("#eForm_form_details_uiParameters_mode").val() == "slide" || $("#eForm_form_details_uiParameters_catStyleNeeded").val() == "true" || $("#eForm_form_details_uiParameters_mode").val() == "seo" || $("#variantName").val() == "true") {
                    preLoadImages(false)
                } else {
                    preLoadImages(true)
                }
            } else {
                if (!b && c == "insurance") {
                    preLoadImages(true, c)
                }
            }
        }
    }
});
$(function() {
    $("#scrollup").hide();
    $(window).scroll(function() {
        if ($(document).height() > 1600) {
            if (($(this).scrollTop() > 1600) && $(".screen-block-grey").hasClass("dontshow")) {
                $("#scrollup").fadeIn()
            } else {
                $("#scrollup").fadeOut()
            }
        }
    });
    $("#scrollup a").click(function() {
        $("body,html").animate({
            scrollTop: 0
        }, 800);
        return false
    })
});

function hookBackButtonForModal(b, a) {
    b.on("show.bs.modal", function(f) {
        var d = window.location + "#" + a;
        var c = {
            search: a
        };
        history.pushState(c, "", d)
    });
    $(window).on("popstate", function() {
        $(".modal").each(function() {
            if ($(this).hasClass("in")) {
                var c = "#" + ($(this).attr("id"));
                $(c).modal("hide")
            }
        })
    })
}

function constructErrorMessage(b) {
    var c = "<table><tr><td><span class='error-icon'><img src='images/icon-error.png'/></span></td><td><span class='error-info'>";
    var a = "</span></td></tr></table>";
    return c + b + a
}

function initializeRecommender(c, f, e, a, d, g) {
    var b = {
        callback: g || function(i, h, k) {
            var j = $("div .item.active").attr("slidename");
            if ((i.exactMatch && i.recommendations.length == 0) && ((f == "CITY" && (j == "city" || j == "coapp_city" || j == "propertycity")) || (f == "COMPANY" && j == "employer") || (j == "car" && f == "CAR"))) {
                if ($("#productNameSpace").val() == "car-insurance") {
                    populateVariantCallback();
                    moveToSlideInQueue("next")
                } else {
                    moveToNextSlide()
                }
            }
        },
        showRecommendation: function(i, h, j) {
            j.attr("disabled", false).parents().filter("li:first").removeClass("dontshow").find(".wwlbl");
            if (j != null && typeof j != "undefined") {
                $("button[id=" + j.attr("id") + "]").focus();
                j.addClass("validate");
                j.removeClass("dontvalidate");
                j.siblings("div").removeClass("validate");
                $("button[id='" + j.attr("id") + "']").siblings("ul").find(".selected").removeClass("selected")
            }
        },
        hideRecommendation: function(h) {
            h.attr("disabled", true).parents().filter("li:first").addClass("dontshow");
            h.removeClass("validate");
            h.addClass("dontvalidate")
        }
    };
    b = $.extend(false, {}, {
        urlParams: {
            type: f,
            limit: 60
        },
        showMatchMsg: false,
        showSearchingMsg: true,
        changeMatchMsg: true
    }, b);
    b = $.extend(false, {}, {
        onRecoStart: a || function() {},
        onRecoEnd: d || function() {}
    }, b);
    b = $.extend(false, {}, {
        getDataFromDifferentField: c.dataFromField,
        dataFromField: c.dataFromField
    }, b);
    c.field.recommender("/" + e + "/recommend.html", c.reco, b, f)
}

function constructErrorMessage(b) {
    var c = "<table><tr><td><span class='error-icon'><img src='images/icon-error.png'/></span></td><td><span class='error-info'>";
    var a = "</span></td></tr></table>";
    return c + b + a
}
$("#eForm").validate({
    ignore: ".dontvalidate",
    errorPlacement: function(a, b) {
        var c = $(".item.active .footer-common");
        a.addClass("input-error-cl");
        if (c.length > 0 && (b.is(".existing-cc-options-required") || b.is(".adults-required") || b.is(".eldestage-required") || b.is(".hl-dob-day-required") || b.is(".hl-dob-month-required") || b.is(".hl-dob-year-required") || b.is(".basic-required-options") || b.is(".basic-required-date") || b.is(".fuelType-required") || b.is(".pl-required-options") || b.is(".hl-month-required") || b.is(".hl-year-required") || b.is(".hl-required-options") || b.is(".basic-pick-options") || b.is(".easy-options") || b.is(".goahead-options") || b.is(".hl-work-experience-required") || b.is(".basic-country-options") || b.is(".hl-coapplicant-joining-month-abroad-required") || b.is(".hl-joining-month-abroad-required") || b.is(".hl-coapp-work-experience-required") || b.is(".hl-joining-year-abroad-required") || b.is(".joining-date") || b.is(".hl-coapplicant-dob-year-required") || b.is(".hl-coapp-joining-year-required") || b.is(".hl-coapplicant-joining-month-required") || b.is(".hl-joining-month-required") || b.is(".basic-city-options") || b.is(".coapplicant-employment-type-required") || b.is(".employment-type-required") || b.is(".basic-manufacture-year"))) {
            c.html("");
            c.append(a);
            c.show()
        } else {
            if (b.closest(".input-group").length > 0 && !(b.is(".other-city-required-for-property"))) {
                a.insertAfter(b.closest(".input-group"))
            } else {
                a.insertAfter(b)
            }
        }
    },
    highlight: function(c, a, b) {
        $(c).trigger("bberror")
    }
});

function initSentenceFormValidations() {
    $.validator.addMethod("isDropDownChanged", function(b, a) {
        return ($(a).val() == "true")
    });
    $("#eFormSentence").validate({
        ignore: [],
        errorPlacement: function(a, b) {
            a.addClass("input-error-cl");
            a.html(constructErrorMessage("Uh-oh! Please give us these few details to proceed!"));
            var c = $(".el-sentence-error");
            c.html("");
            c.append(a);
            c.show()
        },
        rules: {
            "el-annual-salary": {
                required: true
            },
            "el-annual-salary-mobile": {
                required: true
            },
            "el-dob": {
                required: true
            },
            "el-genderdropdowntracker": {
                isDropDownChanged: true
            },
            "el-citydropdowntracker": {
                isDropDownChanged: true
            },
            "el-smokeinfodropdowntracker": {
                isDropDownChanged: true
            }
        },
        highlight: function(b, a) {
            if ($(b).hasClass("dropDownChangeTracker")) {
                $(b).next("select.trackDropDownChange").next("div").removeClass("cl-el-state").addClass("cl-el-state error-border-line")
            } else {
                $(b).addClass("error-border-line")
            }
        },
        onkeyup: function(a, b) {
            $(a).parents("span").find(".el-form-input").each(function(c) {
                if ($(this).val() != "") {
                    $(this).removeClass("error-border-line")
                }
            });
            if ($(".error-border-line").length == 0) {
                $(".el-sentence-error label").hide()
            }
        }
    })
}
$(function() {
    if ($("#productTypeCategory").val() == "INSURANCE") {
        initSentenceFormValidations()
    }
});

function initializeMarketingTag() {
    $(document).ready(function() {
        if (productType.val() == "home-loan") {
            triggerLongFormVariantAdWord()
        }
    });
    applicantResidenceCity.change(function() {
        triggerLongFormVariantAdWord()
    });
    var d = applicantResidenceCityOther.val();
    applicantResidenceCityOther.on("result", function() {
        if (applicantResidenceCityOther.val() != d) {
            triggerLongFormVariantAdWord()
        }
        d = applicantResidenceCityOther.val()
    });
    var c = applicantCompanyName.val();
    applicantCompanyName.on("result", function() {
        if (applicantCompanyName.val() != c) {
            triggerLongFormVariantAdWord()
        }
        c = applicantCompanyName.val()
    });
    var b = (productType.val() == "personal-loan" || productType.val() == "credit-card") ? applicantMonthlyTakeHomeSalary.val() : applicantGrossMonthlyIncome;
    if (productType.val() == "personal-loan" || productType.val() == "credit-card") {
        applicantMonthlyTakeHomeSalary.focus(function() {
            b = applicantMonthlyTakeHomeSalary.val()
        });
        applicantMonthlyTakeHomeSalary.on("blur", function() {
            if (applicantMonthlyTakeHomeSalary.val() != b) {
                triggerLongFormVariantAdWord()
            }
        })
    } else {
        applicantGrossMonthlyIncome.focus(function() {
            b = applicantGrossMonthlyIncome.val()
        });
        applicantGrossMonthlyIncome.on("blur", function() {
            if (applicantGrossMonthlyIncome.val() != b) {
                triggerLongFormVariantAdWord()
            }
        })
    }
    $('input[name="form.carName"]').change(function() {
        if ($('input[name="form.carName"]:checked').attr("id") != "carName0") {
            triggerLongFormVariantAdWord()
        }
    });
    var e = carName.val();
    carName.on("result", function() {
        if (carName.val() != e) {
            triggerLongFormVariantAdWord()
        }
        e = carName.val()
    });
    var a = costOfPropertyStandAlone.val();
    costOfPropertyStandAlone.focus(function() {
        a = costOfPropertyStandAlone.val()
    });
    costOfPropertyStandAlone.blur(function() {
        if (a != costOfPropertyStandAlone.val()) {
            triggerLongFormVariantAdWord()
        }
    })
}

function isFieldValuePresent(a) {
    return (a.val() != "" && a.val() != undefined) ? true : false
}

function pushMarketingTag(g, a, e, c, f, b) {
    var d = {
        event: "sendEligForm",
        city: g,
        employer: a,
        salary_display: e,
        productType: $("#productNameSpace").val()
    };
    if (e != undefined) {
        d.salary = Number(e.replace(/\,/g, ""))
    }
    if ($("#productNameSpace").val() == "car-loan") {
        d.car = c
    } else {
        if ($("#productNameSpace").val() == "home-loan") {
            d.propertycost = Number(f.replace(/\,/g, ""));
            d.propertycost_display = f
        } else {
            if ($("#productNameSpace").val() == "personal-loan") {
                d.purposeOfLoan = b
            } else {
                if ($("#productNameSpace").val() == "credit-card") {
                    d.rewardsType = b
                }
            }
        }
    }
    dataLayer.push(d)
}

function triggerLongFormVariantAdWord() {
    var a = (productType.val() == "personal-loan" || productType.val() == "credit-card") ? applicantMonthlyTakeHomeSalary : applicantGrossMonthlyIncome;
    if (getCity() != "" && isFieldValuePresent(applicantCompanyName) && isFieldValuePresent(a)) {
        if (productType.val() == "personal-loan" && productType.val() == "credit-card") {
            pushMarketingTag(getCity(), applicantCompanyName.val(), a.val())
        } else {
            if (productType.val() == "car-loan" && isFieldValuePresent($('input[name="form.carName"]:radio:checked')) || $('input[name="form.carName"]:radio:checked').attr("class") == "optional") {
                var b = ($('input[name="form.carName"]:radio:checked').attr("class") == "optional") ? carName.val() : $('input[name="form.carName"]:radio:checked').val();
                pushMarketingTag(getCity(), applicantCompanyName.val(), a.val(), b, undefined)
            } else {
                if (productType.val() == "home-loan" && isFieldValuePresent(costOfPropertyStandAlone)) {
                    pushMarketingTag(getCity(), applicantCompanyName.val(), a.val(), undefined, costOfPropertyStandAlone.val())
                }
            }
        }
    }
}

function triggerSlideVariantAdWord() {
    var g = $('#eForm input[name="form.applicantPlaceHolder.residenceCity.value"]');
    if (g.attr("type") == "radio") {
        var c = ($('#eForm input[name="form.applicantPlaceHolder.residenceCity.value"]:radio:checked').val() != BBConstants.otherValue) ? $('#eForm input[name="form.applicantPlaceHolder.residenceCity.value"]:radio:checked').val() : $("#eForm input[name='form.applicantPlaceHolder.residenceCity.fallback']").val()
    } else {
        c = g.val()
    }
    var a = $('#eForm input[name="form.carName"]');
    if (a.attr("type") == "radio") {
        var i = ($('input[name="form.carName"]:radio:checked').val() != "Other") ? $('input[name="form.carName"]:radio:checked').val() : $('input[name="carName_fallback_value_textbox"]').val()
    } else {
        i = a.val()
    }
    var f = $("input[name='form.details.property.costOfProperty']").val();
    var d = $("#productNameSpace").val();
    var b;
    if (d == "personal-loan") {
        b = $("input[name='form.details.purposeOfLoan']:radio:checked").val()
    } else {
        if (d == "credit-card") {
            b = $("input[name='form.details.purposeOfCard']:radio:checked").val()
        }
    }
    var h = (d == "personal-loan" || d == "credit-card") ? $("#eForm input[name='form.details.applicant.income.monthlyTakeHomeSalary']").val() : (d == "home-loan") ? (($("#eForm input[name='form.details.applicant.citizenshipStatus']").val() == BBConstants.residentIndian) ? $("#eForm input[name='form.details.applicant.income.grossMonthlyIncome']").val() : $("#eForm input[name='form.details.applicant.income.monthlyTakeHomeSalary']").val()) : $("#eForm input[name='form.details.applicant.income.grossMonthlyIncome']").val();
    var e = $("div .item.active:visible").attr("slidename");
    if (e == "salaryaccount" || e == "salarysavingsaccount" || e == "hascoapplicant") {
        pushMarketingTag(c, $("#eForm input[name='form.applicantPlaceHolder.companyName']").val(), h, i, f, b)
    }
}

function initNewStaggeredForm() {
    moveNextSlideMobile();
    $("#newstaggered-carousel [slideNextOn]").each(function() {
        var a = $(this).attr("slideNextOn").split(",");
        for (var b = 0; b < a.length; b++) {
            $(this).on(String(a[b]), function() {
                slideNext()
            })
        }
    });
    $("#newstaggered-carousel [showArrowOn]").each(function() {
        var b = $(this).attr("showArrowOn").split(",");
        for (var a = 0; a < b.length; a++) {
            $(this).on(String(b[a]), function() {
                showNextArrow()
            })
        }
    });
    $("#newstaggered-carousel [showTooltipOn]").each(function() {
        if ($("#mobileSite").val() != "true") {
            var d = $(this).attr("showTooltipOn").split(",");
            var a = $(this).attr("tooltip-message");
            var c = $(this).attr("tooltip-placement");
            for (var b = 0; b < d.length; b++) {
                $(this).tooltip({
                    placement: ((c != undefined && c != "") ? c : ($(window).width() < 1023 ? "bottom" : "right")),
                    trigger: String(d[b]),
                    title: a
                })
            }
        }
    });
    totalSlides = $("#newstaggered-carousel .item").length;
    $("#newstaggered-carousel,#get-quote").on("slide.bs.carousel", function(b, a) {
        if (b.direction == "left") {
            var d = true;
            if ($(".item.active .recommendedSearchImg").length > 0) {
                return false
            }
            $(".item.active .validate").each(function() {
                var e = $(this).valid();
                if (e != true) {
                    $(this).focus();
                    d = false;
                    return false
                }
            });
            if ($("#productNameSpace").val() == "home-loan" && !d) {
                var c = $("div .item.active #continueButton");
                c.hasClass("search-for-button") ? c.removeClass("loading-circle").html("Search for Home Loans") : c.removeClass("loading-circle").html("Continue")
            } else {
                if ($("#productNameSpace").val() == "personal-loan" && !d) {
                    var c = $("div .item.active #continueButton");
                    c.hasClass("search-for-button") ? c.removeClass("loading-circle").html("Search for Personal Loans") : c.removeClass("loading-circle").html("Continue")
                }
            }
            return d && canMoveToNextSlideForHl
        }
        return true
    });
    $("#get-quote #newstaggered-carousel").on("slid.bs.carousel", function(b, a) {
        if ($("#mobileSite").val() == "true") {
            document.activeElement.blur()
        }
        trackVirtualPagePath();
        triggerSlideVariantAdWord();
        var c = $("#newstaggered-carousel .item.active").index();
        if ($("#productTypeCategory").val() == "LOAN" || $("#productTypeCategory").val() == "CREDIT_CARD" || $("#productNameSpace").val() == "debit-card") {
            slideModelDialogBox()
        }
        if ($("#productNameSpace").val() == "life-insurance") {
            controlSlideDisplay(c)
        } else {
            if (c == 0) {
                $("#left_carousel_control").hide()
            } else {
                $(".news-tagger-heading").removeClass("dontshow");
                if ($("#mobileSite").val() == "true") {
                    $(".news-tagger-heading-mo").removeClass("dontshow")
                }
                $("#left_carousel_control").show()
            }
            if ($("#productNameSpace").val() == "car-insurance") {
                if (c == 1) {
                    $("#left_carousel_control").hide()
                }
                if ($("#newstaggered-carousel input[name='form.details.carDetail.type']:checked") != undefined) {
                    if ($("input[name='form.details.uiParameters.referenceId']").val() != null) {
                        totalSlides = 11
                    } else {
                        totalSlides = 9
                    }
                }
                if ($('#newstaggered-carousel input[name="alReadyExpiredInsurance"]').val() == "true") {
                    if ($("input[name='form.details.uiParameters.referenceId']").val() != null) {
                        totalSlides = 12
                    } else {
                        totalSlides = 10
                    }
                }
            }
            if ($("#productNameSpace").val() == "home-loan" && $("#eForm_form_details_uiParameters_variant").val() != "slide" && $("#displayCityCompleteElig").val() == "false" && $("#eForm_form_details_uiParameters_formType").val() != null && $("#eForm_form_details_uiParameters_formType").val() == "medium" && c == 2) {
                $("#left_carousel_control").hide();
                removeSlideFromQueue("0");
                removeSlideFromQueue("1")
            }
            if ($("#productNameSpace").val() == "health-insurance") {
                if (c == 1) {
                    $("#left_carousel_control").hide()
                }
            }
            if ($("#productNameSpace").val() == "personal-loan") {
                $("div .item #continueButton").each(function() {
                    if ($(this).hasClass("loading-circle")) {
                        if ($(this).hasClass("search-for-button")) {
                            $(this).removeClass("loading-circle").html("Search for Personal Loans")
                        } else {
                            $(this).removeClass("loading-circle").html("Continue")
                        }
                    }
                })
            }
            if ((c + 1) == totalSlides) {
                $("#right_carousel_control").hide()
            } else {
                if ($("#productTypeCategory").val() == "INSURANCE" && c == 0) {
                    $("#right_carousel_control").hide()
                } else {
                    $("#right_carousel_control").show()
                }
            }
        }
    });
    $("#eForm").on("keyup", "input", function(a) {
        if (a.keyCode == 13) {
            var b = $("#newstaggered-carousel .item.active").index();
            if ((b + 1) == totalSlides) {
                if (!$("#eForm").valid()) {
                    return false
                } else {
                    $("#eForm").submit()
                }
            } else {
                if ($("#newstaggered-carousel .item.active").attr("slidename") != "employer") {
                    slideNext()
                }
            }
        }
    });
    $("#eForm_submit_button").click(function() {
        $("#landingPageFlashSale").css("display", "none");
        mailID = $("input[name='form.details.applicant.currentContactDetail.primaryEmail.contact']").val();
        sourceValue = "CONTACT_SLIDE";
        sourceURL = window.location.href;
        if ($("#productTypeCategory").val() == "LOAN") {
            trackEnterEmailEvent(mailID, "eligSlide")
        }
        subscribeToNewsLetterViaMC(mailID, sourceValue, sourceURL);
        if ($('#newstaggered-carousel input[name="alReadyExpiredInsurance"]').val() == "true") {
            moveToSlideInQueue("next")
        } else {
            if (!$("#eForm").valid()) {
                $("html,body").animate({
                    scrollTop: $("#slide-grey").offset().top - 10
                });
                return false
            } else {
                setHiddenVariables();
                if ($("#productNameSpace").val() == "life-insurance" || $("#productNameSpace").val() == "car-insurance" || $("#productNameSpace").val() == "health-insurance") {
                    if ($("#newstaggered-carousel input[name='form.details.carDetail.type']:checked").val() == "NEW") {
                        $("#eForm_form_details_insuranceDetails_policyStartDate").val([pad(new Date().getMonth() + 1), pad(new Date().getDate()), new Date().getFullYear()].join("/"));
                        var a = $("#newstaggered-carousel input[name='form.details.carDetail.registeredOn']");
                        if (!a || a.val() == "") {
                            populateCarRegistrationDate()
                        }
                    }
                    $("#newstaggered-carousel #eForm_submit_button").addClass("loading-circle").val("Please wait..");
                    $("#eForm").submit()
                } else {
                    if ($("#productNameSpace").val() == "home-loan") {
                        handleNRIStatus()
                    }
                    $("header").removeClass("landingpage-sticky");
                    $("body").removeClass("landingpage-body");
                    $(this).addClass("progress-striped button-secondary progress active");
                    $("#waitModal").modal({
                        backdrop: "static"
                    });
                    $("#waitModal").modal("show");
                    trackVirtualPagePath(true, "loadingoffers");
                    $(this).prop("disabled", true);
                    $("#eForm").submit()
                }
            }
        }
    })
}

function highlightSlide() {
    if ($("#landingPageReviews").length != 0) {
        if ($("#mobileSite").val() == "true" || $("#eForm_form_details_uiParameters_mode").val() == "seo") {
            $("html,body").animate({
                scrollTop: $("#get-quote").offset().top - 40
            }, "slow")
        } else {
            $("html,body").animate({
                scrollTop: $("#get-quote").offset().top - 80
            }, "slow")
        }
        $(".close-btn").addClass("active");
        $("#landingPageReviews").addClass("screen-block-z-index");
        $("#slide-grey").removeClass("dontshow");
        $(".menu-section-left > li:last-child a").css({
            zIndex: 0
        })
    }
    setVisibilityPendingAppReminder(false)
}

function unHighlightSlide() {
    if ($("#landingPageReviews").length != 0) {
        $("#landingPageReviews").find(".screen-block-z-index").removeClass("screen-block-z-index");
        $(".close-btn").removeClass("active");
        $("#slide-grey").addClass("dontshow");
        $("html").css({
            overflow: "auto"
        });
        $(".menu-section-left > li:last-child a").css({
            zIndex: 9999
        })
    }
}

function trackVirtualPagePath(a, c) {
    var d = $("#newstaggered-carousel div .item.active");
    if (a || d.is(":visible")) {
        var b = (typeof c == "undefined") ? d.attr("slidename") : c;
        dataLayer.push({
            event: "sendVirtualPage",
            pagePath: "virtual-" + $("#productNameSpace").val() + "/" + b
        })
    }
}

function slideModelDialogBox() {
    if ($("#productNameSpace").val() == "car-insurance" || $("#productNameSpace").val() == "life-insurance" || $("#productNameSpace").val() == "health-insurance") {
        $(".tabmarg").css("z-index", 0)
    }
    highlightSlide();
    $("#eForm").on("click", ".close-btn", function() {
        if ($("#productNameSpace").val() == "car-insurance" || $("#productNameSpace").val() == "life-insurance" || $("#productNameSpace").val() == "health-insurance") {
            $(".tabmarg").css("z-index", 1000)
        }
        unHighlightSlide()
    })
}

function handleNRIStatus() {
    var b = $(".complete-eligibility-form input[name='form.details.applicant.citizenshipStatus']").val();
    var a = $(".complete-eligibility-form  input[name='form.details.coApplicant.citizenshipStatus']").val();
    if (b != BBConstants.residentIndian && b != null && b != "") {
        disableCitySlideForNRI(residenceCitySlideNo)
    }
    if (a != BBConstants.residentIndian && $("input[name='form.details.applyingWithCoApplicant']:checked").val() == "true") {
        if (a == null || a == "") {
            $("#hidden_coapplicant_citizenship_status").attr("disabled", false).val(BBConstants.residentIndian)
        } else {
            disableCitySlideForNRI(coApplicantResidenceCitySlideNo)
        }
    }
    if ($("input[name='form.details.applyingWithCoApplicant']:checked").val() == "false") {
        $("#hidden_coapplicant_citizenship_status").val("").attr("disabled", true)
    }
}
$(window).on("hashchange", function() {
    if (window.location.hash == "#get-quote") {
        hideDealsSlide()
    }
});

function hideDealsSlide() {
    $("#landingPageFlashSale").addClass("dontshow");
    $(".fs-small-sticky").addClass("dontshow")
}

function showDealsSlide() {
    $("#landingPageFlashSale").removeClass("dontshow");
    $(".fs-small-sticky").removeClass("dontshow")
}
$(function() {
    if ($("html").hasClass("ios iphone")) {
        (function(b) {
            b.fn.cancelZoom = function() {
                return this.each(a)
            };
            b("#newstaggered-carousel,#get-quote input,select,textarea").cancelZoom()
        })(jQuery);

        function a() {
            var g = document,
                b, e, h = ",maximum-scale=",
                f = /,*maximum\-scale\=\d*\.*\d*/;
            if (!this.addEventListener || !g.querySelector) {
                return
            }
            b = g.querySelector('meta[name="viewport"]');
            e = b.content;

            function c(d) {
                b.content = e + (d.type == "blur" ? (e.match(f, "") ? "" : h + 10) : h + 1)
            }
            this.addEventListener("focus", c, true);
            this.addEventListener("blur", c, false)
        }
    }
});
var gaCategory = "appReminder";
var gaActions = {
    VIEW_REVIEWS_LINK: "viewReviews",
    CLOSE_REVIEWS_LINK: "closeReviews",
    CALL_ATTEMPT: "callMeAttempt",
    CALL_BUTTON: "callMe",
    CONTINUE_BUTTON: "continue",
    APP_WIDGET_SHOWN_TO_USER: "shownToUser",
    APP_WIDGET_CLOSE_ATTEMPT: "dismiss",
    APP_WIDGET_CLOSE_BY_USER: "dontShow",
    APP_WIDGET_HIDDEN: "hiddenFromUser"
};
$(document).ready(function() {
    if (e()) {
        d()
    }
    $("#pendingAppRemDiv").on("click", ".close-btn", function() {
        if (isCookieEnabled() == true) {
            $("#pendingAppRemDiv").find(".app-remind-header").hide();
            $("#pendingAppRemDiv").find(".app-dont-remind-header").removeClass("headerHide");
            trackGAEventForAppReminder(gaActions.APP_WIDGET_CLOSE_ATTEMPT, getElapsedTimeInSeconds(widgetStartTime))
        } else {
            setVisibilityPendingAppReminder(false, gaActions.APP_WIDGET_CLOSE_BY_USER)
        }
    });
    $("#pendingAppRemDiv").on("click", ".ok-btn", function() {
        setVisibilityPendingAppReminder(false, gaActions.APP_WIDGET_CLOSE_BY_USER);
        var m = $("#pendingAppRemDiv").find("#userChoice")[0].checked;
        if (m == true) {
            setCookie("pendingAppReminder", "dontShow", null, "/")
        }
    });
    $("#pendingAppRemDiv").on("click", "#viewReviews", function() {
        trackGAEventForAppReminder(gaActions.VIEW_REVIEWS_LINK, getProductType(this));
        $.each($(".item"), function(m, n) {
            $(n).find("#showCustReviews").collapse("show");
            $(n).find("#viewReviews").hide()
        });
        $("#pendingAppRemDiv").find("#pendingAppCarousel").carousel("pause")
    });
    $("#pendingAppRemDiv").on("click", "#closeReviews", function() {
        trackGAEventForAppReminder(gaActions.CLOSE_REVIEWS_LINK, getProductType(this));
        $.each($(".item"), function(m, n) {
            $(n).find("#showCustReviews").collapse("hide");
            $(n).find("#viewReviews").show()
        });
        $("#pendingAppRemDiv").find("#pendingAppCarousel").carousel("cycle")
    });
    $("#pendingAppRemDiv").on("click", ".talkToBB", function() {
        $(this).parents(".active").find("#inputPhoneNumber").toggle("slide");
        trackGAEventForAppReminder(gaActions.CALL_ATTEMPT, getProductType(this))
    });
    $("#pendingAppRemDiv").on("click", ".par-continue", function() {
        trackGAEventForAppReminder(gaActions.CONTINUE_BUTTON, getProductType(this))
    });
    $("#pendingAppRemDiv").on("click", "#callbackRequestLead", function() {
        f(this, "#applicationId", "#mobileForCallbackRequest")
    });

    function f(p, q, o) {
        var n = $(p).parents(".active").find(o).val();
        var r = $(p).parents(".active").find(q).val();
        var m = "callMeBackRequest.html?&applicationId=" + r + "&mobile=" + n + "&source=PENDING_APP_REMINDER";
        if (isMobile(n)) {
            $.ajax({
                url: m,
                cache: false,
                type: "post",
                success: function() {
                    trackGAEventForAppReminder(gaActions.CALL_BUTTON, getProductType(p));
                    $(p).parents(".btn-action").find(".talkto_us_success").show();
                    setTimeout(function() {
                        $(p).parents(".btn-action").find(".talkto_us_success").hide();
                        d()
                    }, 5000)
                }
            })
        } else {
            $(".invalid-mobile").show()
        }
    }

    function d() {
        a();
        var m = "/pendingAppReminder_ajax.html?ajax=true";
        $.ajax({
            url: m,
            cache: false,
            type: "get",
            success: function(n) {
                $("#pendingAppRemDiv").html(n);
                formatNumber();
                var o = parseInt($("#pendingApplicationsCount").val());
                if (o > 0) {
                    if (l()) {
                        h();
                        $(window).scroll(h)
                    }
                    g();
                    if (o == 1) {
                        $(".pendingAppCarouselClass").carousel("stop")
                    }
                } else {
                    $("#pendingAppRemDiv").hide()
                }
            }
        })
    }

    function g() {
        $.each($(".loan-amount"), function(m, n) {
            $(n).html(getFormattedAmount($(n).html()))
        });
        $.each($(".emi-amount"), function(m, n) {
            $(n).html(getFormattedAmount($(n).html()))
        })
    }

    function i() {
        var m = false;
        if (isCookieEnabled()) {
            m = getCookie("applicationCreated") == "true"
        }
        return m
    }
    $("#pendingAppRemDiv").on("click", ".previousItem", function() {
        $(".pendingAppCarouselClass").carousel("prev")
    });
    $("#pendingAppRemDiv").on("click", ".nextItem", function() {
        $(".pendingAppCarouselClass").carousel("next")
    });
    $("#pendingAppRemDiv").on("keyup", ".mobileForCallbackReq", function() {
        var n = false;
        var m = $("#mobileForCallbackRequest").val();
        if (m == "" || m.length == 10) {
            n = true
        }
        if (n) {
            $(".invalid-mobile").hide()
        }
    });
    $("#pendingAppRemDiv").on("focus", ".mobileForCallbackReq", function() {
        $(".mobileForCallbackBtnGrp").addClass("focus")
    });
    $("#pendingAppRemDiv").on("focusout", ".mobileForCallbackReq", function() {
        $(".mobileForCallbackBtnGrp").removeClass("focus")
    });

    function a() {
        var m = ($(".tabbable-one") != null) ? $(".tabbable-one").find(".active") : null;
        if (m != null && (m.find("#cl-quick-quote-comparison") != null && m.find("#cl-quick-quote-comparison").size() > 0) || (m.find("#cl-quote-comparison-link") != null && m.find("#cl-quote-comparison-link").size() > 0)) {
            setVisibilityPendingAppReminder(false)
        }
    }
    $("#pendingAppRemDiv").on("click", ".close", function() {
        var m = $(this).parent().find("iframe");
        var o = m.attr("src");
        m.attr("src", "");
        var n = "?autoplay=1";
        if (o.indexOf(n) > -1) {
            o = o.replace(n, "")
        }
        m.attr("src", o)
    });

    function e() {
        var m = (pageName != null && pageName.value == "eligPage" && variantName != null && variantName.value == "completeEligibility");
        return i() && !getUserChoiceForAppReminder() && variant != "slide" && !m
    }

    function h() {
        if (checkVisibilityPendingAppReminder() && !j()) {
            c()
        } else {
            b()
        }
    }

    function l() {
        return $("#pageName") != null && $("#pageName").val() == "indexPage" && k()
    }

    function k() {
        var m = $("#mobileSite") && $("#mobileSite").val() == "true";
        var n = m ? ".all-product" : ".bb-main-menu";
        return $(n) != null && $(n).offset() != null
    }

    function j() {
        if (!k()) {
            return false
        }
        var m = $("#mobileSite") && $("#mobileSite").val() == "true";
        var n = m ? ".all-product" : ".bb-main-menu";
        var o = $(window).height() + $(window).scrollTop() - $(n).offset().top - $(n).height();
        var p = m ? 200 : 400;
        return o < p
    }
    if (!l()) {
        c()
    }

    function c() {
        setVisibilityPendingAppReminder(true)
    }

    function b() {
        setVisibilityPendingAppReminder(false)
    }
    $("#pendingAppRemDiv").on("shown.bs.modal", "#coupon-offer", function() {
        $(".menu-login").css({
            zIndex: 0
        })
    });
    $("#pendingAppRemDiv").on("hidden.bs.modal", "#coupon-offer", function() {
        $(".menu-login").css({
            zIndex: 9999
        })
    })
});

function setAutoplayForModal() {
    var b = $("#coupon-offer").find("iframe");
    var c = b.attr("src");
    var a = "?autoplay=1";
    var d = c + a;
    b.attr("src", d)
}

function trackGAEventForAppReminder(b, a) {
    BB_trackGoogleEventWithLabel(gaCategory, b, a)
}

function getProductType(a) {
    if (a != null && a != "") {
        var b = $(a).parents(".item").attr("data-product");
        if (b != undefined && b != null) {
            return b.toString().toLowerCase().replace("_", "-")
        }
    }
    return ""
};