import os, sys
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "investak_new.settings")
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from django.core.wsgi import get_wsgi_application
os.environ['DJANGO_SETTINGS_MODULE'] = 'investak_new.settings'
application = get_wsgi_application()

import psycopg2
import requests
from django.shortcuts import render_to_response
from django.db import connection
from django.utils.safestring import mark_safe, SafeString
from django.utils.encoding import smart_str, smart_unicode
from django.template.loader import get_template, render_to_string
from django.template import Context
from django.conf import settings
from django.core.mail import send_mail, BadHeaderError
from django.core.mail import EmailMultiAlternatives

print "Send newsletter cron job"
db_con = psycopg2.connect(database="investak_dev", user="investak", password="investak2016", host="localhost", port="5432")

limit = 40
# send user newsletter 
from_email = settings.CONTACT_EMAIL
cursor = db_con.cursor()
cursor.execute("SELECT user_email,news_letter_id,id  FROM investak_newslettertouser WHERE status='0' ORDER BY created_date ASC LIMIT "+str(limit))
records = cursor.fetchall()
for record in records:
	print "Email send to : "+record[0]
	
	newsletter_cursor = db_con.cursor()

	to_email = record[0]
	#get newsletter details
	newsletterList = newsletter_cursor.execute("SELECT subject_name, text_desc, source FROM investak_newsletter  WHERE id='"+str(record[1])+"'")
	newsletter_rec = newsletter_cursor.fetchone()
	subject = newsletter_rec[0]
	email_content = SafeString(smart_str(newsletter_rec[1]))
	source_name = newsletter_rec[2]
	html_content = get_template('news_letter_template.html').render(Context({'content': email_content, 'source_name': source_name}))
	if subject and to_email and from_email:
		try:
			text_content = '...'
			msg = EmailMultiAlternatives("Newsletter - "+str(subject), text_content, 'Investak '+from_email, [str(to_email)])
			msg.attach_alternative(html_content, "text/html")
			msg.send()
		except BadHeaderError:
			print 'Invalid header found.'
	newsletter_cursor.close()		        
	#update newsletter send record 
	update_cursor = db_con.cursor()
	update_cursor.execute("UPDATE investak_newslettertouser SET status='%s' WHERE id='%s'",(1,record[2]))
	db_con.commit()
	update_cursor.close()
cursor.close()
db_con.close()
